package com.langit7.welovehonda;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.LatLonObject2;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.parse.ParseLatLon2;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class TourAndTrackingListUserTrackingDetailActivity extends FragmentActivity{

	boolean networkSlow;
	public int currPos;
	ProgressBar pr;
	public GoogleMap map;
	double latitude;
	double longitude;
	GPSTracker gps;
	MarkerOptions marker2;
	public Marker posMarker;
	private String insStr;
	@SuppressWarnings("unused")
	private String gid;
	@SuppressWarnings("unused")
	private String uid;
	private int position;
	@SuppressWarnings("unused")
	private Polyline polyLine;
	private LatLng newLatLong;
	private LatLng newLatlong2;
	private LatLng newLatlong3;
	@SuppressWarnings("unused")
	private LatLng newLatlong4;
	ArrayList<LatLonObject2>listData;
	@SuppressWarnings("unused")
	private ArrayList<NewsDetailObject> listDetailObject;
	private ArrayList<CataloguDetail2Object> listLatlon;
	private ImageView ava;
	private TextView userName;
	ImageLoader imageLoader = ImageLoader.getInstance();
	private DisplayImageOptions options;
	private String user;
	private String urlimage;
	private int w;
	private int h;
	private ImageButton buttHint;
	private Dialog dialogRetweet;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking Location Map");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.track_location_history_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;
		
		init();
		buttHint = (ImageButton)findViewById(R.id.hint);
		gid = Credential.getCredentialData(this, BaseID.KEY_DATA_GID);
		uid = Credential.getCredentialData(this, BaseID.KEY_DATA_UID);
		position = (int) getIntent().getIntExtra("POS", 0);
		ava = (ImageView)findViewById(R.id.avatar);
		userName = (TextView)findViewById(R.id.title2);

		pr = (ProgressBar)findViewById(R.id.progressBar);
		gps = new GPSTracker(TourAndTrackingListUserTrackingDetailActivity.this);

		buttHint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogDesc("Menampilkan list history dari seluruh aktivitas tracking yang telah dilakukan user.");
			}
		});
		
		try {
			initilizeMap();
			userName.setText(user);
			imageLoader.displayImage(urlimage, ava,options);
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		map = mapFrag.getMap();
//		map.getUiSettings().setZoomControlsEnabled(false);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			initilizeMap();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}


	public void init(){

		user = Credential.getCredentialData(TourAndTrackingListUserTrackingDetailActivity.this, BaseID.KEY_DATA_USER_NAME_LIST);
		urlimage = Credential.getCredentialData(TourAndTrackingListUserTrackingDetailActivity.this, BaseID.KEY_DATA_GID_LIST);

		imageLoader.init(ImageLoaderConfiguration.createDefault(TourAndTrackingListUserTrackingDetailActivity.this));

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.cacheInMemory(true)
		.displayer(new RoundedBitmapDisplayer(300))
		.showImageOnLoading(R.drawable.ic_launcher)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();

		listData = new ArrayList<LatLonObject2>();

		listDetailObject = new ArrayList<NewsDetailObject>();
		listLatlon = new ArrayList<CataloguDetail2Object>();

	}

	public void showView(){

		insStr = Credential.getCredentialData(this, BaseID.KEY_DATA_LIST_HISTORY_FRIEND);
		ParseLatLon2 parsingForum = new ParseLatLon2();
		try {
			listData = parsingForum.getArrayListObject(insStr);
			listLatlon = listData.get(position).getListLatlon();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String strLat = listLatlon.get(0).getName();
		double valueLat = Double.parseDouble(strLat);
		String strLon = listLatlon.get(0).getDesc();
		double valueLon = Double.parseDouble(strLon);
		newLatlong3 = new LatLng(valueLat, valueLon);

		CameraPosition cameraPosition = new CameraPosition.Builder().target(
				newLatlong3).zoom(17).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		addPolylineLocations();
	}

	private void addPolylineLocations() {

		for ( int i = 0; i < listLatlon.size(); i++) {

			String strLat = listLatlon.get(i).getName();
			double valueLat = Double.parseDouble(strLat);
			String strLon = listLatlon.get(i).getDesc();
			double valueLon = Double.parseDouble(strLon);


			String strLat2 = listLatlon.get(i+1).getName();
			double valueLat2 = Double.parseDouble(strLat2);
			String strLon2 = listLatlon.get(i+1).getDesc();
			double valueLon2 = Double.parseDouble(strLon2);

			newLatlong2 = new LatLng(valueLat2, valueLon2);
			newLatLong = new LatLng(valueLat, valueLon);

			polyLine = map.addPolyline(new PolylineOptions().add(newLatLong, newLatlong2).width(5).color(Color.RED));
		}
	}
	
	private void dialogDesc(final String idStatus) {
		dialogRetweet = new Dialog(this, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button2);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, int pos) {
		Intent intent = new Intent(context, TourAndTrackingListUserTrackingDetailActivity.class);
		Bundle b = new Bundle();
		b.putInt("POS", pos);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
