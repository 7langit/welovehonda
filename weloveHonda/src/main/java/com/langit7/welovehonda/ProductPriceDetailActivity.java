package com.langit7.welovehonda;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ViewPagerAdapter;
import com.langit7.welovehonda.adapter.ViewPagerSliderAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductPriceDetailActivity extends Activity{

	private String content;
	boolean networkSlow;
//	private ImageLoader imageLoader= ImageLoader.getInstance();
	private DisplayImageOptions options;
	private Button buttonBuy;
	private String img;
	private String harga;
//	private ImageView imgView;
	private TextView descDet;
	private TextView priceDet;
	private String title;
	private TextView titleDet;
	private BroadcastReceiver logoutReceiver;
	private ViewPager sliderViewPager;
	private LinearLayout sliderDots;

	private int dotsCount;
	private ImageView[] dots;
//	View view;
	private Context context;

	private List<String> listImage = new ArrayList<>();
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Product Price Detail");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_price_layout);
		context = ProductPriceDetailActivity.this;
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttonBuy = (Button)findViewById(R.id.buttonbuy);
//		imgView = (ImageView)findViewById(R.id.imageView);
		descDet = (TextView)findViewById(R.id.detail);
		priceDet = (TextView)findViewById(R.id.price);
		titleDet = (TextView)findViewById(R.id.title);
		sliderViewPager = (ViewPager)findViewById(R.id.sliderViewPager);
		sliderDots = (LinearLayout)findViewById(R.id.sliderDots);

		try {
			init();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
//			init();
//			showView();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void init(){
//		listImage.clear();
		content = (String)getIntent().getSerializableExtra("CONTENT");
		img = (String)getIntent().getSerializableExtra("IMG");
		harga = (String)getIntent().getSerializableExtra("PRICE");
		title = (String)getIntent().getSerializableExtra("TITLE");
		listImage = getIntent().getStringArrayListExtra("LIST_IMAGE");

	}


	private void showView() {

		try {

			descDet.setText(content);

			titleDet.setText(title);
			priceDet.setText(harga);


			//setup slider
			ViewPagerSliderAdapter viewPagerAdapter = new ViewPagerSliderAdapter(ProductPriceDetailActivity.this, listImage);
			sliderViewPager.setAdapter(viewPagerAdapter);
			dotsCount = viewPagerAdapter.getCount();

			dots = new ImageView[dotsCount];

			for (int i = 0; i < dotsCount; i++) {
				dots[i] = new ImageView(context);
				dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.slider_uf));

				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				params.setMargins(8, 0, 8, 0);
				params.width = 40;
				params.height = 40;
				sliderDots.addView(dots[i], params);
			}

			dots[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.slider_f));

			sliderViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

				}

				@Override
				public void onPageSelected(int position) {
					for (int i = 0; i < dotsCount; i++) {
						dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.slider_uf));
					}
					dots[position].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.slider_f));
				}

				@Override
				public void onPageScrollStateChanged(int state) {

				}
			});

			//end setup slider

			buttonBuy.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					DealerLocationSearchActivity.show(ProductPriceDetailActivity.this);

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String content, String img2,String price2,String title2, List<String> listImage) {
		Intent intent = new Intent(context, ProductPriceDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("IMG", img2);
		b.putString("PRICE", price2);
		b.putString("CONTENT", content);
		b.putString("TITLE", title2);
		b.putStringArrayList("LIST_IMAGE", (ArrayList<String>) listImage);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
