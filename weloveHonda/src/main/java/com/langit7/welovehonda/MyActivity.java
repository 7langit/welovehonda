package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.langit7.welovehonda.adapter.ListAdapterForumList;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class MyActivity extends Activity{

	private Button buttActivity;
	private Button buttForum;
	public ProgressBar pr;
	private String KEY_DATA;
	private SharedPreferences prefData;
	ArrayList<LatLonObject>listForumObject;
	private boolean networkSlow;
	private ListAdapterForumList listMenu;
	private ListView listViewMenu;
	private ImageButton buttLogout;
	private RelativeLayout disclaimerButt;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_activity_layout5);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		Credential.saveCredentialData(this, BaseID.KEY_DATA_LOGIN, "1");

		buttActivity = (Button)findViewById(R.id.activity);
		buttForum = (Button)findViewById(R.id.forum);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttForum.setBackgroundResource(R.drawable.button_forumactivityf);
		buttLogout = (ImageButton)findViewById(R.id.logout);
		listViewMenu = (ListView)findViewById(R.id.listView);
		disclaimerButt = (RelativeLayout)findViewById(R.id.areadisclaimer2);

		disclaimerButt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyActivityDisclaimer.show(MyActivity.this);
			}
		});

		buttLogout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Credential.saveCredentialData(MyActivity.this, BaseID.KEY_DATA_LOGIN, "");
				MyActivityRegister.show(MyActivity.this);
				finish();
			}
		});

		buttActivity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyActivityEvent.show(MyActivity.this);
			}
		});

		try {
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void init(){

		KEY_DATA = BaseID.KEY_DATA_FORUM;
		prefData = getPreferences(MODE_PRIVATE);
		listForumObject = new ArrayList<LatLonObject>();

	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(BaseID.URL_FORUM_LIST);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("type", "forum"));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

		ParseLatLon parsingForum = new ParseLatLon();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		listMenu = new ListAdapterForumList(this, R.layout.listitem, listForumObject);
		listViewMenu.setAdapter(listMenu);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		String login = Credential.getCredentialData(this, BaseID.KEY_DATA_LOGIN);
		if(login.equals("1")){
			DashboardActivity.show(MyActivity.this);
		}else{
			RegisterMyActivity.show(MyActivity.this);
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(MyActivity.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}
	
	public static void show(Context context) {
		Intent intent = new Intent(context, MyActivity.class);
		context.startActivity(intent);
//		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		((Activity)context).overridePendingTransition(0, 0);
	}
}
