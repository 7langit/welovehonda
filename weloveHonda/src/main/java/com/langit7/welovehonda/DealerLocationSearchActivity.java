package com.langit7.welovehonda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.API.APIServices;
import com.langit7.welovehonda.object.dealer_type;
import com.langit7.welovehonda.object.listdata;
import com.langit7.welovehonda.object.nv;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DealerLocationSearchActivity extends Activity{


	public Dialog dialog;
	ProgressBar pr;
	private BroadcastReceiver logoutReceiver;

	private Spinner spprovinsi;
	private Spinner spkota;
	private Spinner sptype;
	private Button btnsubmit;

	List<nv> mcitys = new ArrayList<>();
	List<nv> mprovinces = new ArrayList<>();
	List<String> citys = new ArrayList<String>();
	List<String> provinces = new ArrayList<String>();

	List<String> types = new ArrayList<>();
	List<nv> dealerservices = new ArrayList<>();
	ArrayAdapter<String> typeadapter;


	List<dealer_type> dealertypes = new ArrayList<>();

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_dealer_layout2);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		pr = (ProgressBar) findViewById(R.id.progressBar);
		spprovinsi = (Spinner)findViewById(R.id.spprovinsi);
		spkota = (Spinner)findViewById(R.id.spkota);
		sptype = (Spinner)findViewById(R.id.sptype);
		btnsubmit = (Button)findViewById(R.id.btnsubmit);


		APIServices.generateService(this).getProvince("1", new Callback<listdata<nv>>() {
			@Override
			public void success(listdata<nv> nvlistdata, Response response) {
				pr.setVisibility(View.INVISIBLE);
				if (nvlistdata != null && nvlistdata.getData() != null && nvlistdata.getData().size() > 0) {
					provinces.clear();
					mprovinces.clear();
					provinces.add("Pilih Provinsi");
					for (nv d : nvlistdata.getData()) {
						provinces.add(d.getName());
						mprovinces.add(d);
					}
					ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, provinces); //selected item will look like a spinner set from XML
					spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
					spprovinsi.setAdapter(spinnerArrayAdapter);
					spinnerArrayAdapter.notifyDataSetChanged();

					citys.add("Pilih Kota");
					ArrayAdapter<String> spinner1ArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, citys); //selected item will look like a spinner set from XML
					spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
					spkota.setAdapter(spinner1ArrayAdapter);
				}

				APIServices.generateService(getApplicationContext()).getDealerService("1", new Callback<listdata<nv>>() {
					@Override
					public void success(listdata<nv> nvlistdata, Response response) {


						if (nvlistdata.getData() != null && nvlistdata.getData().size() > 0) {
							dealerservices.clear();
							types.clear();

							types.add("Pilih Jenis Pelayanan");
							for (nv d : nvlistdata.getData()) {
								dealerservices.add(d);
								types.add(d.getName());
							}
							typeadapter.notifyDataSetChanged();


						}
					}

					@Override
					public void failure(RetrofitError error) {

					}
				});


			}

			@Override
			public void failure(RetrofitError error) {

				finish();
			}
		});


		spprovinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

				if (i > -1) {
					if(spprovinsi.getSelectedItemPosition()>0) {
						showLoadingDialog();
						APIServices.generateService(getApplicationContext()).getCity("1", mprovinces.get(spprovinsi.getSelectedItemPosition() - 1).getId(), new Callback<listdata<nv>>() {
							@Override
							public void success(listdata<nv> nvlistdata, Response response) {
								if (nvlistdata != null && nvlistdata.getData() != null && nvlistdata.getData().size() > 0) {
									citys.clear();
									mcitys.clear();
									citys.add("Pilih Kota");
									for (nv d : nvlistdata.getData()) {
										citys.add(d.getName());
										mcitys.add(d);
									}
									ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, citys); //selected item will look like a spinner set from XML
									spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
									spkota.setAdapter(spinnerArrayAdapter);
									spinnerArrayAdapter.notifyDataSetChanged();

								}
								dismisLoadingDialog();
							}

							@Override
							public void failure(RetrofitError error) {

							}
						});
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		typeadapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, types); //selected item will look like a spinner set from XML
		typeadapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
		sptype.setAdapter(typeadapter);

		APIServices.generateService(getApplicationContext()).getDealerType("1", new Callback<listdata<dealer_type>>() {
			@Override
			public void success(listdata<dealer_type> dealer_typelistdata, Response response) {
				if (dealer_typelistdata.getData() != null && dealer_typelistdata.getData().size() > 0) {
					dealerservices.clear();
					for (dealer_type d : dealer_typelistdata.getData()) {
						dealertypes.add(d);
					}
				}
			}
			@Override
			public void failure(RetrofitError error) {
			}
		});


		btnsubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try{
					if (spprovinsi.getSelectedItemPosition() > 0 && spkota.getSelectedItemPosition() > 0 && sptype.getSelectedItemPosition() > 0) {
						Intent ii = new Intent(getApplicationContext(), DealerDetailActivity.class);
						ii.putExtra("cid", mcitys.get(spkota.getSelectedItemPosition()-1).getId());
						ii.putExtra("pid", mprovinces.get(spprovinsi.getSelectedItemPosition()-1).getId());
						ii.putExtra("tid", dealerservices.get(sptype.getSelectedItemPosition()-1).getId());
						ii.putExtra("types", (Serializable) dealertypes);

						startActivity(ii);
					}else{
						if(spprovinsi.getSelectedItemPosition() ==0)
							Toast.makeText(getApplicationContext(), "Pilih Provinsi", Toast.LENGTH_SHORT).show();
						else if(spkota.getSelectedItemPosition() ==0)
							Toast.makeText(getApplicationContext(),"Pilih Kota", Toast.LENGTH_SHORT).show();
						else if(sptype.getSelectedItemPosition() ==0)
							Toast.makeText(getApplicationContext(), "Pilih Jenis Pelayanan", Toast.LENGTH_SHORT).show();
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}
	

	public static void show(Context context) {
		Intent intent = new Intent(context, DealerLocationSearchActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	public void showLoadingDialog(){
		if(dialog==null || !dialog.isShowing()) {
			dialog = ProgressDialog.show(DealerLocationSearchActivity.this, "Loading", "Please Wait...",true,true);
			//dialog =DialogUtil.createLoadingDialog(ctx);
		}
	}

    public void dismisLoadingDialog(){
        if(dialog!=null && dialog.isShowing())
            try {
                dialog.dismiss();
            } catch (Exception e) {
                //e.printStackTrace();
            }

    }
}
