package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ExpandableListAdapterProductCatalogue;
import com.langit7.welovehonda.object.TypeProductCatalogueObject;
import com.langit7.welovehonda.parse.ParseTypeProductCatalogue;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MyActivityEvent extends Activity{

	private Button buttActivity;
	private Button buttForum;
	private ProgressBar pr;
	private String KEY_DATA;
	private SharedPreferences prefData;
	private ExpandableListAdapterProductCatalogue listMenu;
	public boolean networkSlow;
	ExpandableListView listViewMenu;
	String title;
	protected String pos;
	private BroadcastReceiver logoutReceiver;

	ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects = new ArrayList<TypeProductCatalogueObject>();
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - My Activity Download Manual Owner Page");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_activity_layout3);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttActivity = (Button)findViewById(R.id.activity);
		buttForum = (Button)findViewById(R.id.forum);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttActivity.setBackgroundResource(R.drawable.button_myactivityf);
		listViewMenu = (ExpandableListView) findViewById(R.id.lvExp);

		buttForum.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String preflogin = Credential.getCredentialData(MyActivityEvent.this, BaseID.KEY_DATA_LOGIN);
				if(preflogin.equals("1")){
					MyActivity.show(MyActivityEvent.this);
				}else{
					MyActivityRegister.show(MyActivityEvent.this);
				}
			}
		});

		init();
		new OpenData().execute();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	public void init(){

		KEY_DATA = "list_activity";
		prefData = getPreferences(MODE_PRIVATE);
		
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.showImageOnLoading(R.drawable.imagedefault_slidescreen)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
//			is = getData(BaseID.URL_LIST_MYACTIVITY);
			is = getData(BaseID.URL_LIST_ALL_MYACTIVITY);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

//		ParseCatalogue parsingCata = new ParseCatalogue();
		ParseTypeProductCatalogue parseTypeProductCatalogue = new ParseTypeProductCatalogue();
		try {
//			listCataObject = parsingCata.getArrayListObject(insStr);
//			listCataloObject = listCataObject.get(0).getListDetail2Object();
			
			parseTypeProductCatalogue.fethingData(insStr);
			typeProductCatalogueObjects = parseTypeProductCatalogue.getTypeProductCatalogueObjects();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		listMenu = new ExpandableListAdapterProductCatalogue(this, typeProductCatalogueObjects, imageLoader, options);
		listViewMenu.setAdapter(listMenu);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;

		
		listViewMenu.setIndicatorBounds(width - GetPixelFromDips(50), width
				- GetPixelFromDips(10));
		listViewMenu.setIndicatorBounds(width - GetPixelFromDips(50), width
				- GetPixelFromDips(10));
	}
	
	public int GetPixelFromDips(float pixels) {
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(MyActivityEvent.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, MyActivityEvent.class);
		context.startActivity(intent);
//		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		((Activity)context).overridePendingTransition(0, 0);
	}
}
