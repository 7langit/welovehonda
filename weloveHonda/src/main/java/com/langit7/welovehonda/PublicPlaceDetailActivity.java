package com.langit7.welovehonda;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class PublicPlaceDetailActivity extends FragmentActivity{

	public GoogleMap map;
	double latitude;
	double longitude;
	String title;
	String phone;
	String email;
	String addr;
	private TextView judul;
	private TextView add;
	private String latit;
	private String longit;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Public Places Detail");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.public_place_detail_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		judul = (TextView)findViewById(R.id.title);
		add = (TextView)findViewById(R.id.addr);

		try {
			init();
			initilizeMap();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}


	}

	public void init(){

		title = (String)getIntent().getSerializableExtra("TITLE");
		addr =  (String)getIntent().getSerializableExtra("ADDR");
		latit = (String)getIntent().getSerializableExtra("LAT");
		longit = (String)getIntent().getSerializableExtra("LON");

		latitude = Double.parseDouble(latit);
		longitude = Double.parseDouble(longit);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			init();
			initilizeMap();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		LatLng newLatLong = new LatLng(latitude, longitude);
		MarkerOptions marker = new MarkerOptions().position(newLatLong)
				.title(title).snippet(addr);
		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_publicplace));
		map.addMarker(marker);

		judul.setText(title);
		add.setText("Alamat: "+addr);

	}

	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		map = mapFrag.getMap();
//
//		CameraPosition cameraPosition = new CameraPosition.Builder().target(
//				new LatLng(latitude, longitude)).zoom(18).build();
//
//		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String title2,String addr2, String latt, String lonn ) {
		Intent intent = new Intent(context, PublicPlaceDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("TITLE", title2);
		b.putString("ADDR", addr2);
		b.putString("LAT", latt);
		b.putString("LON", lonn);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
