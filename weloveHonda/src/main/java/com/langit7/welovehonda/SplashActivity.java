package com.langit7.welovehonda;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Config;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import static com.langit7.welovehonda.util.Credential.decode;

public class SplashActivity extends Activity {
	private Thread splashThread;
	private int splashTime = 4000;
	private boolean isShowing = true;
	private Context context;

	private GoogleCloudMessaging gcm;
	String regId;

	public static final String REG_ID = "regId";
	private static final String APP_VERSION = "appVersion";

	protected static final String TAG = SplashActivity.class.getName();
	private String testUser = "testUser123/qwerty";

	private Boolean networkSlow = false;

	String messageLogin = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.splash);
		context = this;




		// startService(new Intent(this, ServiceNotif.class));

		ImageView imageSplash = (ImageView) findViewById(R.id.imageSplash);

		imageSplash.setImageResource(R.drawable.splash);

		Animation fading = AnimationUtils.loadAnimation(this, R.anim.fade);
		imageSplash.startAnimation(fading);

		regId = registerGCM();

		startSplash();

	}

	private synchronized void startSplash() {
		if (splashThread == null) {
			splashThread = new Thread() {
				@Override
				public void run() {

					try {
						int waited = 0;
						while (isShowing && waited < splashTime) {
							sleep(1000);
							waited += 1000;
						}
					} catch (InterruptedException e) {
						// do nothing
						e.printStackTrace();
						System.out.println("what happen here???");
					} finally {
						SplashActivity.this.finish();
						toHome();
						stopSplash();
					}
				}
			};
			splashThread.start();
		}
	}

	public synchronized void stopSplash() {
		if (splashThread != null) {
			Thread thread = splashThread;
			splashThread = null;
			thread.interrupt();
		}
	}

	private void toHome() {
		DashboardActivity.show(this);
	}

	public String registerGCM() {

		gcm = GoogleCloudMessaging.getInstance(this);
		regId = getRegistrationId(context);
		if (TextUtils.isEmpty(regId)) {
			new registerInBackground().execute();
			Log.d("RegisterActivity",
					"registerGCM - successfully registered with GCM server - regId: "
							+ regId);
		} else {
//			Toast.makeText(getApplicationContext(),
//					"RegId already available. RegId: " + regId,
//					Toast.LENGTH_LONG).show();
		}
		return regId;
	}

	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getSharedPreferences(
				SplashActivity.class.getSimpleName(), Context.MODE_PRIVATE);
		String registrationId = prefs.getString(REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		int registeredVersion = prefs.getInt(APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	public class registerInBackground extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				regId = gcm.register(Config.GOOGLE_PROJECT_ID);
				Log.d("RegisterActivity", "registerInBackground - regId: "
						+ regId);
				msg = "Device registered, registration ID=" + regId;

				storeRegistrationId(context, regId);
				// Credential.saveCredentialData(getApplicationContext(),
				// "gcmid",
				// regId);
				// regId = Credential.getCredentialData(getApplicationContext(),
				// "gcmid");
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
				Log.d("RegisterActivity", "Error: " + msg);
			}
			Log.d("RegisterActivity", "AsyncTask completed: " + msg);
			return msg;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			new RegIdTask().execute();
		}

	}

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getSharedPreferences(
				SplashActivity.class.getSimpleName(), Context.MODE_PRIVATE);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(REG_ID, regId);
		editor.putInt(APP_VERSION, appVersion);
		editor.commit();
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			Log.d("LoginActivity",
					"I never expected this! Going down, going down!" + e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			isShowing = false;
		}
		return super.onTouchEvent(event);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		return;
	}

	public class RegIdTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return false;
			}
			processLogin(regId);
			return true;
		}
	}

	public void processLogin(String reg_id) {

		InputStream is;
		try {
			is = postDataLogin(BaseID.URL_REG_ID, reg_id);
			if (is == null) {
				networkSlow = true;
			} else {
				parsing(is);
				networkSlow = false;
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private InputStream postDataLogin(String url, String reg_id) {
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("reg_id", reg_id));
		GetInputStream getIS = new GetInputStream(SplashActivity.this);
		is = getIS.postInputStream(url, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws JSONException, IOException {

		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		Log.e("insStr", "insStr= " + insStr);

		if (insStr.equals("[]")) {
			setMessageLogin("Login Failed");
		} else if (insStr.contains("error: ")) {
			setMessageLogin(insStr);
		} else if (insStr.equals("user already exist")) {
			setMessageLogin("User Already Exist");
			setMessageLogin("0");
		} else {
			setMessageLogin("OK");
			Log.e("sessid", "sessid");
		}
	}

	public String getMessageLogin() {
		return messageLogin;
	}

	public void setMessageLogin(String messageLogin) {
		this.messageLogin = messageLogin;
	}

}
