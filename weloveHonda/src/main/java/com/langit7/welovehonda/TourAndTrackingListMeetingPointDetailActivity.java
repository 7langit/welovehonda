package com.langit7.welovehonda;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.langit7.welovehonda.parse.ParseNews;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

public class TourAndTrackingListMeetingPointDetailActivity extends FragmentActivity{

	public GoogleMap map;
	double latitude;
	double longitude;
	String title;
	private TextView judul;
	private int position;
	private String insStr;
	ArrayList<NewsObject>listForumObject;
	ArrayList<NewsDetailObject> listDetailObject;
	ArrayList<CataloguDetail2Object> listLatlon;
	private int position2;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour And Tracking");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_tracking_map_detail_layout);
		judul = (TextView)findViewById(R.id.title2);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====
		
	}
	
	public void init(){
		title = (String)getIntent().getSerializableExtra("TITLE");
		judul.setText(title);
		position = (int) getIntent().getIntExtra("POS", 0);
		position2 = Credential.getCredentialDataInt(this, BaseID.KEY_DATA_POSITION);
		
		listForumObject = new ArrayList<NewsObject>();
		listDetailObject = new ArrayList<NewsDetailObject>();
		listLatlon = new ArrayList<CataloguDetail2Object>();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		try {
			init();
			initilizeMap();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onResume();
	}

	public void showView(){
		
		insStr = Credential.getCredentialData(this, BaseID.KEY_DATA_LIST_EVENT);
		ParseNews parsingForum = new ParseNews();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
			listDetailObject = listForumObject.get(0).getListDetailObject();
			listLatlon = listDetailObject.get(position2).getListLatLonObject();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		latitude = Double.parseDouble(listLatlon.get(position).getName());
		longitude = Double.parseDouble(listLatlon.get(position).getDesc());

		LatLng newLatLong = new LatLng(latitude, longitude);
		MarkerOptions marker = new MarkerOptions().position(newLatLong)
				.title(title);
		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_placeplainred480x800));
		map.addMarker(marker);

	}

	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		map = mapFrag.getMap();
//
//		CameraPosition cameraPosition = new CameraPosition.Builder().target(
//				new LatLng(latitude, longitude)).zoom(18).build();
//
//		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String title2,int pos) {
		Intent intent = new Intent(context, TourAndTrackingListMeetingPointDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("TITLE", title2);
		b.putInt("POS", pos);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
