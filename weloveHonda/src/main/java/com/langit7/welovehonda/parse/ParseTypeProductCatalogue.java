package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.SubTypeProductCatalogueObject;
import com.langit7.welovehonda.object.TypeProductCatalogueObject;

public class ParseTypeProductCatalogue {
	String xs;
	String type_cat;
	String icon_cat;
	String items_cat;
	String nid;
	String name;
	String image;
	String link_detail;
	String desc;
	Integer comment_count;

	ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects = new ArrayList<TypeProductCatalogueObject>();
	ArrayList<SubTypeProductCatalogueObject> subTypeProductCatalogueObjects = new ArrayList<SubTypeProductCatalogueObject>();

	public void fethingData(String json) throws JSONException {
		JSONObject jObject = new JSONObject(json);

		if (jObject.has("xs")) {
			setXs(jObject.optString("xs"));
		}

		if (jObject.has("data")) {
			JSONArray jArData = jObject.getJSONArray("data");
			for (int j = 0; j < jArData.length(); j++) {
				JSONObject jObData = jArData.getJSONObject(j);
				if (jObData.has("type_cat")) {
					type_cat = jObData.optString("type_cat");
				}
				if (jObData.has("icon_cat")) {
					icon_cat = jObData.optString("icon_cat");
				}
				if (jObData.has("items_cat")) {
					items_cat = jObData.optString("items_cat");
					JSONArray itemArrayItems = new JSONArray(items_cat);
					subTypeProductCatalogueObjects = new ArrayList<SubTypeProductCatalogueObject>();
					for (int k = 0; k < itemArrayItems.length(); k++) {
						JSONObject jObItems = itemArrayItems.getJSONObject(k);
						if (jObItems.has("nid")) {
							nid = jObItems.optString("nid");
						}
						if (jObItems.has("tid")) {
							nid = jObItems.optString("tid");
						}
						if (jObItems.has("name")) {
							name = jObItems.optString("name");
						}
						if (jObItems.has("title")) {
							name = jObItems.optString("title");
						}
						if (jObItems.has("image")) {
							image = jObItems.optString("image");
						}
						if (jObItems.has("link_detail")) {
							link_detail = jObItems.optString("link_detail");
						}
						if (jObItems.has("link")) {
							link_detail = jObItems.optString("link");
						}
						if (jObItems.has("detail_link")) {
							link_detail = jObItems.optString("detail_link");
						}
						
						if (jObItems.has("desc")) {
							desc = jObItems.optString("desc");
						}
						
						if (jObItems.has("comment_count")) {
							comment_count = jObItems.optInt("comment_count");
						}
						
						SubTypeProductCatalogueObject subTypeProductCatalogueObject = new SubTypeProductCatalogueObject(
								nid, name, image, link_detail, desc, comment_count);
						subTypeProductCatalogueObjects
								.add(subTypeProductCatalogueObject);
						setSubTypeProductCatalogueObjects(subTypeProductCatalogueObjects);
					}
				}

				TypeProductCatalogueObject typeProductCatalogueObject = new TypeProductCatalogueObject(
						type_cat, icon_cat, items_cat,
						subTypeProductCatalogueObjects);
				typeProductCatalogueObjects.add(typeProductCatalogueObject);

			}
		}
		setTypeProductCatalogueObjects(typeProductCatalogueObjects);
	}

	public String getType_cat() {
		return type_cat;
	}

	public void setType_cat(String type_cat) {
		this.type_cat = type_cat;
	}

	public String getIcon_cat() {
		return icon_cat;
	}

	public void setIcon_cat(String icon_cat) {
		this.icon_cat = icon_cat;
	}

	public String getItems_cat() {
		return items_cat;
	}

	public void setItems_cat(String items_cat) {
		this.items_cat = items_cat;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLink_detail() {
		return link_detail;
	}

	public void setLink_detail(String link_detail) {
		this.link_detail = link_detail;
	}

	public ArrayList<TypeProductCatalogueObject> getTypeProductCatalogueObjects() {
		return typeProductCatalogueObjects;
	}

	public void setTypeProductCatalogueObjects(
			ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects) {
		this.typeProductCatalogueObjects = typeProductCatalogueObjects;
	}

	public ArrayList<SubTypeProductCatalogueObject> getSubTypeProductCatalogueObjects() {
		return subTypeProductCatalogueObjects;
	}

	public void setSubTypeProductCatalogueObjects(
			ArrayList<SubTypeProductCatalogueObject> subTypeProductCatalogueObjects) {
		this.subTypeProductCatalogueObjects = subTypeProductCatalogueObjects;
	}

	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs = xs;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getComment_count() {
		return comment_count;
	}

	public void setComment_count(Integer comment_count) {
		this.comment_count = comment_count;
	}

}
