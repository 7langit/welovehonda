package com.langit7.welovehonda.parse;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.TwitterTimelineObject;

public class ParseTwitter {
	ArrayList<TwitterTimelineObject> listTwitterTimelineObject;
	
	public ParseTwitter(){
		listTwitterTimelineObject = new ArrayList<TwitterTimelineObject>();
	}

	public ArrayList<TwitterTimelineObject> getArrayListObject(String jsonStr) throws JSONException{
		
		String text = "";
		String id = "";
		String urlProfileImage = "";
		String date = "";
		
		JSONArray jsonArr = new JSONArray(jsonStr);
		JSONObject jsonObj;
		JSONObject jsonUserObj;
		
		for(int i=0;i<jsonArr.length();i++){
			jsonObj = jsonArr.getJSONObject(i);
			if(jsonObj.has("text")){
				text = jsonObj.getString("text").toString();
			}
			if(jsonObj.has("id")){
				id = jsonObj.getString("id").toString();
			}
			if(jsonObj.has("user")){
				jsonUserObj = jsonObj.getJSONObject("user");
				if(jsonUserObj.has("profile_image_url")){
					urlProfileImage = jsonUserObj.getString("profile_image_url").toString();
				}
			}
			if(jsonObj.has("created_at")){
				date = jsonObj.getString("created_at").toString();
			}
			
			StringTokenizer tokens = new StringTokenizer(date, "+");
			date = tokens.nextToken();
			
			TwitterTimelineObject twitterTimelineObject = new TwitterTimelineObject(id, urlProfileImage, date, text);
			listTwitterTimelineObject.add(twitterTimelineObject);
		}
		
		return listTwitterTimelineObject;
	}
}