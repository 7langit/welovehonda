package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.LatLonObject2;
import com.langit7.welovehonda.object.LatLonObject3;

public class ParseLatLon2 {

	ArrayList<LatLonObject2> listLatLonObject;
	ArrayList<LatLonObject3>listLatlonObject2;
	private String uid;
	private String gid;
	private String status;
	private String trackid;
	private String datetime;
	private String latlon;
	private ArrayList<CataloguDetail2Object> listlatLondetail;
	private String lat2;
	private String lon2;

	public ParseLatLon2() {
		listLatLonObject = new ArrayList<LatLonObject2>();
	}

	public ArrayList<LatLonObject2> getArrayListObject(String json) throws JSONException {

		JSONArray itemArray = new JSONArray(json);

		for (int i = 0; i < itemArray.length(); i++) {

			JSONObject jOb = itemArray.getJSONObject(i);


			if (jOb.has("uid")) {
				uid = jOb.optString("uid");
			}

			if (jOb.has("gid")) {
				gid = jOb.optString("gid");
			}

			if (jOb.has("status")) {
				status = jOb.optString("status");
			}

			if (jOb.has("trackid")) {
				trackid = jOb.optString("trackid");
			}

			if (jOb.has("datetime")) {
				datetime = jOb.optString("datetime");
			}
			
			if (jOb.has("latlon")) {
				latlon = jOb.optString("latlon");
				JSONArray itemArray4 = new JSONArray(latlon);
				listlatLondetail = new ArrayList<CataloguDetail2Object>();
				for (int k = 0; k < itemArray4.length(); k++) {
					JSONObject jOb4 = itemArray4.getJSONObject(k);

					if (jOb4.has("lat")) {
						lat2 = jOb4.optString("lat");
					}

					if (jOb4.has("lon")) {
						lon2 = jOb4.optString("lon");
					}

					CataloguDetail2Object catDetail2 = new CataloguDetail2Object(lat2, lon2);
					listlatLondetail.add(catDetail2);
				}
			}

			LatLonObject2 resObject = new LatLonObject2(uid, trackid, datetime, gid, status,listlatLondetail);
			listLatLonObject.add(resObject);
		}

		return listLatLonObject;
	}
}


