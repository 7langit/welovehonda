package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.EventObject;

public class ParseEvent {
	ArrayList<EventObject> listRespondObject;
	private String ongoing;
	private String expired;

	public ParseEvent() {
		listRespondObject = new ArrayList<EventObject>();
	}

	public ArrayList<EventObject> getArrayListRespondObject(String json) throws JSONException {
		

		JSONObject jOb = new JSONObject(json);

			if (jOb.has("ongoing")) {
				ongoing = jOb.optString("ongoing");
			}

			if (jOb.has("expired")) {
				expired = jOb.optString("expired");
			}
			
			EventObject resObject = new EventObject(ongoing,expired);
			listRespondObject.add(resObject);

		return listRespondObject;
	}
}

