package com.langit7.welovehonda.parse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.CatalogueDetail3Object;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;

public class ParseNews {

	ArrayList<NewsObject> listNewsObjects;
	ArrayList<NewsDetailObject> listDetailObjects;

	private String xs;
	private String data;
	private String nid;
	private String title;
	private String image;
	private String date;
	private String body;
	private String images;
	private String price;
	private String ket;
	private String tid;
	private String linkdetail;
	private String name;
	private String link;
	private String info;
	private String uid;
	private String admin;
	private String password;
	private String event;
	private String place;
	private String place2;
	private String latlon;
	private String lat2;
	private String lon2;
	private String gid;
	private ArrayList<CataloguDetail2Object> listlatLondetail;
	private ArrayList<CatalogueDetail3Object> listplace;

	private String product_price_image;
	private ArrayList<String> list_image =null;

	public ParseNews(){
		listNewsObjects = new ArrayList<>();
	}

	public ArrayList<NewsObject> getArrayListObject(String json) throws JSONException {


		JSONObject jOb = new JSONObject(json);

		if (jOb.has("xs")) {
			xs = jOb.optString("id");
		}

		if (jOb.has("data")) {
			data = jOb.optString("data");
			listDetailObjects = new ArrayList<NewsDetailObject>();
			JSONArray itemArray2 = new JSONArray(data);
			for (int j = 0; j < itemArray2.length(); j++) {

				JSONObject jOb2 = itemArray2.getJSONObject(j);

				if (jOb2.has("nid")) {
					nid = jOb2.optString("nid");
				}

				if (jOb2.has("gid")) {
					gid = jOb2.optString("gid");
				}

				if (jOb2.has("link_detail")) {
					linkdetail = jOb2.optString("link_detail");
				}

				if (jOb2.has("info")) {
					info = jOb2.optString("info");
				}

				if (jOb2.has("uid")) {
					uid = jOb2.optString("uid");
				}

				if (jOb2.has("admin")) {
					admin = jOb2.optString("admin");
				}

				if (jOb2.has("password")) {
					password = jOb2.optString("password");
				}

				if (jOb2.has("event")) {
					event = jOb2.optString("event");
				}

				if (jOb2.has("tid")) {
					tid = jOb2.optString("tid");
				}

				if (jOb2.has("title")) {
					title = jOb2.optString("title");
				}

				if (jOb2.has("name")) {
					name = jOb2.optString("name");
				}

				if (jOb2.has("image")) {
					image = jOb2.optString("image");
				}

				if (jOb2.has("date")) {
					date = jOb2.optString("date");
				}

				if (jOb2.has("body")) {
					body = jOb2.optString("body");
				}

				if (jOb2.has("images")) {
					images = jOb2.optString("images");
					//					Log.d("pic","pic"+images);
				}

				if (jOb2.has("price2")) {
					price = jOb2.optString("price2");
				}

				if (jOb2.has("place")) {
					place = jOb2.optString("place");
					listplace = new ArrayList<CatalogueDetail3Object>();
					JSONArray itemArray3 = new JSONArray(place);
					for (int k = 0; k < itemArray3.length(); k++) {
						JSONObject jOb3 = itemArray3.getJSONObject(k);

						if (jOb3.has("place")) {
							place2 = jOb3.optString("place");
						}

						Log.d("place","place"+place2);
						CatalogueDetail3Object placedetail = new CatalogueDetail3Object(place2, "", "");
						listplace.add(placedetail);
					}
				}

				if(jOb2.has("image_product_price")){
					try{
						list_image = new ArrayList<>();
						list_image.clear();
						product_price_image = jOb2.optString("image_product_price");
						JSONArray item = new JSONArray(product_price_image);
						for(int i=0; i<item.length(); i++){
							list_image.add(item.get(i).toString());
						}

					}catch (Exception e){
						e.printStackTrace();
					}

				}

				if (jOb2.has("latlon")) {
					latlon = jOb2.optString("latlon");
					JSONArray itemArray4 = new JSONArray(latlon);
					listlatLondetail = new ArrayList<CataloguDetail2Object>();
					for (int k = 0; k < itemArray4.length(); k++) {
						JSONObject jOb4 = itemArray4.getJSONObject(k);

						if (jOb4.has("lat")) {
							lat2 = jOb4.optString("lat");
						}

						if (jOb4.has("lon")) {
							lon2 = jOb4.optString("lon");
						}

						CataloguDetail2Object catDetail2 = new CataloguDetail2Object(lat2, lon2);
						listlatLondetail.add(catDetail2);
					}
				}

				if (jOb2.has("ket")) {
					ket = jOb2.optString("ket");
				}

				if (jOb2.has("link")) {
					link = jOb2.optString("link");
				}
				NewsDetailObject newsDetail = new NewsDetailObject(image, title, body, date, nid, ket, images, price, tid, link, name, linkdetail, info, uid, admin, password, event, place2, lat2, lon2,gid, listlatLondetail,listplace);
//				newsDetail.setList_image(list_image);

				listDetailObjects.add(newsDetail);
				listDetailObjects.get(j).setList_image(list_image);
			}
		}

		NewsObject newsObject = new NewsObject(xs, listDetailObjects);
		listNewsObjects.add(newsObject);
		return listNewsObjects;
	}

}
