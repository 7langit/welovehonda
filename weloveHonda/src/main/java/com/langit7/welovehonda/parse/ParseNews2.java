package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.NewsObject1;
import com.langit7.welovehonda.object.NewsObject2;

public class ParseNews2 {

	ArrayList<NewsObject1> listNewsObjects;
	ArrayList<NewsObject2> listDetailObjects;
	ArrayList<String> listDetailObjects2;

	private String xs;
	private String post;
	private String title;
	private String url;
	private String content;
	private String attach;
	private String img;

	public ParseNews2(){
		listNewsObjects = new ArrayList<NewsObject1>();
	}

	public ArrayList<NewsObject1> getArrayListObject(String json) throws JSONException {


		JSONObject jOb = new JSONObject(json);

		if (jOb.has("count")) {
			xs = jOb.optString("count");
		}

		if (jOb.has("posts")) {
			post = jOb.optString("posts");
			listDetailObjects = new ArrayList<NewsObject2>();
			JSONArray itemArray2 = new JSONArray(post);
			for (int j = 0; j < itemArray2.length(); j++) {

				JSONObject jOb2 = itemArray2.getJSONObject(j);

				if (jOb2.has("title")) {
					title = jOb2.optString("title");
				}

				if (jOb2.has("url")) {
					url = jOb2.optString("url");
				}

				if (jOb2.has("content")) {
					content = jOb2.optString("content");
				}

				if (jOb2.has("attachments")) {
					attach = jOb2.optString("attachments");

					JSONArray itemArray3 = new JSONArray(attach);
					for (int k = 0; k < itemArray3.length(); k++) {
						JSONObject jOb3 = itemArray3.getJSONObject(k);

						if (jOb3.has("url")) {
							img = jOb3.optString("url");
						}
					}
				}

				NewsObject2 newsDetail = new NewsObject2(url, title, content, img);
				listDetailObjects.add(newsDetail);
			}
		}

		NewsObject1 newsObject = new NewsObject1(xs, listDetailObjects);
		listNewsObjects.add(newsObject);

		return listNewsObjects;
	}

}
