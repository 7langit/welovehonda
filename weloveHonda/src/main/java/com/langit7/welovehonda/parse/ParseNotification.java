package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.NotifDetailsObject;

public class ParseNotification {
	
	ArrayList<NotifDetailsObject> notifObjects;
	
	private String xs;
	private String post;
	private String nid;
	private String title;
	private String image_url;
	private String body;
	private String img;
	private String date;
	private String link_detail;

	public ParseNotification(){
		notifObjects = new ArrayList<NotifDetailsObject>();
	}

	public void fethingData(String json) throws JSONException {


		JSONObject jOb = new JSONObject(json);

		if (jOb.has("xs")) {
			xs = jOb.optString("xs");
		}

		if (jOb.has("data")) {
			post = jOb.optString("data");
			notifObjects = new ArrayList<NotifDetailsObject>();
			JSONArray itemArray2 = new JSONArray(post);
			for (int j = 0; j < itemArray2.length(); j++) {

				JSONObject jOb2 = itemArray2.getJSONObject(j);
				
				if (jOb2.has("nid")) {
					nid = jOb2.optString("nid");
				}

				if (jOb2.has("title")) {
					title = jOb2.optString("title");
				}
				
				if (jOb2.has("date")) {
					date = jOb2.optString("date");
				}
				
				if (jOb2.has("body")) {
					body = jOb2.optString("body");
				}

				if (jOb2.has("image_url")) {
					image_url = jOb2.optString("image_url");
				}
				
				if (jOb2.has("link_detail")) {
					link_detail = jOb2.optString("link_detail");
				}
				
//				edit
				NotifDetailsObject notifObject = new NotifDetailsObject(nid, title, body, image_url, date, image_url);
				notifObjects.add(notifObject);
			}
		}

	}


	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs = xs;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLink_detail() {
		return link_detail;
	}

	public void setLink_detail(String link_detail) {
		this.link_detail = link_detail;
	}

	public ArrayList<NotifDetailsObject> getNotifObjects() {
		return notifObjects;
	}

	public void setNotifObjects(ArrayList<NotifDetailsObject> notifObjects) {
		this.notifObjects = notifObjects;
	}

}
