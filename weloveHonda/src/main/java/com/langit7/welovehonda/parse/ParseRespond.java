package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.RespondObject;

public class ParseRespond {
	ArrayList<RespondObject> listRespondObject;

	public ParseRespond() {
		listRespondObject = new ArrayList<RespondObject>();
	}

	public ArrayList<RespondObject> getArrayListRespondObject(String json) throws JSONException {
		String status = "";
		String message = "";
		String trackid = "";

		JSONObject jOb = new JSONObject(json);

			if (jOb.has("status")) {
				status = jOb.optString("status");
			}

			if (jOb.has("message")) {
				message = jOb.optString("message");
			}
			
			if (jOb.has("trackid")) {
				trackid = jOb.optString("trackid");
			}
			
			RespondObject resObject = new RespondObject(status, message,trackid);
			listRespondObject.add(resObject);
		

		return listRespondObject;
	}
}

