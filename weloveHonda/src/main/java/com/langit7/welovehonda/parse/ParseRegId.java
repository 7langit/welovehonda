package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.MessageObject;

public class ParseRegId {
	
	String status;
	String message;
	ArrayList<MessageObject> messageObjects = new ArrayList<MessageObject>();
	
	public void fethingData(String json) throws JSONException {

		JSONObject jOUser = new JSONObject(json);
		if (jOUser.has("status")) {
			setStatus(jOUser.optString("status"));
		}

		if (jOUser.has("message")) {
			setMessage(jOUser.optString("message"));
		}
		
		
		MessageObject messageObject = new MessageObject(status, message);
		messageObjects.add(messageObject);
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<MessageObject> getMessageObjects() {
		return messageObjects;
	}

	public void setMessageObjects(ArrayList<MessageObject> messageObjects) {
		this.messageObjects = messageObjects;
	}

}
