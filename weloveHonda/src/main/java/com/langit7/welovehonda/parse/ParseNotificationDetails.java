package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.EventObject;
import com.langit7.welovehonda.object.NotifDetailsObject;

public class ParseNotificationDetails {
	ArrayList<NotifDetailsObject> notifDetailsObjects;
	String title;
	String body;
	String image_url;
	String date;

	public ParseNotificationDetails() {
		notifDetailsObjects = new ArrayList<NotifDetailsObject>();
	}

	public ArrayList<NotifDetailsObject> getArrayListRespondObject(String json) throws JSONException {
		
		JSONArray jArr = new JSONArray(json);
		JSONObject jOb = jArr.getJSONObject(0);
		

			if (jOb.has("title")) {
				title = jOb.optString("title");
			}

			if (jOb.has("body")) {
				body = jOb.optString("body");
			}
			
			if (jOb.has("date")) {
				date = jOb.optString("date");
			}
			
			if (jOb.has("image_url")) {
				image_url = jOb.optString("image_url");
			}
			
			NotifDetailsObject notifDetailsObject = new NotifDetailsObject(title, body, image_url, date);
			notifDetailsObjects.add(notifDetailsObject);

		return notifDetailsObjects;
	}
}

