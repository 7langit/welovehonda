package com.langit7.welovehonda.parse;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.FacebookTimelineObject;


public class ParsingTimelineFacebook {
	ArrayList<FacebookTimelineObject> listFacebookTimelineObject;

	public ParsingTimelineFacebook(){
		listFacebookTimelineObject = new ArrayList<FacebookTimelineObject>();
	}

	public ArrayList<FacebookTimelineObject> getArrayListFacebookTimelineObjects(String json) throws JSONException{

		String text = "";
		String id = "";
		String date = "";
		String imageUrl = "";
		//		String urlProfileImage = "";

		JSONObject jsonObj = new JSONObject(json);
		JSONArray jsonArr = jsonObj.getJSONArray("data");

		JSONObject jsonDataObj;
		for(int i=0;i<jsonArr.length();i++){
			jsonDataObj = jsonArr.getJSONObject(i);
			id = jsonDataObj.optString("id");
			text = jsonDataObj.optString("message");
			date = jsonDataObj.optString("created_time");
			imageUrl = jsonDataObj.optString("picture");
			
			StringTokenizer tokens = new StringTokenizer(date, "+");
			date = tokens.nextToken().replace("T", " ");
			
			FacebookTimelineObject facebookTimelineObject = new FacebookTimelineObject(text, id,date,imageUrl);
			listFacebookTimelineObject.add(facebookTimelineObject);
		}

		return listFacebookTimelineObject;
	}
}