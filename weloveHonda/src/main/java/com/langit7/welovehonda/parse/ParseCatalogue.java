package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.CataloguDetailMainFeaturesObject;
import com.langit7.welovehonda.object.CatalogueDetail3Object;
import com.langit7.welovehonda.object.CatalogueDetailObject;
import com.langit7.welovehonda.object.CatalogueObject;

public class ParseCatalogue {
	
	ArrayList<CatalogueObject> listCataObjects;
	ArrayList<CatalogueDetailObject> listDetail2Objects;
	ArrayList<CataloguDetail2Object> listDetail3Objects;
	ArrayList<CatalogueDetail3Object> listDetail4Objects;
	ArrayList<CataloguDetailMainFeaturesObject> cataloguDetailMainFeaturesObjects;
	
	private String xs;
	private String data;
	private String nid;
	private String title;
	private String main;
	private String name;
	private String desc;
	private String image;
	private String color;
	private String icon;
	private String motor;
	private String spare;
	private String acc;
	private String images;
	private String imagescover;
	private String month;
	private String link;
	private String mainFeatures;
	private String description;
	
	public ParseCatalogue(){
		
		listCataObjects = new ArrayList<CatalogueObject>();
	}

	public ArrayList<CatalogueObject> getArrayListObject(String json) throws JSONException {


		JSONObject jOb = new JSONObject(json);

		if (jOb.has("xs")) {
			xs = jOb.optString("xs");
		}

		if (jOb.has("data")) {
			data = jOb.optString("data");
			listDetail2Objects = new ArrayList<CatalogueDetailObject>();
			JSONArray itemArray2 = new JSONArray(data);
			for (int j = 0; j < itemArray2.length(); j++) {
				
				JSONObject jOb2 = itemArray2.getJSONObject(j);

				if (jOb2.has("tid")) {
					nid = jOb2.optString("tid");
				}

				if (jOb2.has("title")) {
					title = jOb2.optString("title");
				}
				
				if (jOb2.has("month")) {
					month = jOb2.optString("month");
				}
				
				if (jOb2.has("images")) {
					images = jOb2.optString("images");
				}
				
				if (jOb2.has("images_cover")) {
					imagescover = jOb2.optString("images_cover");
				}

				if (jOb2.has("specification")) {
					main = jOb2.optString("specification");
					listDetail3Objects = new ArrayList<CataloguDetail2Object>();
					JSONArray itemArray3 = new JSONArray(main);
					for (int k = 0; k < itemArray3.length(); k++) {
						
						JSONObject jOb3 = itemArray3.getJSONObject(k);
						
						if (jOb3.has("name")) {
							name = jOb3.optString("name");
						}

						if (jOb3.has("desc")) {
							desc = jOb3.optString("desc");
						}
						
						CataloguDetail2Object catDetail2 = new CataloguDetail2Object(name, desc);
						listDetail3Objects.add(catDetail2);
					}
				}
				
				if (jOb2.has("color")) {
					color = jOb2.optString("color");
					listDetail4Objects = new ArrayList<CatalogueDetail3Object>();
					JSONArray itemArray4 = new JSONArray(color);
					for (int l = 0; l < itemArray4.length(); l++) {
						
						JSONObject jOb4 = itemArray4.getJSONObject(l);
						
						if (jOb4.has("icon")) {
							icon = jOb4.optString("icon");
						}

						if (jOb4.has("motorcycle")) {
							motor = jOb4.optString("motorcycle");
						}
						
						if (jOb4.has("description")) {
							description = jOb4.optString("description");
						}
						
						CatalogueDetail3Object catDetail3 = new CatalogueDetail3Object(icon, motor, description);
						listDetail4Objects.add(catDetail3);
						
					}
				}
				
				if (jOb2.has("main_features")) {
					mainFeatures = jOb2.optString("main_features");
					cataloguDetailMainFeaturesObjects = new ArrayList<CataloguDetailMainFeaturesObject>();
					JSONArray itemArray5 = new JSONArray(mainFeatures);
					for (int k = 0; k < itemArray5.length(); k++) {
						
						JSONObject jOb5 = itemArray5.getJSONObject(k);
						
						if (jOb5.has("name")) {
							name = jOb5.optString("name");
						}

						if (jOb5.has("desc")) {
							desc = jOb5.optString("desc");
						}
						
						if (jOb5.has("image")) {
							image = jOb5.optString("image");
						}
						
						CataloguDetailMainFeaturesObject cataloguDetailMainFeaturesObject = new CataloguDetailMainFeaturesObject(name, desc, image);
						cataloguDetailMainFeaturesObjects.add(cataloguDetailMainFeaturesObject);
					}
				}

				if (jOb2.has("sparepart")) {
					spare = jOb2.optString("sparepart");
				}

				if (jOb2.has("accesories")) {
					acc = jOb2.optString("accesories");
				}
				
				if (jOb2.has("link")) {
					link = jOb2.optString("link");
				}

				CatalogueDetailObject catDetail = new CatalogueDetailObject(nid, title, spare,month, acc,images,imagescover,link, listDetail3Objects, listDetail4Objects, cataloguDetailMainFeaturesObjects);
				listDetail2Objects.add(catDetail);
			}
		}

		CatalogueObject catalogueObject = new CatalogueObject(xs, listDetail2Objects);
		listCataObjects.add(catalogueObject);

		return listCataObjects;
	}

	public ArrayList<CataloguDetailMainFeaturesObject> getCataloguDetailMainFeaturesObjects() {
		return cataloguDetailMainFeaturesObjects;
	}

	public void setCataloguDetailMainFeaturesObjects(
			ArrayList<CataloguDetailMainFeaturesObject> cataloguDetailMainFeaturesObjects) {
		this.cataloguDetailMainFeaturesObjects = cataloguDetailMainFeaturesObjects;
	}

	public String getMainFeatures() {
		return mainFeatures;
	}

	public void setMainFeatures(String mainFeatures) {
		this.mainFeatures = mainFeatures;
	}

}
