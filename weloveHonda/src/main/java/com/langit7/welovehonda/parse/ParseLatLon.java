package com.langit7.welovehonda.parse;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.langit7.welovehonda.object.LatLonObject;

public class ParseLatLon {

	ArrayList<LatLonObject> listLatLonObject;
	private String latlong;
	private String address;
	private String city;
	private String phone;
	private String nid;
	private String title;
	private String lat;
	private String lon;
	private String distance;
	private String tid;
	private String linkdetail;
	private String detail_link;
	private String desc;
	private String comment_count;
	private String cid;
	private String created;
	private String comment;
	private String full_name;
	private String photo;
	private String email;
	private String name;
	private String uid;
	private String username;
	private String gid;
	private String status;
	private String trackid;
	private String datetime;

	public ParseLatLon() {
		listLatLonObject = new ArrayList<LatLonObject>();
	}

	public ArrayList<LatLonObject> getArrayListObject(String json) throws JSONException {

		JSONArray itemArray = new JSONArray(json);

		for (int i = 0; i < itemArray.length(); i++) {

			JSONObject jOb = itemArray.getJSONObject(i);

			if (jOb.has("nid")) {
				nid = jOb.optString("nid");
			}

			if (jOb.has("tid")) {
				tid = jOb.optString("tid");
			}

			if (jOb.has("uid")) {
				uid = jOb.optString("uid");
			}

			if (jOb.has("name")) {
				name = jOb.optString("name");
			}

			if (jOb.has("username")) {
				username = jOb.optString("username");
			}

			if (jOb.has("link_detail")) {
				linkdetail = jOb.optString("link_detail");
			}

			if (jOb.has("gid")) {
				gid = jOb.optString("gid");
			}

			if (jOb.has("status")) {
				status = jOb.optString("status");
			}

			if (jOb.has("trackid")) {
				trackid = jOb.optString("trackid");
			}

			if (jOb.has("datetime")) {
				datetime = jOb.optString("datetime");
			}

			if (jOb.has("desc")) {
				desc = jOb.optString("desc");
			}

			if (jOb.has("comment_count")) {
				comment_count = jOb.optString("comment_count");
			}

			if (jOb.has("cid")) {
				cid = jOb.optString("cid");
			}

			if (jOb.has("created")) {
				created = jOb.optString("created");
			}

			if (jOb.has("comment")) {
				comment = jOb.optString("comment");
			}

			if (jOb.has("title")) {
				title = jOb.optString("title");
			}

			if (jOb.has("full_name")) {
				full_name = jOb.optString("full_name");
			}

			if (jOb.has("photo")) {
				photo = jOb.optString("photo");
			}

			if (jOb.has("address")) {
				address = jOb.optString("address").replace("\r\n ", "\r\n");
			}

			if (jOb.has("detail_link")) {
				detail_link = jOb.optString("detail_link");
			}

			if (jOb.has("phone")) {
				phone = jOb.optString("phone");
			}

			if (jOb.has("email")) {
				email = jOb.optString("email");
			}

			if (jOb.has("city")) {
				city = jOb.optString("city");
			}

			if(jOb.has("latlong")) {
				latlong = jOb.optString("latlong");
				JSONArray itemArray2 = new JSONArray(latlong);
				for (int j = 0; j < itemArray2.length(); j++) {
					lat = itemArray2.get(0).toString();
					lon = itemArray2.get(1).toString();

				}
			}

			if (jOb.has("distance")) {
				distance = jOb.optString("distance");
			}

			LatLonObject resObject = new LatLonObject(nid, title, address, phone, city, lat, lon, distance, tid, linkdetail, detail_link, cid, comment_count, comment, created, desc, full_name, email, photo, name, uid, username, trackid, datetime, gid, status);
			listLatLonObject.add(resObject);

		}


		return listLatLonObject;
	}
}


