package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterTrackingList;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.langit7.welovehonda.parse.ParseNews;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class TourAndTrackingListActivity extends Activity{

	private Button buttHowTo;
	private Button buttListTrack;
	public ProgressBar pr;
	private String KEY_DATA;
	private SharedPreferences prefData;
	ArrayList<NewsObject>listForumObject;
	ArrayList<NewsDetailObject> listDetailObject;
	private boolean networkSlow;
	private ListAdapterTrackingList listMenu;
	private ListView listViewTrack;
	private ImageButton buttLogout;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking List Page");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_tracking_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		Credential.saveCredentialData(this, BaseID.KEY_DATA_LOGIN, "1");

		buttHowTo = (Button)findViewById(R.id.howtotrack);
		buttLogout = (ImageButton)findViewById(R.id.logout);
		buttListTrack = (Button)findViewById(R.id.listgroup);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttListTrack.setBackgroundResource(R.drawable.button_listgroupf480);
		listViewTrack = (ListView)findViewById(R.id.listView);

		buttHowTo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				TourAndTrackingHowActivity.show(TourAndTrackingListActivity.this);
			}
		});

		buttLogout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Credential.saveCredentialData(TourAndTrackingListActivity.this, BaseID.KEY_DATA_LOGIN, "");
				TourAndTrackingLoginActivity.show(TourAndTrackingListActivity.this);
				finish();
			}
		});
	}

	public void init(){

		KEY_DATA = BaseID.KEY_DATA_FORUM_TRACK;
		prefData = getPreferences(MODE_PRIVATE);
		listForumObject = new ArrayList<NewsObject>();
		listDetailObject = new ArrayList<NewsDetailObject>();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		init();
		new OpenData().execute();
		super.onResume();
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(BaseID.URL_TRACK_LIST);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);
		Credential.saveCredentialData(this, BaseID.KEY_DATA_LIST_EVENT, insStr);
		ParseNews parsingForum = new ParseNews();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
			listDetailObject = listForumObject.get(0).getListDetailObject();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		listMenu = new ListAdapterTrackingList(this, R.layout.listitem, listDetailObject);
		listViewTrack.setAdapter(listMenu);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		String login = Credential.getCredentialData(this, BaseID.KEY_DATA_LOGIN);
		if(login.equals("1")){
			DashboardActivity.show(TourAndTrackingListActivity.this);
		}else{
			RegisterMyActivity.show(TourAndTrackingListActivity.this);
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(TourAndTrackingListActivity.this, "Please try again in a few minutes", Toast.LENGTH_LONG).show();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, TourAndTrackingListActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
