package com.langit7.welovehonda;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.social.FacebookSocial;
import com.langit7.welovehonda.social.Twitter;
import com.langit7.welovehonda.util.BaseID;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

@SuppressLint("NewApi")
public class NewsDetailActivity extends Activity {

	private String title;
	private String content;
	private ProgressBar progressBar2;
	private String messageShareTwitter;
	private Dialog dialogFb;
	private EditText statusFb;
	private String messageShare;
	private TextView titleText;
	private Dialog dialogTweet;
	private TextView tweet;
	private String urlImg;
	private String imgView;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private ImageView imageDetail;
	private int w;
	private int h;
	private BroadcastReceiver logoutReceiver;
	private static SharedPreferences mSharedPreferences;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu News Detail");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_detail_layout);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		w = displaymetrics.widthPixels;
		h = displaymetrics.heightPixels;

		mSharedPreferences = getSharedPreferences(BaseID.PREFERENCE_NAME,
				MODE_PRIVATE);

		titleText = (TextView) findViewById(R.id.title);
		imageDetail = (ImageView) findViewById(R.id.imagenew);

		try {
			init();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			init();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean isConnected() {
		return mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, null) != null;
	}

	public void init() {

		title = (String) getIntent().getSerializableExtra("TITLE");
		content = (String) getIntent().getSerializableExtra("CONTENT");
		urlImg = (String) getIntent().getSerializableExtra("URL");
		imgView = (String) getIntent().getSerializableExtra("IMG");

		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder().cacheOnDisc(true)
				.showImageOnLoading(R.drawable.imagedefault_slidescreen)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		progressBar2 = (ProgressBar) findViewById(R.id.progressBar);

		messageShare = "I am reading \"" + title + "\" on WeLoveHondaApp";
		messageShareTwitter = "I am reading \"" + title
				+ "\" on WeLoveHondaApp" + "\r\n" + urlImg;

	}

	@SuppressWarnings("deprecation")
	private void settingWebViewClient(WebView webview, String body) {

		String contentAwal = "<html><head><style type='text/css'>html,body {;text-align:left;} .rtecenter{ text-align:center; }</style></head><body><p>"
				+ "<body width=100% leftmargin='2' rightmargin='2' topmargin='2' marginwidth='0' marginheight='0'>";

		String contentTengah = body;
		String contentAkhir = "</body></html>";
		String content1 = contentAwal + contentTengah + contentAkhir;

		WebView webViewContent = webview;
		webViewContent.clearView();
		webViewContent.setHorizontalScrollBarEnabled(false);
		webViewContent.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				if (progressBar2.getVisibility() == View.VISIBLE) {
					progressBar2.setVisibility(View.INVISIBLE);
				}
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
				return true;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
			}

		});
		webViewContent.loadDataWithBaseURL(null, content1, "text/html",
				"utf-8", null);
	}

	private void showView() {

		titleText.setText(title);

		imageLoader.displayImage(imgView, imageDetail, options);
		WebView webview = (WebView) findViewById(R.id.webView);
		settingWebViewClient(webview, content);

		Button imageButtonFacebookShare = (Button) findViewById(R.id.buttonfb);
		imageButtonFacebookShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Facebook();
			}
		});

		Button imageButtonTwitterShare = (Button) findViewById(R.id.buttontwit);
		imageButtonTwitterShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Twitter();
			}
		});
	}

	public void Facebook() {

		dialogFb = new Dialog(NewsDetailActivity.this,
				R.style.Theme_Transparent);
		dialogFb.setContentView(R.layout.dialog_field_and_2button);
		dialogFb.getWindow().setLayout(w, h);
		statusFb = (EditText) dialogFb.findViewById(R.id.tweet);
		statusFb.setPadding(10, 10, 10, 10);
		statusFb.setLines(3);

		statusFb.setText(messageShare);
		RelativeLayout areaAdapter = (RelativeLayout) dialogFb
				.findViewById(R.id.area_dialog);

		statusFb.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(statusFb.getWindowToken(), 0);
					return true;
				}
				return false;
			}
		});

		TextView textPositive = (TextView) dialogFb
				.findViewById(R.id.text_positive);
		textPositive.setText("Share");
		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FacebookSocial.show(NewsDetailActivity.this, statusFb.getText()
						.toString(), urlImg);
				dialogFb.dismiss();
			}
		});

		TextView textNegative = (TextView) dialogFb
				.findViewById(R.id.text_negative);
		textNegative.setText("Cancel");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogFb.dismiss();
			}
		});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogFb.dismiss();
			}
		});

		dialogFb.show();
	}

	public void Twitter() {

		dialogTweet = new Dialog(NewsDetailActivity.this,
				R.style.Theme_Transparent);
		dialogTweet.setContentView(R.layout.dialog_field_and_2button);
		dialogTweet.getWindow().setLayout(w, h);

		tweet = (EditText) dialogTweet.findViewById(R.id.tweet);
		tweet.setText(messageShareTwitter);
		tweet.setPadding(10, 10, 10, 10);
		tweet.setLines(3);

		RelativeLayout areaAdapter = (RelativeLayout) dialogTweet
				.findViewById(R.id.area_dialog);

		tweet.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(tweet.getWindowToken(), 0);
					return true;
				}
				return false;
			}
		});

		TextView textPositive = (TextView) dialogTweet
				.findViewById(R.id.text_positive);
		textPositive.setText("Share");
		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String status = tweet.getText().toString();
				if (status.length() < 141) {
					if (isConnected()) {
						try {
							Twitter.show(NewsDetailActivity.this, status, "1");
							dialogTweet.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
					} else {
						try {
							Twitter.show(NewsDetailActivity.this, status, "1");
							dialogTweet.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Status Is Over 140 Characters", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

		TextView textNegative = (TextView) dialogTweet
				.findViewById(R.id.text_negative);
		textNegative.setText("Cancel");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogTweet.dismiss();
			}
		});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogTweet.dismiss();
			}
		});
		dialogTweet.show();
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String title, String content,
			String url, String img2) {
		Intent intent = new Intent(context, NewsDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("TITLE", title);
		b.putString("CONTENT", content);
		b.putString("URL", url);
		b.putString("IMG", img2);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.slide_in_right,
				R.anim.slide_out_left);
	}

}
