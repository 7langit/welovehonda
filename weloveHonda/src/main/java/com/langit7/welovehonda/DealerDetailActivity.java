package com.langit7.welovehonda;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.langit7.welovehonda.API.APIServices;
import com.langit7.welovehonda.adapter.ViewPagerAdapter;
import com.langit7.welovehonda.object.dealer;
import com.langit7.welovehonda.object.dealer_type;
import com.langit7.welovehonda.object.listdata;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pance on 10/26/17.
 */

public class DealerDetailActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{

    private TabLayout tabs;
    private ViewPager viewPager;

    private ViewPagerAdapter viewPagerAdapter;

    List<dealer> ls1, ls2, ls3;

    List<dealer_type> dealertypes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealerdetail);
        tabs = (TabLayout)findViewById(R.id.tabs);
        viewPager = (ViewPager)findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(this);
//        viewpager.setCurrentItem(2);

        tabs.setupWithViewPager(viewPager);

        dealertypes = (List<dealer_type>) getIntent().getSerializableExtra("types");
        if (dealertypes == null) {
            finish();
        }
        ls1 = new ArrayList<>();
        ls2 = new ArrayList<>();
        ls3 = new ArrayList<>();

        APIServices.generateService(this).getDealer("1", getIntent().getStringExtra("tid"), getIntent().getStringExtra("pid"), getIntent().getStringExtra("cid"), new Callback<listdata<dealer>>() {
            @Override
            public void success(listdata<dealer> dealerlistdata, Response response) {

                if (dealerlistdata != null && dealerlistdata.getData() != null && dealerlistdata.getData().size() > 0) {
                    ls1.clear();
                    ls2.clear();
                    ls3.clear();

                    for (dealer d : dealerlistdata.getData()) {
                        if (d.getCategory().getId().equalsIgnoreCase(dealertypes.get(0).getId())) {
                            ls1.add(d);
                        }
                        else if (d.getCategory().getId().equalsIgnoreCase(dealertypes.get(1).getId())) {
                            ls2.add(d);
                        }
                        else if (d.getCategory().getId().equalsIgnoreCase(dealertypes.get(2).getId())) {
                            ls3.add(d);
                        }
                    }

                }
                setupViewPager(viewPager, ls1, ls2, ls3);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });


    }

    private void setupViewPager(ViewPager viewPager, List<dealer> ls1, List<dealer> ls2, List<dealer> ls3){

        RegularFragment regularFragment = RegularFragment.Instantiate(ls1, dealertypes.get(0).getName(), dealertypes.get(0));
        WingFragment wingFragment = WingFragment.Instantiate(ls2, dealertypes.get(1).getName(), dealertypes.get(1));
        BigWingFragment bigWingFragment = BigWingFragment.Instantiate(ls3, dealertypes.get(2).getName(), dealertypes.get(2));
        viewPagerAdapter.addViewPagerAdapter(regularFragment,"Regular");
        viewPagerAdapter.addViewPagerAdapter(wingFragment, "Wing");
        viewPagerAdapter.addViewPagerAdapter(bigWingFragment, "Big Wing");
        viewPager.setAdapter(viewPagerAdapter);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
