package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class PublicPlaceActivity extends FragmentActivity implements OnCameraChangeListener, OnMapReadyCallback{

	boolean networkSlow;
	ArrayList<LatLonObject> listObject;
	ProgressBar pr;
	//	LayananDetailsAdapter mLayananDetailsAdapter;
	public GoogleMap map;
	double latitude;
	double longitude;
	public String url;
	GPSTracker gps;
	private SharedPreferences prefData;
	private String KEY_DATA;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Public Places");
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.public_place_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		pr = (ProgressBar)findViewById(R.id.progressBar);

		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);

		mapFrag.getMapAsync(this);

	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
//		map.setMyLocationEnabled(true);
//
		map = googleMap;
		CameraPosition cameraPosition = new CameraPosition.Builder().target(
				new LatLng(latitude, longitude)).zoom(12).build();
//


		for (int i = 0; i < listObject.size(); i++) {
			String strLat = listObject.get(i).getLat();
			double valueLat = Double.parseDouble(strLat);
			String strLon = listObject.get(i).getLon();
			double valueLon = Double.parseDouble(strLon);

			LatLng newLatLong = new LatLng(valueLat, valueLon);
			MarkerOptions marker = new MarkerOptions().position(newLatLong)
					.title(String.valueOf(i)).snippet(listObject.get(i).getTitle());
			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_publicplace));
			map.addMarker(marker);
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

				@Override
				public void onInfoWindowClick(Marker marker) {

					int pos = Integer.parseInt(marker.getTitle());
					PublicPlaceDetailActivity.show(PublicPlaceActivity.this, listObject.get(pos).getTitle(), listObject.get(pos).getAddress(), listObject.get(pos).getLat(), listObject.get(pos).getLon());
				}
			});
		}

		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//		LatLng sydney = new LatLng(-34, 151);
//		map.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//		map.moveCamera(CameraUpdateFactory.newLatLng(sydney));
	}


	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		mapFrag.getMapAsync(this);
//		map.setMyLocationEnabled(true);
//
//		CameraPosition cameraPosition = new CameraPosition.Builder().target(
//				new LatLng(latitude, longitude)).zoom(12).build();
//
//		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		try {
			init();
			gps();
//			initilizeMap();
			new OpenData().execute();
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
	}

	public void init(){
		url = BaseID.URL_PUBLIC_PLACE;
		prefData = getPreferences(MODE_PRIVATE);

		KEY_DATA = BaseID.KEY_DATA_PUBLIC_PLACE;
		listObject = new ArrayList<LatLonObject>();
	}

	public void gps(){

		gps = new GPSTracker(PublicPlaceActivity.this);

		// check if GPS enabled
		if(gps.canGetLocation()){

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();

			// \n is for new line
			//			Log.d("latlon","lat= "+latitude+"lon= "+longitude);
		}else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("lat", ""+latitude));
		nameValuePairs.add(new BasicNameValuePair("long", ""+longitude));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

		ParseLatLon parsingNews = new ParseLatLon();
		try {
			listObject = parsingNews.getArrayListObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

//		for (int i = 0; i < listObject.size(); i++) {
//			String strLat = listObject.get(i).getLat();
//			double valueLat = Double.parseDouble(strLat);
//			String strLon = listObject.get(i).getLon();
//			double valueLon = Double.parseDouble(strLon);
//
//			LatLng newLatLong = new LatLng(valueLat, valueLon);
//			MarkerOptions marker = new MarkerOptions().position(newLatLong)
//					.title(String.valueOf(i)).snippet(listObject.get(i).getTitle());
//			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_publicplace));
//			map.addMarker(marker);
//			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
//
//				@Override
//				public void onInfoWindowClick(Marker marker) {
//
//					int pos = Integer.parseInt(marker.getTitle());
//					PublicPlaceDetailActivity.show(PublicPlaceActivity.this, listObject.get(pos).getTitle(), listObject.get(pos).getAddress(), listObject.get(pos).getLat(), listObject.get(pos).getLon());
//				}
//			});
//		}
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, PublicPlaceActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub

	}

}
