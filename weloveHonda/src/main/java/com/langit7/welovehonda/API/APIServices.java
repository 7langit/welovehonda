package com.langit7.welovehonda.API;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.langit7.welovehonda.util.Constants;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;


/**
 * Created by donny on 12/8/15.
 */
public class APIServices {

    public static API generateService(final Context ctx){

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);


        //final String token=getPreverence(ctx,"token");
        final String token="d1cf0f986f2aff69b56a5858a4cc9ecec4a00d02";


        Gson gson = new GsonBuilder().disableHtmlEscaping().create();


        //request with retrofit
        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setConverter(new GsonConverter(new GsonBuilder()
//                        .excludeFieldsWithoutExposeAnnotation()
//                        .create()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("API REQUEST"))
                .setConverter(new GsonConverter(gson))
                .setEndpoint(Constants.BASE_URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {

                        if(getPreverence(ctx,"token")!=null &&getPreverence(ctx,"token").length()>0 ) {
                            request.addHeader("Authorization", "JWT " + getPreverence(ctx,"token"));
                        }else if(token!=null && token.length()>0) {
                            request.addHeader("Authorization", "Token " + token);
                        }
                    }
                })
                .setClient(new OkClient(okHttpClient)).build();

        API api;
        api = restAdapter.create(API.class);
        return api;
    }

    public static String getPreverence(Context ctx, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return prefs.getString(key, "");
    }

}
