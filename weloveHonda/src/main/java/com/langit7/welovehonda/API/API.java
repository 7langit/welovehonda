package com.langit7.welovehonda.API;

import com.langit7.welovehonda.object.authentication;
import com.langit7.welovehonda.object.dealer;
import com.langit7.welovehonda.object.dealer_type;
import com.langit7.welovehonda.object.listdata;
import com.langit7.welovehonda.object.nv;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by donny on 12/7/15.
 */
public interface API {


    @GET("/list-dealer/")
    void getDealer(
            @Query("xs") String xs,
            @Query("service_type") String service_type,
            @Query("province") String province,
            @Query("city") String city,
            Callback<listdata<dealer>> res
    );
    @GET("/list-dealer/")
    void getDealer(
            @Query("xs") String xs,
            @Query("category") String category,
            Callback<listdata<dealer>> res
    );



    @GET("/list-province/")
    void getProvince(
            @Query("xs") String xs,
            Callback<listdata<nv>> res
    );
    @GET("/list-city/")
    void getCity(
            @Query("xs") String xs,
            @Query("province") String province,
            Callback<listdata<nv>> res
    );


    @GET("/list-dealer-service/")
    void getDealerService(
            @Query("xs") String xs,
            Callback<listdata<nv>> res
    );

    @FormUrlEncoded
    @POST("/list-inform-dealer/")
    void informDealer(
            @Field("name") String name,
            @Field("email") String email,
            @Field("body") String body,
            @Field("phone") String phone,
            @Field("province") String province,
            @Field("city") String city,
            Callback<nv> res
    );


    @GET("/list-dealer-type/")
    void getDealerType(
            @Query("xs") String xs,
            Callback<listdata<dealer_type>> res
    );

    @FormUrlEncoded
    @POST("/api-token-auth/")
    void login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("fcmid") String fcmid,
            Callback<authentication> res
    );



    @FormUrlEncoded
    @POST("/login-community/")
    void loginComunity(
            @Field("email") String email,
            @Field("hcid") String hcid,
            @Field("fcmid") String fcmid,
            Callback<authentication> res
    );




}
