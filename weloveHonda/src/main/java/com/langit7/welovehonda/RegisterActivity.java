package com.langit7.welovehonda;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.object.RespondObject;
import com.langit7.welovehonda.parse.ParseRespond;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

import eu.janmuller.android.simplecropimage.CropImage;

public class RegisterActivity extends Activity {

	private Button buttonReg;
	private EditText name;
	private EditText username;
	private EditText email;
	private EditText password;
	private TextView uploadPhoto;
	Bitmap photo;
	private String name1;
	private String user1;
	private String email1;
	private String pass1;
	HttpEntity resEntity;
	ArrayList<RespondObject> listRespondObjects;
	ByteArrayOutputStream bao;

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

	public static final int REQUEST_CODE_GALLERY = 0x1;
	public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
	public static final int REQUEST_CODE_CROP_IMAGE = 0x3;

	private File mFileTemp;
	public String insStr;
	private EditText phone;
	protected String phone1;
	private String message;
	private String response_str;
	private String response_str_final;
	private String preffoto;
	private String status;
	private File file1;
	protected String filename;
	private BroadcastReceiver logoutReceiver;
	private Boolean isNotConnected = false;
	private Boolean isExist = false;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registeractivity_layout);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		name = (EditText) findViewById(R.id.name);
		username = (EditText) findViewById(R.id.username);
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		phone = (EditText) findViewById(R.id.phone);
		uploadPhoto = (TextView) findViewById(R.id.uploadtext);
		buttonReg = (Button) findViewById(R.id.buttonreg);

		listRespondObjects = new ArrayList<RespondObject>();

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mFileTemp = new File(Environment.getExternalStorageDirectory(),
					TEMP_PHOTO_FILE_NAME);
		} else {
			mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
		}

		showView();

	}

	public void showView() {

		uploadPhoto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				cropimage();
			}
		});

		buttonReg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				preffoto = Credential.getCredentialData(RegisterActivity.this,
						BaseID.KEY_PREF_REG);

				name1 = name.getText().toString();
				user1 = username.getText().toString();
				email1 = email.getText().toString();
				pass1 = password.getText().toString();
				phone1 = phone.getText().toString();

				if (pass1.equals("") || pass1.length() < 6
						|| pass1.contains(" ")) {
					Toast.makeText(
							RegisterActivity.this,
							"Minimum password 6 karakter dan tidak mengandung spasi",
							Toast.LENGTH_LONG).show();
				} else if (user1.equals("") || user1.contains(" ")) {
					Toast.makeText(
							RegisterActivity.this,
							"Kolom username tidak boleh kosong dan mengandung spasi",
							Toast.LENGTH_SHORT).show();
				} else if (isEmailValid(email1) == false) {
					Toast.makeText(RegisterActivity.this,
							"Format email masih salah", Toast.LENGTH_SHORT)
							.show();
				} else {
					if (preffoto.equals("1")) {
						new OpenDataReg().execute();
					} else {
						// Toast.makeText(RegisterActivity.this,
						// "Please choose your image",
						// Toast.LENGTH_SHORT).show();
						InputStream inputStream = getResources()
								.openRawResource(R.raw.imagedefault2);
						Bitmap bmp = BitmapFactory.decodeStream(inputStream);
						filename = Environment.getExternalStorageDirectory()
								.toString()
								+ File.separator
								+ "welovehonda.jpg";
						FileOutputStream out;
						try {
							out = new FileOutputStream(filename);
							bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
							new OpenDataReg().execute();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}
		});
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public void cropimage() {

		final String[] items = new String[] { "Take from camera",
				"Select from gallery" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, items);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// builder.setTitle("Select Image");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) { // pick from
																	// camera
				if (item == 0) {
					takePicture();
				} else { // pick from file
					openGallery();
				}
			}
		});

		final AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void takePicture() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {
			Uri mImageCaptureUri = null;
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				mImageCaptureUri = Uri.fromFile(mFileTemp);
			} else {
				/*
				 * The solution is taken from here:
				 * http://stackoverflow.com/questions
				 * /10042695/how-to-get-camera-result-as-a-uri-in-data-folder
				 */
				mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
			}
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
					mImageCaptureUri);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
		} catch (ActivityNotFoundException e) {

		}
	}

	private void openGallery() {

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
	}

	private void startCropImage() {

		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);
		intent.putExtra(CropImage.OUTPUT_X, 300);
		intent.putExtra(CropImage.OUTPUT_Y, 300);

		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode != RESULT_OK) {

			return;
		}

		switch (requestCode) {

		case REQUEST_CODE_GALLERY:

			try {

				InputStream inputStream = getContentResolver().openInputStream(
						data.getData());
				FileOutputStream fileOutputStream = new FileOutputStream(
						mFileTemp);
				copyStream(inputStream, fileOutputStream);
				fileOutputStream.close();
				inputStream.close();

				startCropImage();

			} catch (Exception e) {

			}

			break;
		case REQUEST_CODE_TAKE_PICTURE:

			startCropImage();
			break;
		case REQUEST_CODE_CROP_IMAGE:

			String path = data.getStringExtra(CropImage.IMAGE_PATH);
			if (path == null) {

				return;
			}

			photo = BitmapFactory.decodeFile(mFileTemp.getPath());
			Credential.saveCredentialData(this, BaseID.KEY_PREF_REG, "1");
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class OpenDataReg extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(RegisterActivity.this, "Loading",
					"Proses registrasi berjalan", true);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				doFileUpload();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (isNotConnected) {
				makeToast("Connection Error!");
			} else if (isExist) {
				makeToast("User already exist!");
			} else {
				Credential.saveCredentialData(RegisterActivity.this,
						BaseID.KEY_PREF_REG, "");
				respondcek();
			}
			progress.dismiss();
		}
	}

	public void respondcek() {

		status = listRespondObjects.get(0).getStatus();
		message = listRespondObjects.get(0).getMessage();

		if (status.equals("1")) {
			Toast.makeText(RegisterActivity.this, "Register success",
					Toast.LENGTH_LONG).show();
//			MyActivity.show(RegisterActivity.this);
			MyActivityRegister.show(RegisterActivity.this);
			Credential.saveCredentialData(RegisterActivity.this,
					BaseID.KEY_DATA_EMAIL, email1);
			Credential.saveCredentialData(RegisterActivity.this,
					BaseID.KEY_DATA_USERNAME, user1);
		} else {
			password.setText("");
			Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG)
					.show();
		}
	}

	private void doFileUpload() throws Exception {

		String urlString = BaseID.URL_REGISTER;
		isNotConnected = false;

		if (preffoto.equals("1")) {
			file1 = new File(mFileTemp.getPath());
		} else {
			file1 = new File(filename);
			Credential.saveCredentialData(RegisterActivity.this,
					BaseID.KEY_PREF_REG, "");
		}

		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(urlString);
			FileBody bin1 = new FileBody(file1);
			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("photo", bin1);
			reqEntity.addPart("username", new StringBody(user1));
			reqEntity.addPart("email", new StringBody(email1));
			reqEntity.addPart("full_name", new StringBody(name1));
			reqEntity.addPart("phone", new StringBody(phone1));
			reqEntity.addPart("password", new StringBody(pass1));

			post.setEntity(reqEntity);
			HttpResponse response = client.execute(post);
			resEntity = response.getEntity();
			response_str = EntityUtils.toString(resEntity);
			if(response_str.contains("0{")){
				response_str_final = response_str.replace("0{", "{");
			}else{
				response_str_final = response_str.replace("1{", "{");
			}
			ParseRespond parseRespond = new ParseRespond();
			listRespondObjects = parseRespond
					.getArrayListRespondObject(response_str_final);
			
			if(response_str_final.contains("already exist")){
				isExist = true;
			}else{
				isExist = false;
			}
			
		} catch (Exception ex) {
			// Log.e("Debug", "error: " + ex.getMessage(), ex);
			isNotConnected = true;
			String msg = "Error :" + ex.getMessage();
			// Log.d("RegisterActivity", "Error: " + msg);
			throw new Exception(msg);
		}
	}

	protected void makeToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, RegisterActivity.class);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.slide_in_right,
				R.anim.slide_out_left);
	}
}
