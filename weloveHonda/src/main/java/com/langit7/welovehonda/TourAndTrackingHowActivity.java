package com.langit7.welovehonda;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

public class TourAndTrackingHowActivity extends Activity{

	private Button buttHowTo;
	private Button buttListTrack;
	public ProgressBar pr;
	ArrayList<NewsObject>listForumObject;
	ArrayList<NewsDetailObject> listDetailObject;
	private ImageButton buttCreate;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_and_track_how_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttHowTo = (Button)findViewById(R.id.howtotrack);
		buttListTrack = (Button)findViewById(R.id.listgroup);
		buttCreate = (ImageButton)findViewById(R.id.logout);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttHowTo.setBackgroundResource(R.drawable.button_howtocreategroupf480);

		buttListTrack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String preflogin = Credential.getCredentialData(TourAndTrackingHowActivity.this, BaseID.KEY_DATA_LOGIN);
				if(preflogin.equals("1")){
					TourAndTrackingListActivity.show(TourAndTrackingHowActivity.this);
				}else{
					TourAndTrackingLoginActivity.show(TourAndTrackingHowActivity.this);
				}

			}
		});

		buttCreate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TourAndTrackingCreateGroup.show(TourAndTrackingHowActivity.this);
			}
		});

	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, TourAndTrackingHowActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
