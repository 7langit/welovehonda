package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Log;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterGallery;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.langit7.welovehonda.parse.ParseNews;
import com.langit7.welovehonda.social.SocialWebActivity;
import com.langit7.welovehonda.social.TwitterTimeline;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

@SuppressWarnings("deprecation")
public class DashboardActivity extends Activity {

	private ImageButton productCategory;
	private ImageButton productPrice;
	private ImageButton dealerLocation;
	private ImageButton tourTrack;
	private ImageButton publicPlace;
	private ImageButton activityButton;
	private ImageButton whatsNew;
	private ImageButton socialMedia;
	private Gallery gallery;
	SharedPreferences prefData;
	private String KEY_DATA;
	boolean networkSlow;
	private ViewPager imageDefault;
	ArrayList<NewsObject> listNewsObject;
	ArrayList<NewsDetailObject> listDetailObject;
	private int PicPosition;
	private String url;
	ArrayList<String> listImage;
	ArrayList<String> listUrl;
	private LogoutReceiver logoutReceiver;
	private ImageView imageNotif;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Landing Page");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.dashboard_layout);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		gallery = (Gallery) findViewById(R.id.gallery);

		productCategory = (ImageButton) findViewById(R.id.imageButton1);
		productPrice = (ImageButton) findViewById(R.id.imageButton2);
		dealerLocation = (ImageButton) findViewById(R.id.imageButton3);
		publicPlace = (ImageButton) findViewById(R.id.imageButton4);
		tourTrack = (ImageButton) findViewById(R.id.imageButton5);
		activityButton = (ImageButton) findViewById(R.id.imageButton6);
		whatsNew = (ImageButton) findViewById(R.id.imageButton7);
		socialMedia = (ImageButton) findViewById(R.id.imageButton8);
		
		publicPlace.setVisibility(View.GONE);
		
		imageDefault = (ViewPager) findViewById(R.id.indi);
		imageNotif = (ImageView) findViewById(R.id.imageNotif);

		try {
			init();
			buttonHome();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void init() {
		url = BaseID.URL_HIGHLIGHT;
		KEY_DATA = BaseID.KEY_HIGHLIGHT;
		prefData = getPreferences(MODE_PRIVATE);
		listNewsObject = new ArrayList<NewsObject>();
		listDetailObject = new ArrayList<NewsDetailObject>();
		listImage = new ArrayList<String>();
		listUrl = new ArrayList<String>();
	}

	public void buttonHome() {

		productCategory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Credential.saveCredentialData(getApplicationContext(),
						BaseID.KEY_CLASS, BaseID.KEY_CLASS_CATALOGE);
				ProductCatalogueActivity.show(DashboardActivity.this);
			}
		});

		productPrice.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Credential.saveCredentialData(getApplicationContext(),
						BaseID.KEY_CLASS, BaseID.KEY_CLASS_PRICE);
				ProductPriceActivity.show(DashboardActivity.this);
			}
		});

		dealerLocation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				DealerLocationSearchActivity.show(DashboardActivity.this);

			}
		});

//		publicPlace.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//
//				PublicPlaceActivity.show(DashboardActivity.this);
//			}
//		});

		tourTrack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String preflogin = Credential.getCredentialData(
						DashboardActivity.this, BaseID.KEY_DATA_LOGIN);
				if (preflogin.equals("1")) {
					String join_data = Credential.getCredentialData(
							DashboardActivity.this,
							BaseID.KEY_DATA_JOIN_GROUP_TRACK);
					if (join_data.equals("1")) {
						int position = Credential.getCredentialDataInt(
								DashboardActivity.this,
								BaseID.KEY_DATA_POSITION_TRACK);
						TourAndTrackingLocationActivity.show(
								DashboardActivity.this, position);
					} else {
						TourAndTrackingListActivity
								.show(DashboardActivity.this);
						Credential.saveCredentialData(DashboardActivity.this,
								BaseID.KEY_DATA_JOIN_GROUP_TRACK, "");
					}
				} else {
					TourAndTrackingLoginActivity.show(DashboardActivity.this);
				}
			}
		});

		activityButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Credential.saveCredentialData(getApplicationContext(),
						BaseID.KEY_CLASS, BaseID.KEY_CLASS_ACTIVITY);
				MyActivityEvent.show(DashboardActivity.this);

			}
		});

		whatsNew.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				NewsActivity.show(DashboardActivity.this);
			}
		});

		socialMedia.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				TwitterTimeline.show(DashboardActivity.this);
			}
		});
		
		imageNotif.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(), NotifActivity.class);
				startActivity(intent);
			}
		});
	}

	class OpenData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				// cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					// Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}// end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);
		System.out.println(insStr);
		ParseNews parsingNews = new ParseNews();
		try {
			listNewsObject = parsingNews.getArrayListObject(insStr);
			listDetailObject = listNewsObject.get(0).getListDetailObject();
			for (int i = 0; i < listDetailObject.size(); i++) {
				listImage.add(listDetailObject.get(i).getImages());
				listUrl.add(listDetailObject.get(i).getLink2());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView() {

		imageDefault.setAdapter(new ImagePagerAdapter(this));

		if (listImage.size() > 0) {
			ListAdapterGallery listAdapter = new ListAdapterGallery(this,
					listImage, listUrl);
			gallery.setAdapter(listAdapter);
			setChange();
		}

		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				imageDefault.setCurrentItem(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		gallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			@SuppressWarnings("rawtypes")
			public void onItemClick(AdapterView parent, View v, int position,
					long id) {

				SocialWebActivity.show(DashboardActivity.this,
						listUrl.get(position));
				//System.out.println(listUrl.get(position));
			}
		});
	}

	public void setChange() {

		final AlphaAnimation animation1 = new AlphaAnimation(1.0f, 1.0f);
		animation1.setDuration(2500);
		animation1.setStartOffset(0);

		final AlphaAnimation animation2 = new AlphaAnimation(1.0f, 1.0f);
		animation2.setDuration(2500);
		animation2.setStartOffset(0);

		animation1.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				gallery.startAnimation(animation2);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationStart(Animation arg0) {
			}

		});

		animation2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				gallery.startAnimation(animation1);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationStart(Animation arg0) {
			}
		});

		gallery.startAnimation(animation1);

		final Handler handler = new Handler();
		final Runnable r = new Runnable() {

			@Override
			public void run() {
				myslideshow();
				handler.postDelayed(this, 5000);
			}

			private void myslideshow() {
				PicPosition = gallery.getSelectedItemPosition() + 1;
				if (PicPosition == (listImage.size())) {
					PicPosition = 0;
				}
				if (PicPosition > listImage.size())
					PicPosition = gallery.getSelectedItemPosition(); // stop
				else
					gallery.setSelection(PicPosition);// move t
			}
		};
		handler.postDelayed(r, 20000);
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private LayoutInflater inflater;
		final int[] ids = { R.drawable.page_1, R.drawable.page_2,
				R.drawable.page_3, R.drawable.page_1, R.drawable.page_2,
				R.drawable.page_3 };

		ImagePagerAdapter(Context c) {
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return ids.length;
		}

		@SuppressWarnings("unused")
		public Object getItem(int position) {
			return position;
		}

		@SuppressWarnings("unused")
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object instantiateItem(View view, int position) {
			final View imageLayout = inflater.inflate(R.layout.list_indicator,
					null);
			final ImageView imageView = (ImageView) imageLayout
					.findViewById(R.id.area_indicator);

			imageView.setImageResource(ids[position]);

			((ViewPager) view).addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(DashboardActivity.this,
				"You have network problem, please check your network setting",
				Toast.LENGTH_LONG).show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("com.package.ACTION_LOGOUT");
		getApplicationContext().sendBroadcast(broadcastIntent);
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, DashboardActivity.class);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.slide_in_right,
				R.anim.slide_out_left);
	}

}
