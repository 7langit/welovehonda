package com.langit7.welovehonda;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterTrackingListFriendHistory;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class TourAndTrackingListUserTrackingActivity extends Activity{

	public ProgressBar pr;
	private boolean networkSlow;
	ArrayList<LatLonObject>listData;
	private ListAdapterTrackingListFriendHistory listMenu;
	private ListView listViewTrack;
	private String gid;
	private String uid;
	private String user;
	private String urlimage;
	private ImageView ava;
	private TextView userName;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private int w;
	private int h;
	private ImageButton buttHint;
	private Dialog dialogRetweet;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking List Friend Page");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_tracking_map_history_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		gid = Credential.getCredentialData(this, BaseID.KEY_DATA_GID);
		uid = (String) getIntent().getSerializableExtra("UID");

		buttHint = (ImageButton)findViewById(R.id.hint);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		ava = (ImageView)findViewById(R.id.avatar);
		userName = (TextView)findViewById(R.id.title2);
		listViewTrack = (ListView)findViewById(R.id.listView);
		listViewTrack.setVisibility(View.VISIBLE);
		
		buttHint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogDesc("Menampilkan list history dari seluruh aktivitas tracking yang telah dilakukan user.");
			}
		});

		try {
			init();
			userName.setText(user);
			imageLoader.displayImage(urlimage, ava,options);
			
			Credential.saveCredentialData(TourAndTrackingListUserTrackingActivity.this, BaseID.KEY_DATA_USER_NAME_LIST, user);
			Credential.saveCredentialData(TourAndTrackingListUserTrackingActivity.this, BaseID.KEY_DATA_GID_LIST, urlimage);
			
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void init(){
		user = (String)getIntent().getSerializableExtra("USER");
		urlimage = (String)getIntent().getSerializableExtra("URL");

		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.cacheInMemory(true)
		.displayer(new RoundedBitmapDisplayer(300))
		.showImageOnLoading(R.drawable.ic_launcher)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();

		listData = new ArrayList<LatLonObject>();
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(BaseID.URL_LIST_TRACK_HISTORY);
			if (is == null) {
				//cek data apakah ada di dalam preference
				networkSlow = true;
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("gid", gid));
		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParseLatLon parsingForum = new ParseLatLon();
		Credential.saveCredentialData(TourAndTrackingListUserTrackingActivity.this, BaseID.KEY_DATA_LIST_HISTORY_FRIEND, insStr);
		try {
			listData = parsingForum.getArrayListObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showView(){

		listMenu = new ListAdapterTrackingListFriendHistory(this, R.layout.listitem, listData);
		listViewTrack.setAdapter(listMenu);
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(TourAndTrackingListUserTrackingActivity.this, "Please try again in a few minutes", Toast.LENGTH_LONG).show();
	}
	
	private void dialogDesc(final String idStatus) {
		dialogRetweet = new Dialog(this, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button2);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String username, String url,String gid, String uid) {
		Intent intent = new Intent(context, TourAndTrackingListUserTrackingActivity.class);
		Bundle b = new Bundle();
		b.putString("USER", username);
		b.putString("URL", url);
		b.putString("GID", gid);
		b.putString("UID", uid);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
