package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.langit7.welovehonda.adapter.ExpandableListAdapterProductCatalogue;
import com.langit7.welovehonda.object.TypeProductCatalogueObject;
import com.langit7.welovehonda.parse.ParseTypeProductEvent;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MyActivityForumDetail extends Activity{

	private Button buttActivity;
	private Button buttForum;
	public ProgressBar pr;
	private String KEY_DATA;
	private SharedPreferences prefData;
	private boolean networkSlow;
	private ExpandableListAdapterProductCatalogue listMenu;
	private ExpandableListView listViewMenu;
	private BroadcastReceiver logoutReceiver;
	
	ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects = new ArrayList<TypeProductCatalogueObject>();
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_activity_layout2);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttActivity = (Button)findViewById(R.id.activity);
		buttForum = (Button)findViewById(R.id.forum);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttForum.setBackgroundResource(R.drawable.button_forumactivityf);
		listViewMenu = (ExpandableListView)findViewById(R.id.listView);

		buttActivity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				MyActivityEvent.show(MyActivityForumDetail.this);
			}
		});

		init();
		new OpenData().execute();

	}

	public void init(){

		
		KEY_DATA = BaseID.KEY_DATA_FORUM;
		prefData = getPreferences(MODE_PRIVATE);
		
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.showImageOnLoading(R.drawable.imagedefault_slidescreen)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();

	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
//			is = getData(BaseID.URL_FORUM_TOPIC);
			is = getData(BaseID.URL_ALL_FORUM_TOPIC);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("type", "forum"));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

//		ParseLatLon parsingForum = new ParseLatLon();
		ParseTypeProductEvent parseTypeProductCatalogue = new ParseTypeProductEvent();
		try {
//			listForumObject = parsingForum.getArrayListObject(insStr);
			parseTypeProductCatalogue.fethingData(insStr);
			typeProductCatalogueObjects = parseTypeProductCatalogue.getTypeProductCatalogueObjects();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		listMenu = new ExpandableListAdapterProductCatalogue(this, typeProductCatalogueObjects, imageLoader, options);
		listViewMenu.setAdapter(listMenu);
		listViewMenu.setDivider(this.getResources().getDrawable(R.drawable.list_separatorabuabu));
		listViewMenu.setChildDivider(this.getResources().getDrawable(R.color.white));

	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(MyActivityForumDetail.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String tid) {
		Intent intent = new Intent(context, MyActivityForumDetail.class);
		Bundle b = new Bundle();
		b.putString("TID", tid);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
	
	public static void show(Context context) {
		Intent intent = new Intent(context, MyActivityForumDetail.class);
		Bundle b = new Bundle();
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
