package com.langit7.welovehonda.adapter;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

public class ListAdapterTwitter extends ArrayAdapter<Status> {

	Context mContext;
	Status item;
	ArrayList<Status> listStat;
	private Dialog dialogReplyTweet;
	private int w;
	private int h;
	private Dialog dialogRetweet;
	private static SharedPreferences mSharedPreferences;

	public ListAdapterTwitter(Context context,int textViewResourceId, List<Status> result) {
		super(context, textViewResourceId, result);
		this.mContext = context;
		this.listStat = (ArrayList<Status>) listStat;
	}

	public class ViewHolder{
		public TextView text1;
		public Button buttonRetweet;
		public Button buttonReply;
		public TextView date;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder holder = null;

		mSharedPreferences = mContext.getSharedPreferences(BaseID.PREFERENCE_NAME, Context.MODE_PRIVATE);
		item = getItem(position);

		DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.socialitem_twitter, null);

		holder = new ViewHolder();
		holder.date = (TextView) rowView.findViewById(R.id.date);
		holder.text1 = (TextView) rowView.findViewById(R.id.label);
		holder.buttonRetweet = (Button) rowView.findViewById(R.id.buttonRetweet);
		holder.buttonReply = (Button) rowView.findViewById(R.id.buttonReply);

		holder.date.setText("Created at: "+item.getCreatedAt().toLocaleString());
		if (item.isRetweet()) item = item.getRetweetedStatus();
		holder.text1.setText(item.getText());
		Linkify.addLinks(holder.text1, Linkify.ALL);

		holder.buttonReply.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(mContext, "Reply status", Toast.LENGTH_SHORT).show();
				dialogReplyTweet = new Dialog(mContext, R.style.Theme_Transparent);
				dialogReplyTweet.setContentView(R.layout.dialog_field_and_2button);
				dialogReplyTweet.setTitle("Reply status @welovehonda");
				dialogReplyTweet.getWindow().setLayout(w, h);

				final EditText reply = (EditText) dialogReplyTweet.findViewById(R.id.tweet);
				reply.setText("@welovehonda ");
				reply.setPadding(10, 10, 10, 10);
				reply.setLines(3);

				RelativeLayout areaAdapter = (RelativeLayout) dialogReplyTweet.findViewById(R.id.area_dialog);

				reply.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
								(keyCode == KeyEvent.KEYCODE_ENTER)) {

							InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(reply.getWindowToken(), 0);
							return true;
						}
						return false;
					}});

				TextView textPositive = (TextView) dialogReplyTweet.findViewById(R.id.text_positive);
				textPositive.setText("Reply");
				textPositive.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String status = reply.getText().toString();

						if (status.length()<141) {
							item = getItem(position);
							String strLong = Long.toString(item.getId());
							Credential.saveCredentialData(mContext, BaseID.KEY_ID_TWIT, strLong);
							new ReplyStatus().execute(status);
							dialogReplyTweet.dismiss();
						}else {
							Toast.makeText(mContext, "Status Is Over 140 Characters", Toast.LENGTH_SHORT).show();
						}
					}
				});

				TextView textNegative = (TextView) dialogReplyTweet.findViewById(R.id.text_negative);
				textNegative.setText("Cancel");
				textNegative.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogReplyTweet.dismiss();
					}});

				areaAdapter.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogReplyTweet.dismiss();
					}
				});
				dialogReplyTweet.show();
			}
		});


		holder.buttonRetweet.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				item = getItem(position);
				String strLong = Long.toString(item.getId());
//				dialogConfirm(strLong);
				retweetTweet(strLong);
			}

		});

		rowView.setTag(holder);
		return rowView;
	}

	private void retweetTweet(final String idStatus) {
		dialogRetweet = new Dialog(mContext, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText("Are You Sure You Want To Retweet this Status?");
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogRetweet.findViewById(R.id.text_positive);
		textPositive.setText("YES");
		
		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new RetweetStatus().execute(idStatus);
				dialogRetweet.dismiss();
			}

		});

		TextView textNegative = (TextView) dialogRetweet.findViewById(R.id.text_negative);
		textNegative.setText("NO");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}
	
	class RetweetStatus extends AsyncTask<String, String, String> {

		ProgressDialog pDialog;
		private String err = "";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(mContext);
			pDialog.setMessage("Retweeting Status");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);

				// Access Token 
				String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				twitter4j.Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

				long status2 = Long.parseLong(status);
				twitter.retweetStatus(status2);

			} catch (TwitterException e) {
				if (e.getErrorMessage() != null) {
					err = e.getErrorMessage();
				} else {
					if (e.getStatusCode() == 403) {
						err = "Tweet not sent. Already retweeted.";
					} else {
						err = "Tweet not sent.";
					}
				}
			}catch (Exception e) {
				err = e.getMessage();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			if (!err.equalsIgnoreCase("")) {
				Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(mContext, "Tweet Retweeted", Toast.LENGTH_SHORT).show();
			}
		}

	}

	class ReplyStatus extends AsyncTask<String, String, String> {

		ProgressDialog pDialog;
		private String response;
		private String responseToast;

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(mContext);
			pDialog.setMessage("Reply Status");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);

				// Access Token 
				String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				twitter4j.Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

				StatusUpdate stat = new StatusUpdate(status);
				long status2 = Long.parseLong(Credential.getCredentialData(mContext, BaseID.KEY_ID_TWIT));

				stat.setInReplyToStatusId(status2);
				twitter4j.Status response = twitter.updateStatus(stat);

				if (!(""+response.getId()).equals("")) {
					responseToast = "Reply success";
				}else{
					responseToast = "Failed to Reply";
				}
				Log.d("Status", "> " + response.getText());
			} catch (TwitterException e) {
				e.printStackTrace();
				responseToast = "Failed to Reply";
				Log.e("error", "err"+e.getStatusCode());
				if (e.getErrorMessage() != null) {
					responseToast = e.getErrorMessage();
				} else {
					if (e.getStatusCode() == 403) {
						responseToast = "Status is a duplicate.";
						Log.d("status code1", "msg"+e.getStatusCode()); 
					} else {
						responseToast = "There was an issue when sending your Tweet. Please try again later.";
						Log.d("status code2", "msg"+e.getStatusCode()); 
					}
				}
			}
			return response;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			Toast.makeText(mContext, responseToast, Toast.LENGTH_SHORT).show();
			// updating UI from Background Thread
		}

	}


}

