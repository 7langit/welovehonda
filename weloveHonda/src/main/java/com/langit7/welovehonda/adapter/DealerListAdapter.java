package com.langit7.welovehonda.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.dealer;

import java.util.List;


public class DealerListAdapter extends ArrayAdapter<dealer> {

    Context ctx;


    public DealerListAdapter(Context context, int resource, List<dealer> items) {
        super(context, resource, items);
        this.ctx =context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = null;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_dealer, null);
        }

        dealer p = getItem(position);

        if (p != null) {

                TextView tname= (TextView) v.findViewById(R.id.etname);
                TextView taddr= (TextView) v.findViewById(R.id.taddress);
                TextView ttype= (TextView) v.findViewById(R.id.ttype);
                TextView tphone= (TextView) v.findViewById(R.id.tphone);
                ImageView imgcall= (ImageView) v.findViewById(R.id.imgcall);


            tname.setText(p.getTitle());
            taddr.setText(p.getAddress());
            ttype.setText(p.getCategory().getName());
            tphone.setText(p.getPhone());


            imgcall.setId(position);
            imgcall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getItem(view.getId()).getPhone()!=null) {
                        String posted_by = getItem(view.getId()).getPhone();
                        String uri = "tel:" + posted_by.trim();
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(uri));
                        ctx.startActivity(intent);
                    }else{
                        Toast.makeText(ctx, "Maaf, Dealer ini belum memiliki nomor yang dapat dihubungi", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        return v;
    }





}