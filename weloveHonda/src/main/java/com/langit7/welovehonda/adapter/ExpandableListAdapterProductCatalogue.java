package com.langit7.welovehonda.adapter;

import java.io.File;
import java.util.ArrayList;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.MyActivityForumTopicDetail;
import com.langit7.welovehonda.ProductCatalogueMainActivity;
import com.langit7.welovehonda.ProductPrice2Activity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.TypeProductCatalogueObject;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.BroadcastManager;
import com.langit7.welovehonda.util.Credential;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ExpandableListAdapterProductCatalogue extends
		BaseExpandableListAdapter {

	private Context context;
	// private List<String> _listDataHeader; // header titles
	// // child data in format of header title, child title
	// private HashMap<String, List<String>> _listDataChild;
	//
	// refactor
	ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects = new ArrayList<TypeProductCatalogueObject>();
	// ArrayList<SubTypeProductCatalogueObject> subTypeProductCatalogueObjects =
	// new ArrayList<SubTypeProductCatalogueObject>();
	ImageLoader imageLoader;
	DisplayImageOptions options;

	private int w;
	private int h;

	private Dialog dialogPopup;

	private Dialog dialogPopupDownload;

	public TextView title;
	public RelativeLayout areasemua;
	public ImageButton hint;
	private long enqueue;

	private DownloadManager dm;

	public ExpandableListAdapterProductCatalogue(Context context,
			ArrayList<TypeProductCatalogueObject> typeProductCatalogueObjects,
			ImageLoader imageLoader, DisplayImageOptions options) {
		this.context = context;
		this.typeProductCatalogueObjects = typeProductCatalogueObjects;
		// this.subTypeProductCatalogueObjects = subTypeProductCatalogueObjects;
		this.imageLoader = imageLoader;
		this.options = options;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this.typeProductCatalogueObjects.get(groupPosition)
				.getChildren().get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		if (typeProductCatalogueObjects.get(groupPosition).getChildren()
				.get(childPosition).getImage() != null) {
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.list_item, null);
			}

			DisplayMetrics metrics = context.getResources().getDisplayMetrics();
			int width = metrics.widthPixels;

			RelativeLayout areasemua = (RelativeLayout) convertView
					.findViewById(R.id.arealayout);
			ImageView txtListChild = (ImageView) convertView
					.findViewById(R.id.imageViewSubProduct);

			final String credentialData = Credential.getCredentialData(context,
					BaseID.KEY_CLASS);
			if (credentialData.equals(BaseID.KEY_CLASS_ACTIVITY)) {
				RelativeLayout areaTxt = (RelativeLayout) convertView
						.findViewById(R.id.areaTxt);
				areaTxt.setVisibility(View.VISIBLE);

				TextView text = (TextView) convertView.findViewById(R.id.text);
				text.setText(typeProductCatalogueObjects.get(groupPosition)
						.getChildren().get(childPosition).getName()
						.replace("BPP", "Download Manual Owner"));
			}

			options = new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
					.showImageOnLoading(R.drawable.imagedefault_slidescreen)
					.build();

			imageLoader.displayImage(
					typeProductCatalogueObjects.get(groupPosition)
							.getChildren().get(childPosition).getImage()
							+ "&w=" + width, txtListChild, options);

			areasemua.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					doOnClick(groupPosition, childPosition, credentialData);

				}

			});
		} else {
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.listforum2, null);
			}

			DisplayMetrics metrics = context.getResources().getDisplayMetrics();
			w = metrics.widthPixels;
			h = metrics.heightPixels;

			areasemua = (RelativeLayout) convertView
					.findViewById(R.id.areasemua);
			title = (TextView) convertView.findViewById(R.id.title);
			hint = (ImageButton) convertView.findViewById(R.id.hint);
			title.setText(typeProductCatalogueObjects.get(groupPosition)
					.getChildren().get(childPosition).getName());

			hint.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String stat = typeProductCatalogueObjects
							.get(groupPosition).getChildren()
							.get(childPosition).getDesc();
					dialogDesc(stat);
				}
			});

			areasemua.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					MyActivityForumTopicDetail
							.show(context,
									typeProductCatalogueObjects
											.get(groupPosition).getChildren()
											.get(childPosition).getNid(),
									typeProductCatalogueObjects
											.get(groupPosition).getChildren()
											.get(childPosition).getName(),
									typeProductCatalogueObjects
											.get(groupPosition).getChildren()
											.get(childPosition)
											.getLink_detail(),
									typeProductCatalogueObjects
											.get(groupPosition).getChildren()
											.get(childPosition).getDesc());
				}
			});

		}

		return convertView;
	}

	private void doOnClick(final int groupPosition, final int childPosition,
			final String credentialData) {
		if (credentialData.equals(BaseID.KEY_CLASS_CATALOGE)) {
			ProductCatalogueMainActivity.show(context,
					typeProductCatalogueObjects.get(groupPosition)
							.getChildren().get(childPosition).getNid());
		} else if (credentialData.equals(BaseID.KEY_CLASS_PRICE)) {
			// Toast.makeText(
			// context,
			// typeProductCatalogueObjects.get(groupPosition)
			// .getChildren().get(childPosition).getName()
			// + ", "
			// + typeProductCatalogueObjects
			// .get(groupPosition).getChildren()
			// .get(childPosition).getNid(),
			// Toast.LENGTH_SHORT).show();

			ProductPrice2Activity.show(context, typeProductCatalogueObjects
					.get(groupPosition).getChildren().get(childPosition)
					.getNid());
		} else if (credentialData.equals(BaseID.KEY_CLASS_ACTIVITY)) {
			// download

			dialogDownload("Are You Sure You Want To Download This File?",
					typeProductCatalogueObjects.get(groupPosition)
							.getChildren().get(childPosition).getLink_detail(),
					typeProductCatalogueObjects.get(groupPosition)
							.getChildren().get(childPosition).getName()
							.replace("BPP ", ""), typeProductCatalogueObjects
							.get(groupPosition).getChildren()
							.get(childPosition).getName().replace("BPP ", ""),
					BaseID.DIR);

			// PackageManager pm = context.getPackageManager();
			// ComponentName componentName = new ComponentName(
			// context, BroadcastManager.class);
			// pm.setComponentEnabledSetting(componentName,
			// PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
			// PackageManager.DONT_KILL_APP);
			//
			// doDownloadManager(
			// typeProductCatalogueObjects.get(groupPosition)
			// .getChildren().get(childPosition)
			// .getLink_detail(),
			// typeProductCatalogueObjects.get(groupPosition)
			// .getChildren().get(childPosition)
			// .getName().replace("BPP ", ""),
			// typeProductCatalogueObjects.get(groupPosition)
			// .getChildren().get(childPosition)
			// .getName().replace("BPP ", ""), BaseID.DIR);

			// SocialWebActivity.show(context,
			// typeProductCatalogueObjects.get(groupPosition)
			// .getChildren().get(childPosition)
			// .getLink_detail());
		}
	}

	private void dialogDesc(final String idStatus) {
		dialogPopup = new Dialog(context, R.style.Theme_Transparent);
		dialogPopup.setContentView(R.layout.dialog_text_and_2button2);
		dialogPopup.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogPopup
				.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogPopup
				.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogPopup.dismiss();
			}
		});

		dialogPopup.show();
	}

	private void dialogDownload(final String idStatus,
			final String linkDownload, final String title, final String desc,
			final String dir) {

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		dialogPopupDownload = new Dialog(context, R.style.Theme_Transparent);
		dialogPopupDownload.setContentView(R.layout.dialog_text_and_2button);
		dialogPopupDownload.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogPopupDownload
				.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogPopupDownload
				.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogPopupDownload
				.findViewById(R.id.text_positive);
		textPositive.setText("YES");

		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				PackageManager pm = context.getPackageManager();
				ComponentName componentName = new ComponentName(context,
						BroadcastManager.class);
				pm.setComponentEnabledSetting(componentName,
						PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
						PackageManager.DONT_KILL_APP);

				doDownloadManager(linkDownload, title, desc, dir);

				dialogPopupDownload.dismiss();
			}

		});

		TextView textNegative = (TextView) dialogPopupDownload
				.findViewById(R.id.text_negative);
		textNegative.setText("NO");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialogPopupDownload.dismiss();
			}
		});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogPopupDownload.dismiss();
			}
		});

		dialogPopupDownload.show();
	}

	private void doDownloadManager(String linkDownload, String title,
			String desc, String dir) {
		if (!linkDownload.equals("-")) {
			if (!linkDownload.isEmpty()) {
				makeToast("Mulai Mendownload Dokumen ...");
				dm = (DownloadManager) context
						.getSystemService(Context.DOWNLOAD_SERVICE);

				File file = new File(Environment.getExternalStorageDirectory()
						+ dir);
				file.mkdirs();
				linkDownload = linkDownload.replace("https", "http");
				Request request = new Request(Uri.parse(linkDownload))
						.setAllowedNetworkTypes(
								DownloadManager.Request.NETWORK_WIFI
										| DownloadManager.Request.NETWORK_MOBILE)
						.setAllowedOverRoaming(false)
						.setTitle(title)
						.setDescription(desc)
						.setDestinationInExternalPublicDir(
								Environment.DIRECTORY_DOWNLOADS, title + ".pdf");
				enqueue = dm.enqueue(request);
				Credential.saveCredentialDataLong(context, "enqueue", enqueue);
				Credential.saveCredentialData(context, "titleFile" + enqueue,
						title);
				String value = title + ".pdf";
				Credential.saveCredentialData(context, BaseID.KEY_TITTLE_PATH,
						value);
			} else {
				makeToast("Dokumen Tidak Ditemukan!");
			}
		} else {
			makeToast("Dokumen Tidak Ditemukan!");
		}
	}

	protected void makeToast(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this.typeProductCatalogueObjects.get(groupPosition)
				.getChildren().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.typeProductCatalogueObjects.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this.typeProductCatalogueObjects.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = typeProductCatalogueObjects.get(groupPosition)
				.getType_cat();
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		int widthRealtive = width / 5;

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);
		lblListHeader.setTextColor(Color.DKGRAY);

		ImageView imageViewProduct = (ImageView) convertView
				.findViewById(R.id.imageViewProduct);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

		imageLoader.displayImage(typeProductCatalogueObjects.get(groupPosition)
				.getIcon_cat() + "&w=" + widthRealtive, imageViewProduct,
				options);

		LinearLayout areagroup = (LinearLayout) convertView
				.findViewById(R.id.areagroup);
		if (isExpanded) {
			areagroup.setBackgroundResource(R.color.redhonda2);
			lblListHeader.setTextColor(Color.WHITE);
		} else {
			areagroup.setBackgroundResource(R.color.grey);
			lblListHeader.setTextColor(Color.DKGRAY);
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
