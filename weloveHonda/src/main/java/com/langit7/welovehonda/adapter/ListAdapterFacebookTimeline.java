package com.langit7.welovehonda.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.FacebookTimelineObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


public class ListAdapterFacebookTimeline extends ArrayAdapter<FacebookTimelineObject> {

	Context mContext;
	FBookTaskListener taskListener;
	ArrayList<FacebookTimelineObject> listFacebookTimelineObjects;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	private int w;
	private int h;
	private Dialog dialogRetweet;

	public ListAdapterFacebookTimeline(Context context,
			int textViewResourceId,
			List<FacebookTimelineObject> objects,ImageLoader imageLoader, DisplayImageOptions options) {
		super(context, textViewResourceId, objects);
		this.mContext = context;
		this.listFacebookTimelineObjects = (ArrayList<FacebookTimelineObject>) objects;
		this.imageLoader = imageLoader;
		this.options = options;
	}

	public void setTaskListener(FBookTaskListener listener)
	{
		this.taskListener = listener;
	}

	public static interface FBookTaskListener{
		public void doAuthenticationComment(String idFb, String message);
		public void doAuthenticationLike(String idFb);
	}

	public class ViewHolder{
		public ImageView image;
		public TextView text;
		public TextView date;
		public ImageView url;
		public Button buttonLike;
		public Button buttonComment;
	}


	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View rowView = convertView;

		DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.socialitem_fb, null);

		ViewHolder holder = new ViewHolder();
		holder.image = (ImageView) rowView.findViewById(R.id.icon);
		holder.text = (TextView) rowView.findViewById(R.id.label);
		holder.date = (TextView) rowView.findViewById(R.id.date);
		holder.url = (ImageView)rowView.findViewById(R.id.image);
		holder.text.setText(Html.fromHtml(listFacebookTimelineObjects.get(position).getText()));
		holder.date.setText("Created at: "+listFacebookTimelineObjects.get(position).getDate());
		imageLoader.displayImage(listFacebookTimelineObjects.get(position).getImage(), holder.url, options);
		holder.buttonLike = (Button) rowView.findViewById(R.id.buttonLike);
		holder.buttonComment = (Button) rowView.findViewById(R.id.buttonComment);

		holder.buttonLike.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String id = listFacebookTimelineObjects.get(position).getId();
				retweetTweet(id);
//				taskListener.doAuthenticationLike(id);
			}
		});

		holder.buttonComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final String id = listFacebookTimelineObjects.get(position).getId();
				final Dialog dialogCommentStatus = new Dialog(mContext, R.style.Theme_Transparent);
				dialogCommentStatus.setContentView(R.layout.dialog_field_and_2button);

				final EditText reply = (EditText) dialogCommentStatus.findViewById(R.id.tweet);
				reply.setHint("Write your comment here...");

				dialogCommentStatus.getWindow().setLayout(w, h);
				reply.setPadding(10, 10, 10, 10);
				reply.setLines(3);

				RelativeLayout areaAdapter = (RelativeLayout) dialogCommentStatus.findViewById(R.id.area_dialog);

				reply.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
								(keyCode == KeyEvent.KEYCODE_ENTER)) {

							InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(reply.getWindowToken(), 0);
							return true;
						}
						return false;
					}});


				TextView textPositive = (TextView) dialogCommentStatus.findViewById(R.id.text_positive);
				textPositive.setText("Send");
				textPositive.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						taskListener.doAuthenticationComment(id, reply.getText().toString());
						dialogCommentStatus.dismiss();
					}
				});

				TextView textNegative = (TextView) dialogCommentStatus.findViewById(R.id.text_negative);
				textNegative.setText("Cancel");
				textNegative.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogCommentStatus.dismiss();
					}});

				areaAdapter.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogCommentStatus.dismiss();
					}
				});
				dialogCommentStatus.show();
			}
		});

		rowView.setTag(holder);
		return rowView;
	}

	private void retweetTweet(final String idStatus) {
		dialogRetweet = new Dialog(mContext, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText("Are You Sure You Want To Like this Status?");
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogRetweet.findViewById(R.id.text_positive);
		textPositive.setText("Yes");
		
		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				taskListener.doAuthenticationLike(idStatus);
				dialogRetweet.dismiss();
			}

		});

		TextView textNegative = (TextView) dialogRetweet.findViewById(R.id.text_negative);
		textNegative.setText("No");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}
}