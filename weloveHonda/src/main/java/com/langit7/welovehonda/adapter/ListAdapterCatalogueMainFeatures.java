package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.CataloguDetailMainFeaturesObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ListAdapterCatalogueMainFeatures extends
		ArrayAdapter<CataloguDetailMainFeaturesObject> {

	Context context;
	ArrayList<CataloguDetailMainFeaturesObject> listCatalogue;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	
	ImageLoader imageLoaderImg = ImageLoader.getInstance();
	DisplayImageOptions optionsImg;

	private Dialog dialogPopUp = null;

	public ListAdapterCatalogueMainFeatures(Context context,
			int textViewResourceId,
			ArrayList<CataloguDetailMainFeaturesObject> listCatalogue) {
		super(context, textViewResourceId, listCatalogue);
		this.context = context;
		this.listCatalogue = (ArrayList<CataloguDetailMainFeaturesObject>) listCatalogue;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class ViewHolder {
		public TextView spec;
		public TextView desc;
		public ImageView image;
		public RelativeLayout areasemua;
		public LinearLayout areadalam;
		public ProgressBar progressBar1;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		final int width = metrics.widthPixels;
		final int widthRealtive = width / 4;

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listmain_features, null);

		imageLoader.init(ImageLoaderConfiguration.createDefault(context));

		options = new DisplayImageOptions.Builder().cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		

		ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout) rowView
				.findViewById(R.id.areasemua);
		holder.areadalam = (LinearLayout) rowView.findViewById(R.id.areadalam);
		holder.spec = (TextView) rowView.findViewById(R.id.name);
		holder.desc = (TextView) rowView.findViewById(R.id.desc);
		holder.image = (ImageView) rowView.findViewById(R.id.imageView1);
		holder.progressBar1 = (ProgressBar) rowView.findViewById(R.id.progressBar1);

		holder.spec.setText(listCatalogue.get(position).getName());
		holder.desc.setText(listCatalogue.get(position).getDesc());

		holder.spec.setTypeface(null, Typeface.BOLD);

		imageLoader.displayImage(listCatalogue.get(position).getImage() + "&w="
				+ widthRealtive, holder.image, options);

		holder.areadalam.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doPopUpDialog(listCatalogue.get(position).getImage() + "&w="
						+ width);
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

	protected void doPopUpDialog(String url) {
		if (dialogPopUp != null) {
			dialogPopUp = null;
		}
		
		imageLoaderImg.init(ImageLoaderConfiguration.createDefault(context));
		
		optionsImg = new DisplayImageOptions.Builder().cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		dialogPopUp = new Dialog(context, R.style.Theme_Transparent);
		dialogPopUp.setContentView(R.layout.popupimage);
		dialogPopUp.getWindow().setLayout(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		dialogPopUp.setCancelable(true);

//		URL newurl;
//		Bitmap mIcon_val = null;
//		try {
//			newurl = new URL(url);
//			mIcon_val = BitmapFactory.decodeStream(newurl.openConnection()
//					.getInputStream());
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		ImageView imageView1 = (ImageView) dialogPopUp
				.findViewById(R.id.imageView1);
		
		final ProgressBar progressBar1 = (ProgressBar) dialogPopUp
				.findViewById(R.id.progressBar1);

//		imageView1.setImageBitmap(mIcon_val);
		
		imageLoaderImg.displayImage(url, imageView1, optionsImg, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				// TODO Auto-generated method stub
				progressBar1.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
				// TODO Auto-generated method stub
				progressBar1.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String imageUri, View view,
					Bitmap loadedImage) {
				// TODO Auto-generated method stub
				progressBar1.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				// TODO Auto-generated method stub
				progressBar1.setVisibility(View.GONE);
			}
			
		});

		imageView1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialogPopUp.dismiss();
			}
		});

		dialogPopUp.show();
	}
	
}
