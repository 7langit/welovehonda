package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.TourAndTrackingListUserTrackingActivity;
import com.langit7.welovehonda.object.LatLonObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ListAdapterTrackingListFriend extends ArrayAdapter<LatLonObject>{

	Context context;
	ArrayList<LatLonObject>listDetail;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	public ListAdapterTrackingListFriend(Context context, int textViewResourceId, ArrayList<LatLonObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<LatLonObject>)listDetail;

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.cacheInMemory()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.displayer(new RoundedBitmapDisplayer(300))
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.build();

		this.imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
		public ImageView avatar;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listfriendtrack, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);

		holder.avatar = (ImageView)rowView.findViewById(R.id.avatar);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.title.setText(listDetail.get(position).getUsername());

		imageLoader.displayImage(listDetail.get(position).getPhoto(), holder.avatar, options);

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				TourAndTrackingListUserTrackingActivity.show(context, listDetail.get(position).getUsername(), listDetail.get(position).getPhoto(), listDetail.get(position).getGid(), listDetail.get(position).getUid());
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

}
