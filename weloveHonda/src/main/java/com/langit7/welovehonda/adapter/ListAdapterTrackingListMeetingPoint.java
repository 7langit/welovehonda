package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.TourAndTrackingListMeetingPointDetailActivity;
import com.langit7.welovehonda.object.CatalogueDetail3Object;

public class ListAdapterTrackingListMeetingPoint extends ArrayAdapter<CatalogueDetail3Object>{

	Context context;
	ArrayList<CatalogueDetail3Object>listDetail;

	public ListAdapterTrackingListMeetingPoint(Context context, int textViewResourceId, ArrayList<CatalogueDetail3Object>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<CatalogueDetail3Object>)listDetail;
	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listtrack, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.title.setText(listDetail.get(position).getIcon());

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TourAndTrackingListMeetingPointDetailActivity.show(context, listDetail.get(position).getIcon(), position);
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

}
