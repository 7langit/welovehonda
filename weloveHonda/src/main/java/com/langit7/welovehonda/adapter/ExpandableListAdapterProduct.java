package com.langit7.welovehonda.adapter;


import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.TypeProductCatalogueObject;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

public class ExpandableListAdapterProduct extends BaseExpandableListAdapter {

	private SparseArray<TypeProductCatalogueObject> groups;
	public LayoutInflater inflater;
	public Activity activity;

	public ExpandableListAdapterProduct(Activity act, SparseArray<TypeProductCatalogueObject> groups) {
		activity = act;
		this.groups = groups;
		inflater = activity.getLayoutInflater();
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return groups.get(arg0).getChildren().get(arg1);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
			ViewGroup arg4) {
		// TODO Auto-generated method stub
		final String children = (String) getChild(arg0, arg1);
		TextView text = null;
		if (arg3 == null) {
			arg3 = inflater.inflate(R.layout.listrow_details, null);
		}
		text = (TextView) arg3.findViewById(R.id.textView1);
		text.setText(children);
		arg3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(activity, children, Toast.LENGTH_SHORT).show();
			}
		});
		return arg3;
	}

	@Override
	public int getChildrenCount(int arg0) {
		// TODO Auto-generated method stub
		return groups.get(arg0).getChildren().size();
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return groups.get(arg0);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listrow_group, null);
		}
		TypeProductCatalogueObject group = (TypeProductCatalogueObject) getGroup(groupPosition);
		
//		Drawable img = inflater.getContext().getResources().getDrawable( R.drawable.ic_action_search );
//		img.setBounds( 0, 0, 70, 70 );
//		((CheckedTextView) convertView).setCompoundDrawables(img, null,null, null);
		((CheckedTextView) convertView).setText(group.getType_cat());
		((CheckedTextView) convertView).setChecked(isExpanded);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
