package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ListAdapterForumTopicDetail extends ArrayAdapter<LatLonObject>{

	Context context;
	ArrayList<LatLonObject>listDetail;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	public ListAdapterForumTopicDetail(Context context, int textViewResourceId, ArrayList<LatLonObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<LatLonObject>)listDetail;
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.cacheInMemory()
		.showStubImage(R.drawable.ic_launcher)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.displayer(new RoundedBitmapDisplayer(10))
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.build();

		this.imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
	}

	public class ViewHolder {
		public TextView title;
		public TextView comment;
		public ImageView avatar;
		public TextView titleR;
		public TextView commentR;
		public ImageView avatarR;
		public RelativeLayout areasemua;
		public RelativeLayout areaLeft;
		public RelativeLayout areaRight;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listcomment, null);

		final ViewHolder holder = new ViewHolder();

		String username = listDetail.get(position).getName();
		String user = Credential.getCredentialData(context, BaseID.KEY_DATA_USERNAME);
		
		holder.areaLeft = (RelativeLayout) rowView.findViewById(R.id.arealeft);
		holder.areaRight = (RelativeLayout) rowView.findViewById(R.id.arearight);
		holder.avatar = (ImageView)rowView.findViewById(R.id.avatar);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.comment = (TextView)rowView.findViewById(R.id.comment);
		holder.avatarR = (ImageView)rowView.findViewById(R.id.avatarR);
		holder.titleR = (TextView) rowView.findViewById(R.id.titleR);
		holder.commentR = (TextView)rowView.findViewById(R.id.commentR);
		
		holder.comment.setText(listDetail.get(position).getComment());
		holder.title.setText(username);
		holder.commentR.setText(listDetail.get(position).getComment());
		holder.titleR.setText(username);
		
		imageLoader.displayImage(listDetail.get(position).getPhoto(), holder.avatarR,options);
		
		if (username.equals(user)) {
			imageLoader.displayImage(listDetail.get(position).getPhoto(), holder.avatar,options);
			holder.areaLeft.setVisibility(View.VISIBLE);
			holder.areaRight.setVisibility(View.GONE);
		}else{
			holder.areaLeft.setVisibility(View.GONE);
			holder.areaRight.setVisibility(View.VISIBLE);
		}

		rowView.setTag(holder);

		return rowView;
	}

}
