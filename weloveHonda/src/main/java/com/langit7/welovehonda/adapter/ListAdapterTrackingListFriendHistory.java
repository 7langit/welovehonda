package com.langit7.welovehonda.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.TourAndTrackingListUserTrackingDetailActivity;
import com.langit7.welovehonda.object.LatLonObject;

public class ListAdapterTrackingListFriendHistory extends ArrayAdapter<LatLonObject>{

	Context context;
	ArrayList<LatLonObject>listDetail;

	public ListAdapterTrackingListFriendHistory(Context context, int textViewResourceId, ArrayList<LatLonObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<LatLonObject>)listDetail;

	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listtrack, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);

		holder.title = (TextView) rowView.findViewById(R.id.title);

		long epoch = Long.parseLong(listDetail.get(position).getDatetime());
		String date = new java.text.SimpleDateFormat("EEE MM dd HH:mm:ss").format(new java.util.Date (epoch*1000L));
		holder.title.setText(date);
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int pos = position;
				TourAndTrackingListUserTrackingDetailActivity.show(context, pos);
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

}
