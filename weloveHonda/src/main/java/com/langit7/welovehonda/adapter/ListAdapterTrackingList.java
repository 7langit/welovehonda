package com.langit7.welovehonda.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.TourAndTrackingLocationActivity;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.RespondObject;
import com.langit7.welovehonda.parse.ParseRespond;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class ListAdapterTrackingList extends ArrayAdapter<NewsDetailObject>{

	Context context;
	ArrayList<NewsDetailObject>listDetail;
	ArrayList<RespondObject> listRespondObjects = new ArrayList<RespondObject>();
	private Dialog dialogJointeventpass;
	private int w;
	private int h;
	private Dialog dialogJoinnopass;
	private String status;
	private String message;
	@SuppressWarnings("unused")
	private boolean networkSlow;
	private String uid;
	private String gid;
	private String insStr;
	private String desc2;
	protected int pos;
	protected String tit;

	public ListAdapterTrackingList(Context context, int textViewResourceId, ArrayList<NewsDetailObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<NewsDetailObject>)listDetail;
	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listforum, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.title.setText(listDetail.get(position).getTitle());

		uid = Credential.getCredentialData(context, BaseID.KEY_DATA_UID);
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					gid = listDetail.get(position).getGid();
					Credential.saveCredentialData(context, BaseID.KEY_DATA_GID, gid);
					pos = position;
					desc2 = listDetail.get(position).getInfo();
					String pass = listDetail.get(position).getPassword();
					tit = listDetail.get(position).getTitle();
					if(pass.equals("")){
						dialog_join2(tit);
					}else{
						dialog_join(tit,pass);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

	private void dialog_join(final String title,final String pass){

		dialogJointeventpass = new Dialog(context, R.style.Theme_Transparent);
		dialogJointeventpass.setContentView(R.layout.dialog_field_and_2button2);
		TextView textdesc = (TextView) dialogJointeventpass.findViewById(R.id.desc);
		textdesc.setText(desc2);
		final EditText reply = (EditText) dialogJointeventpass.findViewById(R.id.tweet);
		final TextView titleDialog = (TextView)dialogJointeventpass.findViewById(R.id.titleevent);
		titleDialog.setText(title);
		dialogJointeventpass.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogJointeventpass.findViewById(R.id.area_dialog);
		reply.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
						(keyCode == KeyEvent.KEYCODE_ENTER)) {

					InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(reply.getWindowToken(), 0);
					return true;
				}
				return false;
			}});


		TextView textPositive = (TextView) dialogJointeventpass.findViewById(R.id.text_positive);
		textPositive.setText("Ok");
		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(pass.equals(reply.getText().toString())){
					new OpenDataReg().execute();
				}else{
					Toast.makeText(context, "Wrong password", Toast.LENGTH_LONG).show();
				}
				dialogJointeventpass.dismiss();
			}
		});

		TextView textNegative = (TextView) dialogJointeventpass.findViewById(R.id.text_negative);
		textNegative.setText("Cancel");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogJointeventpass.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogJointeventpass.dismiss();
			}
		});
		dialogJointeventpass.show();
	}

	private void dialog_join2(final String title){
		dialogJoinnopass = new Dialog(context, R.style.Theme_Transparent);
		dialogJoinnopass.setContentView(R.layout.dialog_field_and_2button2);
		dialogJoinnopass.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogJoinnopass.findViewById(R.id.area_dialog);
		TextView textdesc = (TextView) dialogJoinnopass.findViewById(R.id.desc);
		textdesc.setText(desc2);
		final EditText reply = (EditText) dialogJoinnopass.findViewById(R.id.tweet);
		reply.setVisibility(View.GONE);
		final TextView titleDialog = (TextView)dialogJoinnopass.findViewById(R.id.titleevent);
		titleDialog.setText(title);
		TextView textPositive = (TextView) dialogJoinnopass.findViewById(R.id.text_positive);
		textPositive.setText("Ok");

		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new OpenDataReg().execute();
				dialogJoinnopass.dismiss();
			}

		});

		TextView textNegative = (TextView) dialogJoinnopass.findViewById(R.id.text_negative);
		textNegative.setText("Cancel");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogJoinnopass.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogJoinnopass.dismiss();
			}
		});

		dialogJoinnopass.show();

	}

	class OpenDataReg extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(context, "Loading",
					"Join On Progress", true);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			cekLoginProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			respondcek();
			progress.dismiss();
		}
	}

	public void respondcek(){

		status = listRespondObjects.get(0).getStatus();
		message = listRespondObjects.get(0).getMessage();

		if(status.equals("1")){
			TourAndTrackingLocationActivity.show(context, pos);
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			Credential.saveCredentialData(context, BaseID.KEY_DATA_TITLE_EVENT, tit);
		}else{
			if(message.equals("You already join this group")){
				TourAndTrackingLocationActivity.show(context, pos);
				Credential.saveCredentialData(context, BaseID.KEY_DATA_TITLE_EVENT, tit);
			}else{
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
	}

	public void cekLoginProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getDataLog(BaseID.URL_JOIN_GROUP);
			if (is == null) {
				networkSlow = true;
			} else {
				parsingCekLogin(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getDataLog(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("gid", gid));
		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		GetInputStream getIS = new GetInputStream(context);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsingCekLogin(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);

		ParseRespond parseRespond = new ParseRespond();
		try {
			listRespondObjects = parseRespond.getArrayListRespondObject(insStr); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
