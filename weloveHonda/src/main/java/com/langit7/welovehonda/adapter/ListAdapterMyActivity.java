package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.CatalogueDetailObject;
import com.langit7.welovehonda.social.SocialWebActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ListAdapterMyActivity extends ArrayAdapter<CatalogueDetailObject>{

	Context context;
	ArrayList<CatalogueDetailObject>listDetail;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	public ListAdapterMyActivity(Context context, int textViewResourceId, ArrayList<CatalogueDetailObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<CatalogueDetailObject>)listDetail;

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.cacheInMemory()
		.showStubImage(R.drawable.imagedefault_slidescreen)
		.showImageForEmptyUri(R.drawable.imagedefault_slidescreen)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.build();

		this.imageLoader = ImageLoader.getInstance();
		imageLoader.init(config);
	}

	public class ViewHolder {
		public ImageView imageViewIcon;
		public RelativeLayout areasemua;
		public TextView title;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int width = metrics.widthPixels;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listpicture2, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.imageViewIcon = (ImageView)rowView.findViewById(R.id.image);
		holder.title = (TextView)rowView.findViewById(R.id.text);
		holder.title.setText(listDetail.get(position).getTitle().replace("BPP", "Download Manual Owner"));
		imageLoader.displayImage(listDetail.get(position).getImages()+"&w="+width, holder.imageViewIcon,options);
		rowView.setTag(holder);
		
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				SocialWebActivity.show(context, listDetail.get(position).getLink());
			}
		});

		return rowView;
	}

}
