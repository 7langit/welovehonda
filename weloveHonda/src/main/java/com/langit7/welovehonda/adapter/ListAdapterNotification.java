package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.NewsDetailActivity;
import com.langit7.welovehonda.NotifDetailActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.NotifDetailsObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ListAdapterNotification extends ArrayAdapter<NotifDetailsObject>{

	Context context;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ArrayList<NotifDetailsObject>listDetail;

	public ListAdapterNotification(Context context, int textViewResourceId, ArrayList<NotifDetailsObject>listDetail,ImageLoader imageLoader,
			DisplayImageOptions options) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.imageLoader = imageLoader;
		this.options = options;
		this.listDetail = (ArrayList<NotifDetailsObject>)listDetail;
	}

	public class ViewHolder {
		public ImageView imageViewIcon;
		public TextView title;
		public TextView tanggal;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listnotif, null);

		final ViewHolder holder = new ViewHolder();
		
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(displaymetrics);

		int w = displaymetrics.widthPixels;
		int h = displaymetrics.heightPixels;
		int hRelative = h / 10;
		
		
		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.imageViewIcon = (ImageView)rowView.findViewById(R.id.image);
		holder.imageViewIcon.getLayoutParams().height = hRelative;
		holder.title = (TextView) rowView.findViewById(R.id.judul);
		holder.title.setText(listDetail.get(position).getTitle());
		holder.tanggal = (TextView) rowView.findViewById(R.id.tanggal);
		holder.tanggal.setText(listDetail.get(position).getDate());
		imageLoader.displayImage(listDetail.get(position).getImg(), holder.imageViewIcon, options);

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//edit
				NotifDetailActivity.show(context, listDetail.get(position).getNid(), listDetail.get(position).getTitle(), listDetail.get(position).getBody(),listDetail.get(position).getImg(), listDetail.get(position).getUrl(), listDetail.get(position).getDate(), true);
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

}
