package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


import com.langit7.welovehonda.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ListAdapterGallery extends BaseAdapter{

	private final Context context;
	public ImageLoader nostraLoader;
	private ArrayList<String> listImageUrl;
	private DisplayImageOptions options;
	@SuppressWarnings("unused")
	private ArrayList<String> listUrl;
	

	public ListAdapterGallery(Context c, ArrayList<String> listImageUrl,ArrayList<String>listUrl) {
		this.context = c;
		this.listImageUrl = listImageUrl;
		this.listUrl = listUrl;

		options = new DisplayImageOptions.Builder()
		.cacheInMemory()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.cacheOnDisc()
		.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.build();

		this.nostraLoader = ImageLoader.getInstance();
		nostraLoader.init(config);

	}

	@Override
	public int getCount() {
		return listImageUrl.size();
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	public class ViewHolder{
		public ImageView imageItem;
		public RelativeLayout areaImage;
		public ProgressBar spinner;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		final ViewHolder holder 		= new ViewHolder();

		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int width = metrics.widthPixels;

		String url = listImageUrl.get(position) + "&w="+width;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listgallery, null);

		holder.imageItem 		= (ImageView) rowView.findViewById(R.id.image); 
		holder.areaImage = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.spinner 			= (ProgressBar) rowView.findViewById(R.id.progressBar1);

		nostraLoader.displayImage(url, holder.imageItem, this.options, new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String imageUri, View view) {
				// TODO Auto-generated method stub
				holder.spinner.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				// TODO Auto-generated method stub
				holder.spinner.setVisibility(View.INVISIBLE);
			}
			
			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				// TODO Auto-generated method stub
				
			}
		});

		rowView.setTag(holder);
		return rowView;
	}

}
