package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.DealerLocationDetailActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.LatLonObject;

public class ListAdapterItem extends ArrayAdapter<LatLonObject>{

	Context context;
	ArrayList<LatLonObject>listSearch;

	public ListAdapterItem(Context context, int textViewResourceId, ArrayList<LatLonObject>listSearch) {
		super(context, textViewResourceId, listSearch);
		this.context = context;
		this.listSearch = (ArrayList<LatLonObject>)listSearch;
	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listone, null);

		ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.title = (TextView) rowView.findViewById(R.id.title);

		holder.title.setText(listSearch.get(position).getTitle());
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				DealerLocationDetailActivity.show(context, listSearch.get(position).getTitle(), listSearch.get(position).getPhone(), "", listSearch.get(position).getAddress(), listSearch.get(position).getLat(), listSearch.get(position).getLon());
			}
		});

		rowView.setTag(holder);
		return rowView;
	}

}
