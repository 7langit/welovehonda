package com.langit7.welovehonda.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.langit7.welovehonda.DealerLocationDetailActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.LatLonObject;

public class ListAdapterSearch extends ArrayAdapter<LatLonObject> implements SectionIndexer, Filterable {

	Context mContext;
	ArrayList<LatLonObject>listSearch;
	ArrayList<String> listName;

	private CustomFilter mFilter;
	private ListView listItem;

	public ListAdapterSearch(Context context, int textViewResourceId, List<LatLonObject> objects, ArrayList<String> listName,
			ListView listItem) {
		super(context, textViewResourceId, objects);
		mContext = context;
		listSearch = (ArrayList<LatLonObject>)objects;
		this.listName = listName;
		this.listItem = listItem;
	}


	public ListAdapterSearch(Context context, int textViewResourceId, ArrayList<LatLonObject> listDirectoryObjects,
			ArrayList<String> listName,ListView listItem) {
		super(context, textViewResourceId);
		mContext = context;
		this.listSearch = listDirectoryObjects;
		this.listName = listName;
		this.listItem = listItem;
	}
	
	@Override
	public Filter getFilter() {

		if (mFilter == null)
			mFilter = new CustomFilter();
		return mFilter;
	}

	@Override
	public void add(LatLonObject object) {
		listSearch.add(object);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return listSearch.size();
	}

	@Override
	public LatLonObject getItem(int position) {
		return (listSearch.get(position));
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public class ViewHolder {
		public TextView phone;
		public TextView title;
		public TextView address;
		public LinearLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listtext, null);

		ViewHolder holder = new ViewHolder();

		holder.areasemua = (LinearLayout)rowView.findViewById(R.id.areasemua);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.address = (TextView) rowView.findViewById(R.id.alamat);
		holder.phone = (TextView) rowView.findViewById(R.id.telp);

		holder.title.setText(listSearch.get(position).getTitle());
		holder.address.setText(listSearch.get(position).getAddress());
		holder.phone.setText(listSearch.get(position).getPhone());
		
		holder.title.setText(Html.fromHtml(listSearch.get(position).getTitle()));
		if (!(listSearch.get(position).getTitle().equals(""))) {
			holder.title.setText(Html.fromHtml(listSearch.get(position).getTitle()));
		} else {
		}

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				DealerLocationDetailActivity.show(mContext, listSearch.get(position).getTitle(), listSearch.get(position).getPhone(), "", listSearch.get(position).getAddress(), listSearch.get(position).getLat(), listSearch.get(position).getLon());
			}
		});

		rowView.setTag(holder);
		return rowView;
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	@Override
	public int getPositionForSection(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getSectionForPosition(int section) {
		// TODO Auto-generated method stub
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < listName.size(); i++) {
			String l = listName.get(i);
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	public class CustomFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub

			constraint = constraint.toString().toLowerCase();
			FilterResults newFilterResults = new FilterResults();
			if (constraint != null && constraint.length() > 0) {
				ArrayList<LatLonObject> auxData = new ArrayList<LatLonObject>();
				for (int i = 0; i < listSearch.size(); i++) {
					if (listSearch.get(i).getTitle().toLowerCase().contains(constraint)) {
						auxData.add(listSearch.get(i));
					}
				}

				newFilterResults.count = auxData.size();
				newFilterResults.values = auxData;
			} else {
				newFilterResults.count = listSearch.size();
				newFilterResults.values = listSearch;
			}

			return newFilterResults;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
			ArrayList<LatLonObject> resultData = new ArrayList<LatLonObject>();
			resultData = (ArrayList<LatLonObject>)results.values;
			listName = new ArrayList<String>();
			for (int i = 0; i < resultData.size(); i++) {
				listName.add(resultData.get(i).getTitle());
			}
			
			ListCityAdapter adapter = new ListCityAdapter(mContext, R.layout.listone, resultData, listName, listItem);
			listItem.setAdapter(adapter);
			notifyDataSetChanged();
		}

	}

}
