package com.langit7.welovehonda.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.langit7.welovehonda.DealerLocationSearchActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.LatLonObject;

public class ListCityAdapter extends ArrayAdapter<LatLonObject> implements SectionIndexer, Filterable {

	Context mContext;
	ArrayList<LatLonObject> listCityObjects;
	ArrayList<String> listName;

	private CustomFilter mFilter;
	private ListView listItem;


	public ListCityAdapter(Context context, int textViewResourceId, List<LatLonObject> objects, ArrayList<String> listName,
			ListView listItem) {
		super(context, textViewResourceId, objects);
		mContext = context;
		listCityObjects = (ArrayList<LatLonObject>)objects;
		this.listName = listName;
		this.listItem = listItem;
	}


	public ListCityAdapter(Context context, int textViewResourceId, ArrayList<LatLonObject> listDirectoryObjects,
			ArrayList<String> listName,ListView listItem) {
		super(context, textViewResourceId);
		mContext = context;
		this.listCityObjects = listDirectoryObjects;
		this.listName = listName;
		this.listItem = listItem;
	}

	@Override
	public Filter getFilter() {

		if (mFilter == null)
			mFilter = new CustomFilter();
		return mFilter;
	}

	public class ViewHolder {
		public RelativeLayout areaListAdapter;
		public TextView cityTitle;
	}

	@Override
	public void add(LatLonObject object) {
		listCityObjects.add(object);
		notifyDataSetChanged();
	}


	@Override
	public int getCount() {
		return listCityObjects.size();
	}

	@Override
	public LatLonObject getItem(int position) {
		return (listCityObjects.get(position));
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		listCityObjects.get(position);

		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listone, null);

		final ViewHolder holder = new ViewHolder();

		holder.cityTitle = (TextView) rowView.findViewById(R.id.title);
		holder.areaListAdapter = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		
		holder.cityTitle.setText(Html.fromHtml(listCityObjects.get(position).getTitle()));
		if (!(listCityObjects.get(position).getTitle().equals(""))) {
			holder.cityTitle.setText(Html.fromHtml(listCityObjects.get(position).getTitle()));
		} else {
		}
		
		holder.areaListAdapter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
//				DealerLocationSearchActivity.show(mContext, listCityObjects.get(position).getLinkdetail(), listCityObjects.get(position).getTid());
			}
		});
		

		return rowView;
	}
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	@Override
	public int getPositionForSection(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getSectionForPosition(int section) {
		// TODO Auto-generated method stub
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < listName.size(); i++) {
			String l = listName.get(i);
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	public class CustomFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub

			constraint = constraint.toString().toLowerCase();
			FilterResults newFilterResults = new FilterResults();
			if (constraint != null && constraint.length() > 0) {
				ArrayList<LatLonObject> auxData = new ArrayList<LatLonObject>();
				for (int i = 0; i < listCityObjects.size(); i++) {
					if (listCityObjects.get(i).getTitle().toLowerCase().contains(constraint)) {
						auxData.add(listCityObjects.get(i));
					}
				}

				newFilterResults.count = auxData.size();
				newFilterResults.values = auxData;
			} else {
				newFilterResults.count = listCityObjects.size();
				newFilterResults.values = listCityObjects;
			}

			return newFilterResults;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
			ArrayList<LatLonObject> resultData = new ArrayList<LatLonObject>();
			resultData = (ArrayList<LatLonObject>)results.values;
			listName = new ArrayList<String>();
			for (int i = 0; i < resultData.size(); i++) {
				listName.add(resultData.get(i).getTitle());
			}
			
			ListCityAdapter adapter = new ListCityAdapter(mContext, R.layout.listone, resultData, listName, listItem);
			listItem.setAdapter(adapter);
			notifyDataSetChanged();
		}

	}

}
