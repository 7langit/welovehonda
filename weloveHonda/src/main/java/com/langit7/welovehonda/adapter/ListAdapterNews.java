package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.NewsDetailActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.NewsObject2;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ListAdapterNews extends ArrayAdapter<NewsObject2>{

	Context context;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ArrayList<NewsObject2>listDetail;

	public ListAdapterNews(Context context, int textViewResourceId, ArrayList<NewsObject2>listDetail,ImageLoader imageLoader,
			DisplayImageOptions options) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.imageLoader = imageLoader;
		this.options = options;
		this.listDetail = (ArrayList<NewsObject2>)listDetail;
	}

	public class ViewHolder {
		public ImageView imageViewIcon;
		public TextView title;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listitem, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.imageViewIcon = (ImageView)rowView.findViewById(R.id.image);
		holder.title = (TextView) rowView.findViewById(R.id.judul);
		holder.title.setText(listDetail.get(position).getTitle());
		imageLoader.displayImage(listDetail.get(position).getImg(), holder.imageViewIcon, options);

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				NewsDetailActivity.show(context, listDetail.get(position).getTitle(), listDetail.get(position).getContent(),listDetail.get(position).getUrl(),listDetail.get(position).getImg());
			}
		});

		rowView.setTag(holder);

		return rowView;
	}

}
