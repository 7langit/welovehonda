package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.CataloguDetail2Object;

public class ListAdapterCatalogue2 extends ArrayAdapter<CataloguDetail2Object>{

	Context context;
	ArrayList<CataloguDetail2Object>listCatalogue;

	public ListAdapterCatalogue2(Context context, int textViewResourceId, ArrayList<CataloguDetail2Object>listCatalogue) {
		super(context, textViewResourceId, listCatalogue);
		this.context = context;
		this.listCatalogue = (ArrayList<CataloguDetail2Object>)listCatalogue;
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class ViewHolder {
		public TextView spec;
		public TextView desc;
		public RelativeLayout areasemua;
		public LinearLayout areadalam;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listmain, null);

		ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.areadalam = (LinearLayout)rowView.findViewById(R.id.areadalam);
		holder.spec = (TextView) rowView.findViewById(R.id.spec);
		holder.desc = (TextView) rowView.findViewById(R.id.desc);

		holder.spec.setText(listCatalogue.get(position).getName());
		holder.desc.setText(listCatalogue.get(position).getDesc());

		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		rowView.setTag(holder);
		
		if(position%2==0){
			rowView.setBackgroundResource(R.color.abu1);
		}else{
			rowView.setBackgroundResource(R.color.abu2);
		}
		
		return rowView;
	}

}
