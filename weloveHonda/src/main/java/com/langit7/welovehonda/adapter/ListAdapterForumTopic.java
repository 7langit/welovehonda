package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.langit7.welovehonda.MyActivityForumTopicDetail;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.LatLonObject;

public class ListAdapterForumTopic extends ArrayAdapter<LatLonObject>{

	Context context;
	ArrayList<LatLonObject>listDetail;
	private Dialog dialogRetweet;
	private int w;
	private int h;

	public ListAdapterForumTopic(Context context, int textViewResourceId, ArrayList<LatLonObject>listDetail) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.listDetail = (ArrayList<LatLonObject>)listDetail;
	}

	public class ViewHolder {
		public TextView title;
		public RelativeLayout areasemua;
		public ImageButton hint;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;
		
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listforum2, null);

		final ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.title = (TextView) rowView.findViewById(R.id.title);
		holder.hint = (ImageButton) rowView.findViewById(R.id.hint);
		holder.title.setText(listDetail.get(position).getTitle());

		holder.hint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String stat = listDetail.get(position).getDesc();
				dialogDesc(stat);
			}
		});

		
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				MyActivityForumTopicDetail.show(context, listDetail.get(position).getNid(), listDetail.get(position).getTitle(),listDetail.get(position).getDetaillink(),listDetail.get(position).getDesc());
			}
		});

		rowView.setTag(holder);

		return rowView;
	}
	
	private void dialogDesc(final String idStatus) {
		dialogRetweet = new Dialog(context, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button2);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}

}
