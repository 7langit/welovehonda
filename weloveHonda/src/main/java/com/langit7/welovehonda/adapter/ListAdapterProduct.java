package com.langit7.welovehonda.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.langit7.welovehonda.ProductPrice2Activity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ListAdapterProduct extends ArrayAdapter<NewsDetailObject>{

	Context context;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ArrayList<NewsObject>listNews;
	ArrayList<NewsDetailObject>listDetail;

	public ListAdapterProduct(Context context, int textViewResourceId, ArrayList<NewsDetailObject>listDetail,ImageLoader imageLoader,
			DisplayImageOptions options) {
		super(context, textViewResourceId, listDetail);
		this.context = context;
		this.imageLoader = imageLoader;
		this.options = options;
		this.listDetail = (ArrayList<NewsDetailObject>)listDetail;
	}

	public class ViewHolder {
		public ImageView imageViewIcon;
		public RelativeLayout areasemua;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;
		
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int width = metrics.widthPixels;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.listpicture, null);

		ViewHolder holder = new ViewHolder();

		holder.areasemua = (RelativeLayout)rowView.findViewById(R.id.areasemua);
		holder.imageViewIcon = (ImageView)rowView.findViewById(R.id.image);
		imageLoader.displayImage(listDetail.get(position).getImages()+"&w="+width, holder.imageViewIcon, options);
		
		holder.areasemua.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				ProductPrice2Activity.show(context, listDetail.get(position).getTid());
				
			}
		});
		
		rowView.setTag(holder);
		return rowView;
	}

}
