package com.langit7.welovehonda.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Donny Adiwilaga on 6/30/2016.
 */

public class fitxImageView extends android.support.v7.widget.AppCompatImageView {
    public fitxImageView(Context context) {
        super(context);
    }

    public fitxImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public fitxImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        try {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
            setMeasuredDimension(width, height);
        } catch (Exception e) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            //e.printStackTrace();
        }
    }
}