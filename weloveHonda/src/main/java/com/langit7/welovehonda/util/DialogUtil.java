package com.langit7.welovehonda.util;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.langit7.welovehonda.R;


/**
 * Created by donny on 10/30/15.
 */
public class DialogUtil {


    public static Dialog createDealerDetailDialog(final Activity ctx, String message,String image) {
        ViewGroup vg = null;
        View view = ctx.getLayoutInflater().inflate(R.layout.dialog_dealer_detail, vg);
        final Dialog dialog = new Dialog(ctx);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//        (view.findViewById(R.id.main_layout)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
        (view.findViewById(R.id.frame)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //donothing
            }
        });


        final TextView tcontent = (TextView) view.findViewById(R.id.tcontent);
        final fitxImageView img = (fitxImageView) view.findViewById(R.id.img);

        if(message!=null || !message.equalsIgnoreCase("null") || message.length()>0) {
            tcontent.setText(Html.fromHtml(message));
        }else {
            tcontent.setVisibility(View.GONE);
        }
        if(image!=null) {
           Glide.with(ctx).load(image).dontTransform().into(img);
            img.setVisibility(View.VISIBLE);
        }else {
            img.setVisibility(View.GONE);
        }


        TextView tclose = (TextView) view.findViewById(R.id.tclose);
        tclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();


            }
        });


        dialog.setContentView(view);
        dialog.show();

        return dialog;
    }




}
