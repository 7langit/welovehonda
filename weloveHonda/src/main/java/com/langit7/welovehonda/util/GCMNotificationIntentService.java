package com.langit7.welovehonda.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.langit7.welovehonda.NotifDetailActivity;
import com.langit7.welovehonda.ProductCatalogueMainActivity;
import com.langit7.welovehonda.R;

public class GCMNotificationIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GCMNotificationIntentService() {
		super("GcmIntentService");
	}

	public static final String TAG = "GCMNotificationIntentService";

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				for (int i = 0; i < 3; i++) {
					Log.i(TAG,
							"Working... " + (i + 1) + "/5 @ "
									+ SystemClock.elapsedRealtime());
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}

				}
				Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());

				sendNotification("" + extras.get(Config.MESSAGE_KEY));
				Log.i(TAG, "Received: " + extras.toString());
				Log.e(TAG, "Received: " + extras.get(Config.MESSAGE_KEY));
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg) {
		Log.e(TAG, "msg: " + msg);

		String nid = "";
		String type = "";
		String title = "";

		JSONObject jo;
		try {
			jo = new JSONObject(msg);
			if (jo.has("nid")) {
				nid = jo.optString("nid");
				Log.e(TAG, "nid: " + nid);
			}
			if (jo.has("title")) {
				title = jo.optString("title");
			}
			if (jo.has("type")) {
				type = jo.optString("type");
				Log.e(TAG, "type: " + type);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// edit lagi activity yang mau diaktifkan
		if (type.equals("product")) {
			Credential.saveBooleanCredentialData(getApplicationContext(),
					BaseID.KEY_REG_ID, true);
			Credential.saveCredentialData(getApplicationContext(),
					BaseID.KEY_REG_NID, nid);
			mNotificationManager = (NotificationManager) this
					.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					new Intent(this, ProductCatalogueMainActivity.class), 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle("WeLoveHonda")
					.setDefaults(Notification.DEFAULT_ALL)
					.setAutoCancel(true)
					.setContentText(title)
					.setStyle(
							new NotificationCompat.BigTextStyle()
									.bigText(title))
					.setContentIntent(contentIntent);

			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		} else {
			Credential.saveCredentialData(getApplicationContext(),
					BaseID.KEY_REG_NID, nid);
			mNotificationManager = (NotificationManager) this
					.getSystemService(Context.NOTIFICATION_SERVICE);
			Credential.saveCredentialData(getApplicationContext(),
					BaseID.KEY_REG_NID, nid);
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					new Intent(this, NotifDetailActivity.class), 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle("WeLoveHonda")
					.setDefaults(Notification.DEFAULT_ALL)
					.setAutoCancel(true)
					.setContentText(title)
					.setStyle(
							new NotificationCompat.BigTextStyle()
									.bigText(title))
					.setContentIntent(contentIntent);

			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		}

	}
}
