package com.langit7.welovehonda.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkManager {

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public static boolean statusNetwork(Context context, String urlNLU) {
		// TODO Auto-generated method stub
		String urlFix = urlNLU;
		if (urlNLU.contains("#")) {
			urlFix = correctingURL(urlNLU);
		}

		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
			try {
				URL url = new URL(urlFix);
				HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
				urlc.setConnectTimeout(30000);
				urlc.connect();
				//				Log.e("sC_CODE", "Status Code = "+urlc.getResponseCode());
				if (urlc.getResponseCode() == 200) {
					return true;
				} else {
					return false;
				}
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
				return false;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	public static String correctingURL(String url) {
		String correctUrl = url;
		if (url.contains("#")) {
			correctUrl = url.replace("#", "%23");
		}
		return correctUrl;
	}
}
