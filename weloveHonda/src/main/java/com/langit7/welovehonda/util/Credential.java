package com.langit7.welovehonda.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;

import java.io.UnsupportedEncodingException;


public class Credential extends Activity {

	public static String encode(String text) {
		// Sending side
		byte[] data = new byte[0];
		try {
			data = text.getBytes("UTF-8");
			String base64 = Base64.encodeToString(data, Base64.DEFAULT);
			return base64;
		} catch (UnsupportedEncodingException e) {
			return "";
		}

	}

	public static String decode(String text) {
		// Receiving side

		try {
			byte[] data = Base64.decode(text, Base64.DEFAULT);
			String decoded = new String(data, "UTF-8");
			return decoded;
		} catch (UnsupportedEncodingException e) {
			return "";
		}

	}

	public static void saveCredentialData(Context context, String KEY, String value) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		Editor editorPreference = credentialDataPref.edit();
		editorPreference.putString(KEY, value);
		editorPreference.commit();
	}
	
	public static String getCredentialData(Context context, String KEY) {
		String valueReturn = "";
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		valueReturn = credentialDataPref.getString(KEY, "");

		return valueReturn;
	}

	public static void saveCredentialDataInt(Context context, String KEY, int idLegend) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		Editor editorPreference = credentialDataPref.edit();
		editorPreference.putInt(KEY, idLegend);
		editorPreference.commit();
	}

	public static int getCredentialDataInt(Context context, String KEY) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		int valueReturn = credentialDataPref.getInt(KEY, 0);
		return valueReturn;
	}
	
	public static Long getCredentialDataLong(Context context, String KEY) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		Long valueReturn = credentialDataPref.getLong(KEY, 0);
		return valueReturn;
	}
	
	public static void saveCredentialDataLong(Context context, String KEY, Long idLegend) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		Editor editorPreference = credentialDataPref.edit();
		editorPreference.putLong(KEY, idLegend);
		editorPreference.commit();
	}
	
	public static void saveBooleanCredentialData(Context context, String KEY, Boolean value) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		Editor editorPreference = credentialDataPref.edit();
		editorPreference.putBoolean(KEY, value);
		editorPreference.commit();
	}
	
	public static boolean getBooleanCredentialData(Context context, String KEY) {
		SharedPreferences credentialDataPref = context.getSharedPreferences(BaseID.KEY_PREFERENCE, MODE_PRIVATE);
		boolean valueReturn = credentialDataPref.getBoolean(KEY, false);
		return valueReturn;
	} 
}
