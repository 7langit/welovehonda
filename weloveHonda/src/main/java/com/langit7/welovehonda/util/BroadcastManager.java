package com.langit7.welovehonda.util;

import java.io.File;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.langit7.welovehonda.R;

public class BroadcastManager extends BroadcastReceiver {
	private long enqueue;

	private DownloadManager dm;
	private NotificationManager mNotificationManager;
	private String titleFile;
	public static final int NOTIFICATION_ID = 1;

	public static final String FINISH_DOWNLOAD_ACTIVITY = "finishDownloadActivity";
	public static final String FINISH_BUY_ACTIVITY = "finishBuyActivity";

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		doOnReceive(context, intent);
	}

	private void doOnReceive(Context context, Intent intent) {
		String action = intent.getAction();
		dm = (DownloadManager) context
				.getSystemService(Context.DOWNLOAD_SERVICE);
		enqueue = Credential.getCredentialDataLong(context, "enqueue");
		if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
			long downloadId = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, 0);

			titleFile = Credential.getCredentialData(context, "titleFile"
					+ downloadId);

			Query query = new Query();
			query.setFilterById(downloadId);
			Cursor c = dm.query(query);
			if (c.moveToFirst()) {
				int columnIndex = c
						.getColumnIndex(DownloadManager.COLUMN_STATUS);
				if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
					makeToast(context, "Download Kamu Berhasil!");
					sendNotification("Download Kamu Berhasil!", context);
				}
			}
		}
	}

	private void sendNotification(String msg, Context context) {
		String credentialData = Credential.getCredentialData(context, BaseID.KEY_TITTLE_PATH);
		
		
		mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
//		Intent intent = new Intent();
//	    intent.setAction(android.content.Intent.ACTION_VIEW);
//	    File file = new File(Environment.getExternalStorageDirectory() + "/Download/", credentialData);
//	    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
//
//	    PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, i, 0);
	    
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle("We Love Honda")
				.setDefaults(Notification.DEFAULT_ALL)
				.setAutoCancel(true)
				.setContentText(titleFile)
				.setContentIntent(pIntent)
				.setStyle(
						new NotificationCompat.BigTextStyle()
								.bigText(titleFile));
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}

	protected void makeToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

}
