package com.langit7.welovehonda.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class BaseDialogs extends Activity {

	public static ProgressDialog mProgressDialog;

	public static void showDialog(Context context, String a, String b) {
		if (mProgressDialog == null) {
			setProgressDialog(context, a, b);
		}
		mProgressDialog.show();
	}

	public static void hideDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	public static void setProgressDialog(Context context, String a, String b) {
		mProgressDialog = new ProgressDialog(context);
//		mProgressDialog.setTitle(a);
		mProgressDialog.setMessage(b);
	}

}
