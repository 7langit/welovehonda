package com.langit7.welovehonda.util;

public class GetHeader {

	public static String setContentHeader(String header) {
		// TODO Auto-generated method stub
		String contentHeader = "";
		String[] temp = header.split(" ");
		if (temp.length > 1) {
			String kalimatBelakang = "";
			for (int i = 1; i < temp.length; i++) {
				kalimatBelakang += " " + temp[i];
			}
			contentHeader = "<b>" + temp[0] + "</b> " + kalimatBelakang;
		} else {
			contentHeader = "<b>" + header + "</b>";
		}
		return contentHeader;
	}

}
