package com.langit7.welovehonda.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;

public class GetInputStream {

	Context context;

	public GetInputStream(Context mContext) {
		// TODO Auto-generated constructor stub
		context = mContext;
	}

	public InputStream getInputStream(String url2) {
		InputStream is = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url2);
		HttpResponse response = null;
		
	
		try {
			if (!NetworkManager.statusNetwork(context, url2)) {
				is = null;
			} else {
				response = httpclient.execute(httpget);
				Log.e("RESP_CODE", "response_code = " + response.getStatusLine().getStatusCode());

				Log.e("RESP_MESSAGE", "response_message = " + response.getStatusLine().getReasonPhrase());
				
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					is = response.getEntity().getContent();
				} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
					is = null;
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}

	public InputStream postInputStream(String url2, List<NameValuePair> nameValuePairs2) {
		InputStream is = null;


		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url2);
		HttpResponse response = null;

		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs2));

			response = httpclient.execute(httppost);
			
			Log.e("RESP_CODE", "response_code = " + response.getStatusLine().getStatusCode());

			Log.e("RESP_MESSAGE", "response_message = " + response.getStatusLine().getReasonPhrase());
			
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				is = response.getEntity().getContent();
			} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
				is = null;
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}

	public InputStream putInputStream(String url2, List<NameValuePair> nameValuePairs2) {
		InputStream is = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPut httpput = new HttpPut(url2);
		HttpResponse response = null;
		try {
			httpput.setEntity(new UrlEncodedFormEntity(nameValuePairs2));

			response = httpclient.execute(httpput);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				is = response.getEntity().getContent();
			} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
				is = null;
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}

	public InputStream postWithHeaderInputStream(String url2, List<NameValuePair> nameValuePairs2, String cookie) {
		InputStream is = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url2);
		httppost.setHeader("Cookie", cookie);
		HttpResponse response = null;
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs2));

			response = httpclient.execute(httppost);
			Log.e("RESP_CODE", "response_code = " + response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				is = response.getEntity().getContent();
			} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
				is = null;
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}
}
