package com.langit7.welovehonda.util;

import static com.langit7.welovehonda.util.Credential.decode;

public class BaseID {

	public static final String URL_NEWS = decode("aHR0cDovL3d3dy53ZWxvdmVob25kYS5jb20vP2pzb249Z2V0X3JlY2VudF9wb3N0cw==");
	public static final String URL_TRACKING = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC90cmFja2luZy5qc29u");
	public static final String URL_FORUM_TOPIC_DETAIL = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9mb3J1bS9nZXRfZm9ydW1fdG9waWNfY29tbWVudC5qc29u");
	public static final String URL_FORUM_LIST = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9mb3J1bS9nZXRfZm9ydW0uanNvbg==");


	public static final String URL_FORUM_TOPIC = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9mb3J1bS9nZXRfZm9ydW1fdG9waWMuanNvbg==");
	public static final String URL_REGISTER = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vc2l0ZXMvcmVnaXN0ZXIucGhw");
	public static final String URL_LOGIN =    decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vc2l0ZXMvbG9naW4ucGhw");
	public static final String URL_CREATE_GROUP = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9ncm91cC10cmFja2luZy9yZXF1ZXN0X2dyb3VwLmpzb24=");

	public static final String URL_TRACK_LIST = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9ncm91cC10cmFja2luZy9nZXRfZ3JvdXBsaXN0Lmpzb24=");
	public static final String URL_PRICE_2 = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0L2dldF9saXN0X3Byb2R1Y3RfcHJpY2UuanNvbg==");
	public static final String URL_PRODUCT_PRICE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0L2dldF9wcm9kdWN0X3ByaWNlLmpzb24=");
	public static final String URL_TWITTER_TIMELINE = decode("aHR0cDovL2NhY2hlLnguN2xhbmdpdC5jb20vYXBpdDEvc3RhdHVzZXMvdXNlcl90aW1lbGluZS5qc29uP2luY2x1ZGVfZW50aXRpZXM9dHJ1ZSZpbmNsdWRlX3J0cz10cnVlJnRyaW1fdXNlcj10cnVlJnNjcmVlbl9uYW1lPXdlbG92ZWhvbmRhJmNvdW50PTEw");
	public static final String URL_LATLON_SEARCH = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9kZWFsZXItbG9jYXRpb24vZ2V0X3NlYXJjaC5qc29u");

	public static final String URL_CITY_LIST = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9kZWFsZXItbG9jYXRpb24vZ2V0X2NpdHkuanNvbg==");
	public static final String URL_POSITION = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9kZWFsZXItbG9jYXRpb24vZ2V0X25lYXJlc3QuanNvbg==");
	public static final String URL_PUBLIC_PLACE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wdWJsaWMtcGxhY2UvZ2V0X25lYXJlc3QuanNvbg==");
	public static final String URL_CATALOGUE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0LWNhdGFsb2d1ZS9nZXRfcHJvZHVjdF9jYXRhbG9ndWUuanNvbg==");
	public static final String URL_ALL_CATALOGUE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0LWNhdGFsb2d1ZS9nZXRfYWxsX3Byb2R1Y3RfY2F0YWxvZ3VlLmpzb24=");

	public static final String URL_DETAIL_CATALOGUE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0LWNhdGFsb2d1ZS9nZXRfZGV0YWlsX3Byb2R1Y3QuanNvbg==");
	public static final String URL_HIGHLIGHT = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9pbWFnZS1jb3Zlci9nZXRfaW1hZ2UuanNvbg==");
	public static final String URL_POST = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9mb3J1bS9zZXRfY29tbWVudC5qc29u");
	public static final String URL_LIST_MYACTIVITY = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9teS1hY3Rpdml0eS9nZXRfbXlfYWN0aXZpdHkuanNvbg==");
	public static final String URL_FORGOT_PASS = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vc2l0ZXMvdXNlcl9mb3Jnb3RfcGFzcy5waHA=");
	public static final String URL_JOIN_GROUP = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9ncm91cC10cmFja2luZy9qb2luX2dyb3VwLmpzb24=");

	public static final String URL_LIST_FRIEND = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9ncm91cC10cmFja2luZy9nZXRfdXNlcl9ncm91cGxpc3QuanNvbg==");
	public static final String URL_START_TRACK = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC90cmFja2luZy9zdGFydF90cmFjay5qc29u");
	public static final String URL_STOP_TRACK = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC90cmFja2luZy9zdG9wX3RyYWNrLmpzb24=");
	public static final String URL_LIST_TRACK_HISTORY = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC90cmFja2luZy9nZXRfdHJhY2suanNvbg==");
	public static final String URL_LEAVE_GROUP = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9ncm91cC10cmFja2luZy9sZWF2ZV9ncm91cC5qc29u");
	public static final String URL_ALL_CATALOGUE_PRICE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0L2dldF9hbGxfcHJpY2VfY2F0YWxvZ3VlLmpzb24=");

	public static final String URL_DETAIL_CATALOGUE_PRICE = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wcm9kdWN0L2dldF9kZXRhaWxfcHJvZHVjdF9wcmljZS5qc29u");
	public static final String URL_LIST_ALL_MYACTIVITY = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9teS1hY3Rpdml0eS9nZXRfYWxsX215X2FjdGl2aXR5Lmpzb24=");
	public static final String URL_ALL_FORUM_TOPIC = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9mb3J1bS9nZXRfYWxsX2ZvcnVtX3RvcGljLmpzb24=");
	public static final String URL_REG_ID = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wdXNoX25vdGlmX2FobS9yZWdfYW5kcm9pZC5qc29u");
	public static final String URL_NOTIF = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wdXNoX25vdGlmX2FobS9nZXRfbGlzdF9ub3RpZmljYXRpb24uanNvbg==");
	public static final String URL_DETAIL_NOTIF = decode("aHR0cDovL2FobS5jbXMubGFuZ2l0Ny5jb20vcmVzdC9wdXNoX25vdGlmX2FobS9nZXRfZGV0YWlsX25vdGlmaWNhdGlvbi5qc29u");
	

	public static final String KEY_DATA_PRODUCT_PRICE = decode("cHJpY2VfZGF0YQ==");
	public static final String KEY_DATA_JOIN_GROUP_TRACK = decode("am9pbl9ncm91cF90cmFja19kYXRh");
	public static final String KEY_DATA_START_TRACK = decode("c3RhcnRfdHJhY2tfYmVnaW5fZGF0YQ==");
	public static final String KEY_DATA_LIST_HISTORY_FRIEND = decode("bGlzdF9oaXN0b3J5X2ZyaWVuZF90cmFja19kYXRh");
	public static final String KEY_DATA_PRODUCT_PRICE2 = decode("cHJpY2UyX2RhdGE=");

	public static final String KEY_DATA_PUBLIC_PLACE = decode("cHVibGljX3BsYWNlX2RhdGE=");
	public static final String KEY_DATA_DEALER_DETAIL = decode("ZGVhbGVyX2RldGFpbF9kYXRh");
	public static final String KEY_DATA_IMAGE = decode("aHR0cDovL29oZGlvLmZtL1N0eWxlL2ltYWdlcy8=");
	public static final String KEY_DATA_CATALOGUE = decode("Y2F0YV9kYXRh");
	public static final String KEY_DATA_CATALOGUE2 = decode("Y2F0YTJfZGF0YQ==");
	public static final String KEY_DATA_CATALOGUE3 = decode("Y2F0YTNfZGF0YQ==");

	public static final String KEY_DATA_DETAIL_NOTIF = decode("Y2F0YTRfZGF0YQ==");
	public static final String KEY_DATA_FB = decode("ZmJfZGF0YQ==");
	public static final String KEY_DATA_TRACKID = decode("dHJhY2tpZF9kYXRh");
	public static final String KEY_DATA_CITY = decode("Y2l0eV9kYXRhXzI=");
	public static final String KEY_DATA_NEWS = decode("bmV3c19kYXRh");

	public static final String KEY_DATA_NOTIF = decode("bm90aWZfZGF0YQ==");
	public static final String KEY_DATA_FORUM = decode("Zm9ydW1fbGlzdF9kYXRh");
	public static final String KEY_DATA_FORUM_TRACK = decode("Zm9ydW1fdHJhY2tfbGlzdF9kYXRh");
	public static final String KEY_DATA_TRACKING_LIST = decode("dHJhY2tpbmdfbGlzdF9kYXRh");
	public static final String KEY_DATA_FORUM_TOPIC_DET = decode("Zm9ydW1fdG9waWNfZGV0YWls");

	public static final String KEY_DATA_PRODUCT_PRICE_2 = decode("cHJpY2VfZGF0YV8y");
	public static final String KEY_PREFERENCE = decode("c2hhcmVkX3dlbG92ZWhvbmRh");
	public static final String KEY_DATA_LOGIN = decode("bG9naW5fZGF0YQ==");
	public static final String KEY_TIMELINE_TWITTER = decode("dGltZWxpbmVfZGF0YQ==");
	public static final String KEY_ID_TWIT = decode("dHdpdF9pZA==");

	public static final String KEY_PREF_REG = decode("cHJlZl9yZWc=");
	public static final String KEY_DATA_LOGIN_FB = decode("ZGF0YV9sb2dpbl9mYg==");
	public static final String KEY_HIGHLIGHT = decode("aGlnaGxpZ2h0X2RhdGE=");
	public static final String KEY_DATA_USERNAME = decode("dXNlcm5hbWVfZGF0YQ==");

	public static final String KEY_DATA_PASSWORD = decode("cGFzc3dvcmRfZGF0YQ==");
	public static final String KEY_DATA_EMAIL = decode("ZW1haWxfZGF0YQ==");
	public static final String KEY_DATA_UID = decode("dWlkX2RhdGE=");
	public static final String KEY_DATA_GID = decode("Z2lkX2RhdGE=");
	public static final String KEY_DATA_USER_NAME_LIST = decode("dXNlcm5hbWVfbGlzdF9kYXRh");

	public static final String KEY_DATA_GID_LIST = decode("bGlzdF9naWRfZGF0YQ==");
	public static final String KEY_DATA_LIST_EVENT = decode("bGlzdF9ldmVudF9kYXRh");
	public static final String KEY_DATA_POSITION = decode("cG9zaXRpb25fZGF0YQ==");
	public static final String KEY_DATA_POSITION_TRACK = decode("cG9zaXRpb25fdHJhY2tfZGF0YQ==");
	public static final String KEY_DATA_AVA = decode("YXZhdGFyX2RhdGE=");
	public static final String KEY_DATA_TITLE_EVENT = decode("ZXZlbnRfdGl0bGVfZGF0YQ==");
	
	public static final String KEY_CLASS_PRICE = decode("a2V5X2NsYXNzX3ByaWNl");
	public static final String KEY_CLASS_CATALOGE = decode("a2V5X2NsYXNzX2NhdGFsb2d1ZQ==");
	public static final String KEY_CLASS_ACTIVITY = decode("a2V5X2NsYXNzX2FjdGl2aXR5");
	public static final String KEY_CLASS = decode("a2V5X2NsYXNz");
	public static final String KEY_TITTLE_PATH = decode("a2V5X3RpdHRsZV9wYXRo");
	public static final String KEY_REG_ID = decode("a2V5X3JlZ19pZA==");
	public static final String KEY_REG_NID = decode("a2V5X3JlZ19uaWQ=");
	
	public static final String DIR = decode("L1dlTG92ZUhvbmRhL2RvYy8=");
	public static String CONSUMER_KEY = decode("MU1UUEt1YWcwa0lpVlJLR2NxaWhn");
	public static String CONSUMER_SECRET = decode("eWNsVUxENWdWYUw2cFJoTzI2TEkzQlFyWko0ZmtnOWg0MFBSWkxuclU=");
	public static final String KEY_DATA_TW_AT_TOK   = decode("a2V5X2RhdGFfdHdfYXRfdG9r");
	public static final String KEY_DATA_TW_AT_SEC   = decode("a2V5X2RhdGFfdHdfYXRfc2Vj");

	public static String PREFERENCE_NAME = decode("dHdpdHRlcl9vYXV0aA==");
	public static final String PREF_KEY_SECRET = decode("b2F1dGhfdG9rZW5fc2VjcmV0");
	public static final String PREF_KEY_TOKEN = decode("b2F1dGhfdG9rZW4=");

	public static final String CALLBACK_URL = decode("b2F1dGg6Ly93ZWxvdmVob25kYQ==");
	public static final String CALLBACK_URL2 = decode("b2F1dGg6Ly93ZWxvdmVob25kYTI=");

	public static final String IEXTRA_AUTH_URL = decode("YXV0aF91cmw=");
	public static final String IEXTRA_OAUTH_VERIFIER = decode("b2F1dGhfdmVyaWZpZXI=");
	public static final String IEXTRA_OAUTH_TOKEN = decode("b2F1dGhfdG9rZW4=");

	//facebook
	public static final String FACEBOOK_APP_ID = decode("MTkyMjU1MjI0Mjk1OTM2");
	public static final String FACEBOOK_APP_SECRET = decode("YTBkMDYwYjJmMzFlYmU3MjNjNGQ0YjU5OWY3MTIwZDA=");
	public static final String[] FACEBOOK_PERMISSION = {
			decode("cHVibGlzaF9zdHJlYW0="),
			decode("ZW1haWw="),
			decode("cHVibGlzaF9jaGVja2lucw=="),
			decode("dXNlcl9zdGF0dXM="),
			decode("dXNlcl9pbnRlcmVzdHM="),
			decode("dXNlcl9saWtlcw==")
	};
	public static final String FACEBOOK_ATOK = decode("YWNjZXNzX3Rva2Vu");
	public static final String FACEBOOK_CALLBACK_URL = decode("aHR0cDovL2xvY2FsaG9zdA==");
	public static final String FACEBOOK_AEX = decode("YWNjZXNzX2V4cGlyZXM=");
	public static final String FACEBOOK_URL = decode("");
}
