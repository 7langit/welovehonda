package com.langit7.welovehonda;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterTrackingListMeetingPoint;
import com.langit7.welovehonda.object.CatalogueDetail3Object;
import com.langit7.welovehonda.object.NewsDetailObject;
import com.langit7.welovehonda.object.NewsObject;
import com.langit7.welovehonda.parse.ParseNews;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

public class TourAndTrackingListMeetingPointActivity extends Activity{

	private ListAdapterTrackingListMeetingPoint listMenu;
	private ListView listViewTrack;
	@SuppressWarnings("unused")
	private String gid;
	private String insStr;
	ArrayList<NewsObject>listForumObject;
	ArrayList<NewsDetailObject> listDetailObject;
	ArrayList<CatalogueDetail3Object> listPlace;
	private int position;
	private ImageButton buttHint;
	private Dialog dialogRetweet;
	private int w;
	private int h;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking List History Page");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_tracking_map_layout2);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		gid = Credential.getCredentialData(this, BaseID.KEY_DATA_GID);
		listViewTrack = (ListView)findViewById(R.id.listView);
		buttHint = (ImageButton)findViewById(R.id.hint);
		listViewTrack.setVisibility(View.VISIBLE);
		position = (int) getIntent().getIntExtra("POS", 0);
		
		buttHint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogDesc("Menampilkan list peserta pada group");
			}
		});

		
	}

	public void init(){

		listForumObject = new ArrayList<NewsObject>();
		listDetailObject = new ArrayList<NewsDetailObject>();
		listPlace = new ArrayList<CatalogueDetail3Object>();

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		try {
			init();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onResume();
	}

	public void showView(){
		
		insStr = Credential.getCredentialData(this, BaseID.KEY_DATA_LIST_EVENT);
		ParseNews parsingForum = new ParseNews();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
			listDetailObject = listForumObject.get(0).getListDetailObject();
			listPlace = listDetailObject.get(position).getListPlace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		listMenu = new ListAdapterTrackingListMeetingPoint(this, R.layout.listitem, listPlace);
		listViewTrack.setAdapter(listMenu);
	}
	
	private void dialogDesc(final String idStatus) {
		dialogRetweet = new Dialog(this, R.style.Theme_Transparent);
		dialogRetweet.setContentView(R.layout.dialog_text_and_2button2);
		dialogRetweet.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogRetweet.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogRetweet.findViewById(R.id.dialog_message);
		message.setText(idStatus);
		message.setVisibility(View.VISIBLE);

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogRetweet.dismiss();
			}
		});

		dialogRetweet.show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, int pos) {
		Intent intent = new Intent(context, TourAndTrackingListMeetingPointActivity.class);
		Bundle b = new Bundle();
		b.putInt("POS", pos);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
