package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterCatalogue2;
import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.CatalogueDetailObject;
import com.langit7.welovehonda.object.CatalogueObject;
import com.langit7.welovehonda.parse.ParseCatalogue;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ProductCatalogueMainActivity extends Activity {
	private Button buttonColor;
	private Button buttonMainFeatures;
	String insStr;
	ListView listViewMenu;
	ListAdapterCatalogue2 listMenu;
	ArrayList<CatalogueObject> listCataObject = new ArrayList<CatalogueObject>();
	ArrayList<CatalogueDetailObject> listCataloObject = new ArrayList<CatalogueDetailObject>();
	ArrayList<CataloguDetail2Object> listMainObject = new ArrayList<CataloguDetail2Object>();
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private ImageView imgView;
	private Button buttonSpesification;
	private String code;
	private Boolean networkSlow = false;
	private String KEY_DATA;
	private String url;
	private SharedPreferences prefData;
	private ProgressBar pr;
	private BroadcastReceiver logoutReceiver;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Product Catalogue Main Features");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_catalogue_layout);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		pr = (ProgressBar) findViewById(R.id.progressBar);
		listViewMenu = (ListView) findViewById(R.id.listView);
		buttonSpesification = (Button) findViewById(R.id.button1);
		buttonColor = (Button) findViewById(R.id.button2);
		imgView = (ImageView) findViewById(R.id.imageView);
		buttonMainFeatures = (Button) findViewById(R.id.button3);
		buttonSpesification
				.setBackgroundResource(R.drawable.button_specificationf);

		try {
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}

	public void init() {

		boolean isFromNotif = Credential.getBooleanCredentialData(
				getApplicationContext(), BaseID.KEY_REG_ID);
		url = BaseID.URL_DETAIL_CATALOGUE;
		if (isFromNotif) {
			String nid = Credential.getCredentialData(getApplicationContext(),
					BaseID.KEY_REG_NID);
			code = nid;
		} else {
			code = (String) getIntent().getSerializableExtra("CODE");
		}
		listCataObject = new ArrayList<CatalogueObject>();
		listCataloObject = new ArrayList<CatalogueDetailObject>();
		KEY_DATA = BaseID.KEY_DATA_CATALOGUE3 + code;
		prefData = getPreferences(MODE_PRIVATE);
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder().cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnLoading(R.drawable.imagedefault_slidescreen)
				.build();
	}

	class OpenData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				// cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					// Ambil dari persistent
					networkSlow = false;
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				networkSlow = false;
				parsing(is);
			}// end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));
		nameValuePairs.add(new BasicNameValuePair("nid", code));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);
		Credential.saveCredentialData(ProductCatalogueMainActivity.this,
				BaseID.KEY_DATA_CATALOGUE2, insStr);

		ParseCatalogue parsingCata = new ParseCatalogue();
		try {
			listCataObject = parsingCata.getArrayListObject(insStr);
			if (listCataObject == null) {
				listCataObject = new ArrayList<CatalogueObject>();
			}
			listCataloObject = listCataObject.get(0).getListDetail2Object();
			if (listCataloObject == null) {
				listCataloObject = new ArrayList<CatalogueDetailObject>();
			}
			listMainObject = listCataloObject.get(0).getListDetail4Object();
			if (listMainObject == null) {
				listMainObject = new ArrayList<CataloguDetail2Object>();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView() {

		try {
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			imageLoader.displayImage(listCataloObject.get(0).getImagescover()
					+ "&w=" + width, imgView, options);
			listMenu = new ListAdapterCatalogue2(this, R.layout.listmain,
					listMainObject);
			listViewMenu.setAdapter(listMenu);
			buttonColor.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					ProductCatalogueColorActivity.show(
							ProductCatalogueMainActivity.this, 0, listCataloObject
									.get(0).getImagescover(), code, url);
				}
			});

			buttonMainFeatures.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ProductCatalogueMainFeaturesActivity.show(
							ProductCatalogueMainActivity.this, code);
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		// overridePendingTransition(R.anim.slide_in_left,
		// R.anim.slide_out_right);
		overridePendingTransition(0, 0);
		super.onBackPressed();
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(ProductCatalogueMainActivity.this,
				"You have network problem, please check your network setting",
				Toast.LENGTH_LONG).show();
	}

	public static void show(Context context, String tid, String img) {
		Credential.saveBooleanCredentialData(context, BaseID.KEY_REG_ID, false);
		Intent intent = new Intent(context, ProductCatalogueMainActivity.class);
		Bundle b = new Bundle();
		b.putString("CODE", tid);
		b.putString("URL", img);
		intent.putExtras(b);
		context.startActivity(intent);
		// ((Activity)context).overridePendingTransition(R.anim.slide_in_right,
		// R.anim.slide_out_left);
		((Activity) context).overridePendingTransition(0, 0);
	}

	public static void show(Context context, String tid) {
		Credential.saveBooleanCredentialData(context, BaseID.KEY_REG_ID, false);
		Intent intent = new Intent(context, ProductCatalogueMainActivity.class);
		Bundle b = new Bundle();
		b.putString("CODE", tid);
		intent.putExtras(b);
		context.startActivity(intent);
		// ((Activity)context).overridePendingTransition(R.anim.slide_in_right,
		// R.anim.slide_out_left);
		((Activity) context).overridePendingTransition(0, 0);
	}

}
