package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListCityAdapter;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class DealerLocationCityActivity extends Activity{

	boolean networkSlow;
	String url,KEY_DATA;

	ArrayList<LatLonObject> listLatLonObject;

	SharedPreferences prefData;

	ListView listViewMenu;
	ListCityAdapter listMenu;
	private ArrayList<String> listName;
	ProgressBar pr;
	String keysearch;
	private EditText inputsearch;
	private View buttonNearest;
	private View buttonSearch;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Dealer Location City");
	}

	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_dealer_layout2);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		listViewMenu = (ListView) findViewById(R.id.listView);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		inputsearch = (EditText)findViewById(R.id.search);
		buttonNearest = (Button)findViewById(R.id.button1);
		buttonSearch = (Button)findViewById(R.id.button2);
		
		buttonNearest.setVisibility(View.GONE);

		pr.setVisibility(View.INVISIBLE);
		buttonSearch.setBackgroundResource(R.drawable.button_searchdealerf);

		inputsearch.setText("");
		buttonNearest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				DealerLocationActivity.show(DealerLocationCityActivity.this);

			}
		});

		buttonSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				DealerLocationSearchActivity.show(DealerLocationCityActivity.this);
			}
		});

		try {
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		try {
			inputsearch.setText("");
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onResume();
	}

	private void init(){

		url = BaseID.URL_CITY_LIST;
		listLatLonObject = new ArrayList<LatLonObject>();
		prefData = getPreferences(MODE_PRIVATE);
		KEY_DATA = BaseID.KEY_DATA_CITY;

	}

	class OpenData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("text", "city"));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

		ParseLatLon parsingSearch = new ParseLatLon();
		try {
			listLatLonObject = parsingSearch.getArrayListObject(insStr);
			extracted();
			listName = new ArrayList<String>();
			for (int i = 0; i < listLatLonObject.size(); i++) {
				listName.add(listLatLonObject.get(i).getTitle());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		try {
			listMenu = new ListCityAdapter(this, R.layout.listone, listLatLonObject, listName,listViewMenu);
			listViewMenu.setAdapter(listMenu);
			listViewMenu.setTextFilterEnabled(true);

			TextWatcher filterTextWatcher = new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

					listMenu.getFilter().filter(s);
				}

				@Override
				public void afterTextChanged(Editable s) {
				}
			};

			inputsearch.addTextChangedListener(filterTextWatcher);
			listMenu.setNotifyOnChange(true);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(logoutReceiver);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	private void extracted() {
		Collections.sort(listLatLonObject, new Comparator<LatLonObject>() {

			@Override
			public int compare(final LatLonObject lhs, final LatLonObject rhs) {
				return lhs.getTitle().compareToIgnoreCase(rhs.getTitle());
			}

		});
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(DealerLocationCityActivity.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, DealerLocationCityActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
