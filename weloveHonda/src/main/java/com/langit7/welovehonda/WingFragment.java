package com.langit7.welovehonda;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.langit7.welovehonda.adapter.DealerListAdapter;
import com.langit7.welovehonda.object.dealer;
import com.langit7.welovehonda.object.dealer_type;
import com.langit7.welovehonda.util.DialogUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pance on 10/26/17.
 */

public class WingFragment extends Fragment {

    private ListView listView;
    private Button btndetail;
    private ImageView logo;
    private TextView tnodata;
    private TextView tdescription;

    private List<dealer> dealers = new ArrayList<>();
    private String TITLE = "";
    private dealer_type type;

    public WingFragment() {
    }

    public static WingFragment Instantiate(List<dealer> dealers, String TITLE, @Nullable dealer_type type){
        WingFragment wingFragment = new WingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("dealers",(Serializable) dealers);
        bundle.putString("title", TITLE);
        bundle.putSerializable("type",type);
        wingFragment.setArguments(bundle);
        return wingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_regular, container, false);

        Bundle args = getArguments();
        if(args != null){
            TITLE = args.getString("title");
            type = (dealer_type) args.getSerializable("type");
            dealers = (List<dealer>) args.getSerializable("dealers");
        }

        listView = (ListView)view.findViewById(R.id.listview);
        btndetail = (Button)view.findViewById(R.id.btndetail);
        logo = (ImageView)view.findViewById(R.id.logo);
        tnodata = (TextView)view.findViewById(R.id.tnodata);
        tdescription = (TextView)view.findViewById(R.id.description);


        if (type != null) {
            tdescription.setText(type.getBody());

            if (type.getId().equalsIgnoreCase("1")) {
                tnodata.setText("Mohon maaf tidak ditemukan dealer Honda BigWing di kota yang Anda pilih");
            } else if (type.getId().equalsIgnoreCase("2")) {
                tnodata.setText("Mohon maaf tidak ditemukan dealer Honda Wing di kota yang Anda pilih");
            } else {
                tnodata.setText("Mohon maaf tidak ditemukan dealer Honda di kota yang Anda pilih");
                btndetail.getLayoutParams().height=0;
                btndetail.setVisibility(View.INVISIBLE);
            }
        }
        logo.setImageResource(R.drawable.logo_dealer_honda_wing);

        if (dealers.size() == 0) {
            tnodata.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        } else {
            DealerListAdapter adp = new DealerListAdapter(getActivity().getApplicationContext(), R.layout.item_dealer, dealers);
            listView.setAdapter(adp);
            tnodata.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }

        btndetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ctx.showMessageDialog("<b>Dealer Detail</b><br/><br/>Dealer honda adalah dealer yang menjual khusus sparepart dan unit produk honda");

                String desc=type.getDescription();
                String img=type.getImage_description();

                if(desc==null)
                    desc="";

                DialogUtil.createDealerDetailDialog(getActivity(),"<b>"+type.getName()+"</b><br/><br/><small>"+desc+"</small>",img);
            }
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getArguments().remove("title");
        getArguments().remove("type");
        getArguments().remove("dealers");
    }
}
