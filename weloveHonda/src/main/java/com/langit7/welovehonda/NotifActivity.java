package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterNotification;
import com.langit7.welovehonda.object.NotifDetailsObject;
import com.langit7.welovehonda.parse.ParseNotification;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class NotifActivity extends Activity{

	private String url;
	boolean networkSlow;
	SharedPreferences prefData;
	ArrayList<NotifDetailsObject> listDetailObject;

	ListView listViewMenu;
	ListAdapterNotification listMenu;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private String KEY_DATA;
	private ProgressBar pr;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Notification");
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notif_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		pr = (ProgressBar) findViewById(R.id.progressBar);
		listViewMenu = (ListView) findViewById(R.id.listView);

		try {
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void init(){

		url = BaseID.URL_NOTIF;
		KEY_DATA = BaseID.KEY_DATA_NOTIF;

		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.cacheInMemory(true)
		.showImageOnLoading(R.drawable.imagedefault_slidescreen)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();


		prefData = getPreferences(MODE_PRIVATE);
		listDetailObject = new ArrayList<NotifDetailsObject>();
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			listViewMenu.setVisibility(View.INVISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);
		
		//edit
		ParseNotification parseNotification = new ParseNotification();
		try {
			parseNotification.fethingData(insStr);
			listDetailObject = parseNotification.getNotifObjects();
			Log.e("list","list"+listDetailObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){
		
		
		//edit
		listMenu = new ListAdapterNotification(this, R.layout.listnotif, listDetailObject, imageLoader, options);
		listViewMenu.setDividerHeight(1);
		listViewMenu.setDivider(this.getResources().getDrawable(R.drawable.list_separatorabuabu));
		listViewMenu.setAdapter(listMenu);

		listViewMenu.setVisibility(View.VISIBLE);

	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(NotifActivity.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, NotifActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
