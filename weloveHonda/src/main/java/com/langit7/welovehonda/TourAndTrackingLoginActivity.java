package com.langit7.welovehonda;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class TourAndTrackingLoginActivity extends Activity{

	private Button buttonReg;
	private Button buttonSign;
	public String insStr;
	public boolean networkSlow;
	private int w;
	private int h;
	private EditText username;
	private EditText pass;
	protected String user1;
	protected String pass1;
	private ArrayList<LatLonObject> listForumObject;
	private Dialog dialogForgot;
	public String insStrForgot;
	@SuppressWarnings("unused")
	private AsyncTask<?, ?, ?> mTaskForgot = null;
	private Button buttHowTo;
	private Button buttListTrack;
	private TextView forgotPass;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking Page");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tour_tracking_layout_login);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttonSign = (Button)findViewById(R.id.buttonsign);
		buttonReg = (Button)findViewById(R.id.buttonreg);

		username = (EditText)findViewById(R.id.username);
		pass = (EditText)findViewById(R.id.password);
		buttHowTo = (Button)findViewById(R.id.howtotrack);
		buttListTrack = (Button)findViewById(R.id.listgroup);
		buttListTrack.setBackgroundResource(R.drawable.button_listgroupf480);
		forgotPass = (TextView)findViewById(R.id.text_forgot);

		DisplayMetrics displaymetrics = new DisplayMetrics();	
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);	

		w = displaymetrics.widthPixels;
		h = displaymetrics.heightPixels;

		buttonReg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				RegisterActivity.show(TourAndTrackingLoginActivity.this);
			}
		});

		buttHowTo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				TourAndTrackingHowActivity.show(TourAndTrackingLoginActivity.this);
			}
		});
		
		forgotPass.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogForgot();
			}
		});

		buttonSign.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					user1= username.getText().toString();
					pass1 = pass.getText().toString();

					if(user1.equals("") || pass1.equals("")){
						Toast.makeText(TourAndTrackingLoginActivity.this, "Please complete the data first", Toast.LENGTH_SHORT).show();
					}else{
						new OpenDataReg().execute();
						username.setText("");
						pass.setText("");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});



	}

	class OpenDataReg extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(TourAndTrackingLoginActivity.this, "Loading",
					"Login Process", true);
			progress.setCancelable(true);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			insertProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				if (insStr.equals("[]")){
					Toast.makeText(TourAndTrackingLoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
				}else{
					respondcek();
				}
			}
			progress.dismiss();
		}
	}

	public void insertProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getData(BaseID.URL_LOGIN);
			if (is == null) {
				networkSlow = true;
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
		nameValuePairs.add(new BasicNameValuePair("username", user1));
		nameValuePairs.add(new BasicNameValuePair("password", pass1));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException,JSONException{
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParseLatLon parsingForum = new ParseLatLon();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
			if(!listForumObject.isEmpty()){
				Credential.saveCredentialData(TourAndTrackingLoginActivity.this, BaseID.KEY_DATA_EMAIL,listForumObject.get(0).getEmail());
				Credential.saveCredentialData(TourAndTrackingLoginActivity.this, BaseID.KEY_DATA_UID,listForumObject.get(0).getUid());
				Credential.saveCredentialData(TourAndTrackingLoginActivity.this, BaseID.KEY_DATA_USERNAME,listForumObject.get(0).getUsername());
				Credential.saveCredentialData(TourAndTrackingLoginActivity.this, BaseID.KEY_DATA_AVA,listForumObject.get(0).getPhoto());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(TourAndTrackingLoginActivity.this, "Please check your network connection", Toast.LENGTH_LONG).show();
	}

	private void respondcek() {
		try {
			TourAndTrackingListActivity.show(TourAndTrackingLoginActivity.this);
			Credential.saveCredentialData(TourAndTrackingLoginActivity.this, BaseID.KEY_DATA_USERNAME, user1);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void showDialogForgot() {
		dialogForgot = new Dialog(this, R.style.Theme_Transparent);
		dialogForgot.setContentView(R.layout.dialog_forgot);
		dialogForgot.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogForgot.findViewById(R.id.area_dialog);

		final EditText editUsername = (EditText) dialogForgot.findViewById(R.id.edit_username);
		final EditText editEmail = (EditText) dialogForgot.findViewById(R.id.edit_email);

		Button btnPositive = (Button) dialogForgot.findViewById(R.id.btn_forgot);
		btnPositive.setOnClickListener(new OnClickListener() {
			private boolean emailcheck;

			@Override
			public void onClick(View v) {
				String un = editUsername.getText().toString();
				String em = editEmail.getText().toString();

				if ((un.equalsIgnoreCase("")) || (em.equalsIgnoreCase(""))) {
					Toast.makeText(TourAndTrackingLoginActivity.this, "Please complete the data first",Toast.LENGTH_LONG).show();
				}else if(un.contains("\\s") || un.contains(" ")){
					Toast.makeText(TourAndTrackingLoginActivity.this, "No spaces allowed ", Toast.LENGTH_LONG).show();
				}else{
					checkemail(em);
					if(emailcheck==true)
					{
						mTaskForgot = new ForgotProccess(un, em).execute();
					}else{
						Toast.makeText(TourAndTrackingLoginActivity.this, "Please fill the email corectly",Toast.LENGTH_LONG).show();
					}
				}
			}

			private void checkemail(String sTR_email) {
				Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
				Matcher matcher = pattern.matcher(sTR_email);
				emailcheck = matcher.matches();
			}

		});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogForgot.dismiss();
			}
		});

		dialogForgot.show();
	}

	//FORGOT
	class ForgotProccess extends AsyncTask<Void, Void, Void>{

		private String un;
		private String em;

		/**
		 * @param un2
		 * @param em2
		 */
		public ForgotProccess(String un, String em) {
			this.un = un;
			this.em = em;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			fetchForgot(un, em);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (networkSlow) {
				Toast.makeText(TourAndTrackingLoginActivity.this, "You have network problem. Check Your network setting", Toast.LENGTH_LONG).show();
			}else{
				if (insStrForgot.startsWith("{")) {
					JSONObject jobjectMessage;
					try {
						jobjectMessage = new JSONObject(insStrForgot);
						if (jobjectMessage.has("status") && jobjectMessage.has("message")) {
							String statusReg = jobjectMessage.optString("status");
							String messageReg = jobjectMessage.optString("message");
							Toast.makeText(TourAndTrackingLoginActivity.this, messageReg, Toast.LENGTH_SHORT).show();
							if (statusReg.equals("1")) {
								dialogForgot.dismiss();

							}else{

							}
						}
					} catch (JSONException e) {
						Toast.makeText(TourAndTrackingLoginActivity.this, "Please try again later" , Toast.LENGTH_SHORT).show();
						//					+e.getMessage()
					}
				}else {
					Toast.makeText(TourAndTrackingLoginActivity.this, "Internal server error" , Toast.LENGTH_SHORT).show();
				}
			}
			mTaskForgot = null;
		}
	}

	public void fetchForgot(String un, String em) {

		InputStream is;
		try {
			is = postDataForgot(BaseID.URL_FORGOT_PASS, un, em);
			if(is == null){
				networkSlow = true;
			}else{
				parsingForgot(is);
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private InputStream postDataForgot(String url2, String un, String em) {
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("username", un));
		nameValuePairs.add(new BasicNameValuePair("email", em));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2,nameValuePairs);
		return is;
	}

	private void parsingForgot(InputStream is) throws JSONException, IOException {
		insStrForgot = GetStringFromInputStream.parsingInputStreamToString(is);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}


	public static void show(Context context) {
		Intent intent = new Intent(context, TourAndTrackingLoginActivity.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
