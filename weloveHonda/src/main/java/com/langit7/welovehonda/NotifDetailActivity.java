package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.object.NotifDetailsObject;
import com.langit7.welovehonda.parse.ParseNotificationDetails;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

@SuppressLint("NewApi")
public class NotifDetailActivity extends Activity {

	private String title;
	private String content = "";
	private ProgressBar pr;
	private TextView titleText;
	private TextView dateText;
	private String date;
	private String imgView;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private ImageView imageDetail;
	private int w;
	private int h;
	private int hRelative;
	private BroadcastReceiver logoutReceiver;
	private static SharedPreferences mSharedPreferences;
	private Boolean networkSlow = false;
	private String KEY_DATA;
	private String url;
	private String linkUrl;
	private SharedPreferences prefData;
	private ArrayList<NotifDetailsObject> notifDetailsObjects = new ArrayList<NotifDetailsObject>();
	private String code;
	private String sCertDate = "";
	private Boolean isNotFromNotif = false;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Notification Detail");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notif_detail_layout);


		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		w = displaymetrics.widthPixels;
		h = displaymetrics.heightPixels;
		hRelative = h / 4;

		mSharedPreferences = getSharedPreferences(BaseID.PREFERENCE_NAME,
				MODE_PRIVATE);

		titleText = (TextView) findViewById(R.id.title);
		dateText = (TextView) findViewById(R.id.date);
		imageDetail = (ImageView) findViewById(R.id.imagenew);
		imageDetail.getLayoutParams().width = hRelative;
		imageDetail.getLayoutParams().height = hRelative;
		pr = (ProgressBar) findViewById(R.id.progressBar);

		KEY_DATA = BaseID.KEY_DATA_DETAIL_NOTIF + code;
		prefData = getPreferences(MODE_PRIVATE);

		url = BaseID.URL_DETAIL_NOTIF;
		
		isNotFromNotif = getIntent().getBooleanExtra("ISNOTFROMNOTIF", false);
		
		if (isNotFromNotif) {
			title = (String) getIntent().getSerializableExtra("TITLE");
			content = (String) getIntent().getSerializableExtra("CONTENT");
			linkUrl = (String) getIntent().getSerializableExtra("URL");
			imgView = (String) getIntent().getSerializableExtra("IMG");
			date = (String) getIntent().getSerializableExtra("DATE");
			code = (String) getIntent().getSerializableExtra("NID");
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
			options = new DisplayImageOptions.Builder().cacheOnDisc(true)
					.showImageOnLoading(R.drawable.imagedefault_slidescreen)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			
			showView();

		} else {
			String nid = Credential.getCredentialData(getApplicationContext(),
					BaseID.KEY_REG_NID);
			code = nid;
			new OpenData().execute();
			isNotFromNotif = false;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			// init();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean isConnected() {
		return mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, null) != null;
	}

	public void init() {

		if (notifDetailsObjects.size() != 0) {
			title = notifDetailsObjects.get(0).getTitle();
			content = notifDetailsObjects.get(0).getBody();
			date = notifDetailsObjects.get(0).getDate();
			imgView = notifDetailsObjects.get(0).getImg();
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
			options = new DisplayImageOptions.Builder().cacheOnDisc(true)
					.showImageOnLoading(R.drawable.imagedefault_slidescreen)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
		}

	}

	@SuppressWarnings("deprecation")
	private void settingWebViewClient(WebView webview, String body) {

		String contentAwal = "<html><head><style type='text/css'>html,body {;text-align:left;} .rtecenter{ text-align:center; }</style></head><body><p>"
				+ "<body width=100% leftmargin='2' rightmargin='2' topmargin='2' marginwidth='0' marginheight='0'>";

		String contentTengah = body;
		String contentAkhir = "</body></html>";
		String content1 = contentAwal + contentTengah + contentAkhir;

		WebView webViewContent = webview;
		webViewContent.clearView();
		webViewContent.setHorizontalScrollBarEnabled(false);
		webViewContent.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				if (pr.getVisibility() == View.VISIBLE) {
					pr.setVisibility(View.INVISIBLE);
				}
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
				return true;
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
			}

		});
		webViewContent.loadDataWithBaseURL(null, content1, "text/html",
				"utf-8", null);
	}

	private void showView() {

		titleText.setText(title);
		dateText.setText(date);

		imageLoader.displayImage(imgView, imageDetail, options);
		WebView webview = (WebView) findViewById(R.id.webView);
		settingWebViewClient(webview, content);
	}

	@Override
	public void onBackPressed() {
		finish();
		if(isNotFromNotif){
			Intent intent = new Intent(getApplicationContext(),
					NotifActivity.class);
			startActivity(intent);
		}else{
			Intent intent = new Intent(getApplicationContext(),
					DashboardActivity.class);
			startActivity(intent);
		}
	}

	class OpenData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				init();
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	private void parsing(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);
		Credential.saveCredentialData(NotifDetailActivity.this,
				BaseID.KEY_DATA_DETAIL_NOTIF, insStr);

		ParseNotificationDetails parsingCata = new ParseNotificationDetails();
		try {
			notifDetailsObjects = parsingCata.getArrayListRespondObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(url);
			if (is == null) {
				// cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					// Ambil dari persistent
					networkSlow = false;
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				networkSlow = false;
				parsing(is);
			}// end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("xs", "1"));
		nameValuePairs.add(new BasicNameValuePair("nid", code));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(NotifDetailActivity.this,
				"There is No Data",
				Toast.LENGTH_LONG).show();
	}

	public static void show(Context context, String nid, String title,
			String body, String img, String url, String date,
			Boolean isNotFromNotif) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, NotifDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("TITLE", title);
		b.putString("CONTENT", body);
		b.putString("URL", url);
		b.putString("IMG", img);
		b.putString("DATE", date);
		b.putString("NID", nid);
		b.putBoolean("ISNOTFROMNOTIF", isNotFromNotif);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.slide_in_right,
				R.anim.slide_out_left);

	}

}
