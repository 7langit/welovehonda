package com.langit7.welovehonda.object;

public class CataloguDetailMainFeaturesObject {
	
	String name;
	String desc;
	String image;
	
	public CataloguDetailMainFeaturesObject(String name, String desc,
			String image) {
		super();
		this.name = name;
		this.desc = desc;
		this.image = image;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	
	
}
