package com.langit7.welovehonda.object;

import java.util.ArrayList;
import java.util.List;

public class NewsDetailObject {
	
	String image;
	String title;
	String body;
	String date;
	String nid;
	String ket;
	String images;
	String price;
	String tid;
	String link;
	String name;
	String link2;
	String info;
	String uid;
	String admin;
	String password;
	String event;
	String place;
	String lat;
	String lon;
	String gid;
	ArrayList<CataloguDetail2Object> listLatLonObject;
	ArrayList<CatalogueDetail3Object>listPlace;
	private List<String> list_image = new ArrayList<>();

	public NewsDetailObject() {
	}

	public NewsDetailObject(String image, String title, String body,
							String date, String nid, String ket, String images, String price,
							String tid, String link, String name, String link2, String info,
							String uid, String admin, String password, String event,
							String place, String lat, String lon, String gid, ArrayList<CataloguDetail2Object> listLatLonObject, ArrayList<CatalogueDetail3Object>listplace) {
		super();
		this.image = image;
		this.title = title;
		this.body = body;
		this.date = date;
		this.nid = nid;
		this.ket = ket;
		this.images = images;
		this.price = price;
		this.tid = tid;
		this.link = link;
		this.name = name;
		this.link2 = link2;
		this.info = info;
		this.uid = uid;
		this.admin = admin;
		this.password = password;
		this.event = event;
		this.place = place;
		this.lat = lat;
		this.lon = lon;
		this.gid = gid;
		this.listLatLonObject = listLatLonObject;
		this.listPlace = listplace;
//		this.list_image = list_image;
	}

	public List<String> getList_image() {
		return list_image;
	}

	public void setList_image(List<String> list_image) {
		this.list_image = list_image;
	}

	public ArrayList<CatalogueDetail3Object> getListPlace() {
		return listPlace;
	}

	public void setListPlace(ArrayList<CatalogueDetail3Object> listPlace) {
		this.listPlace = listPlace;
	}

	public ArrayList<CataloguDetail2Object> getListLatLonObject() {
		return listLatLonObject;
	}

	public void setListLatLonObject(
			ArrayList<CataloguDetail2Object> listLatLonObject) {
		this.listLatLonObject = listLatLonObject;
	}

	public String getGid() {
		return gid;
	}


	public void setGid(String gid) {
		this.gid = gid;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getKet() {
		return ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink2() {
		return link2;
	}

	public void setLink2(String link2) {
		this.link2 = link2;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}
	
}
