package com.langit7.welovehonda.object;

public class TwitterTimelineObject {
	String id, iu, date, text;
	
	public TwitterTimelineObject(String id, String iu, String date, String text) {
		super();
		this.id			= id;
		this.iu			= iu;
		this.date 		= date;
		this.text 		= text;
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIu() {
		return iu;
	}

	public void setIu(String iu) {
		this.iu = iu;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}