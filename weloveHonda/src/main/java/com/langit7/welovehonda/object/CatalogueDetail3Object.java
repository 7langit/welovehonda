package com.langit7.welovehonda.object;

public class CatalogueDetail3Object {
	
	String icon;
	String motor;
	String description;
	
	public CatalogueDetail3Object(String icon, String motor, String description) {
		super();
		this.icon = icon;
		this.motor = motor;
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
