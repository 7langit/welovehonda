package com.langit7.welovehonda.object;

import java.util.List;

/**
 * Created by Donny Adiwilaga on 4/11/2017.
 */

public class listdata<T>{
    String xs;//": 1491531816,
    List<T> data;//": []
    String description;
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getXs() {
        return xs;
    }

    public void setXs(String xs) {
        this.xs = xs;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
