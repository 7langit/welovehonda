package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class NewsObject1 {

	String xs;
	ArrayList<NewsObject2> listDetailObject;
	
	public NewsObject1(String xs, ArrayList<NewsObject2> listDetailObject) {
		super();
		this.xs = xs;
		this.listDetailObject = listDetailObject;
	}
	
	public String getXs() {
		return xs;
	}
	public void setXs(String xs) {
		this.xs = xs;
	}
	public ArrayList<NewsObject2> getListDetailObject() {
		return listDetailObject;
	}
	public void setListDetailObject(ArrayList<NewsObject2> listDetailObject) {
		this.listDetailObject = listDetailObject;
	}
	
	
	
}
