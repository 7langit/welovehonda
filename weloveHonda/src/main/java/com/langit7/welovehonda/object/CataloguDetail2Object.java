package com.langit7.welovehonda.object;

public class CataloguDetail2Object {
	
	String name;
	String desc;
	
	public CataloguDetail2Object(String name, String desc) {
		super();
		this.name = name;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
