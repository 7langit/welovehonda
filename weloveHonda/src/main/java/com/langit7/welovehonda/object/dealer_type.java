package com.langit7.welovehonda.object;

import java.io.Serializable;

/**
 * Created by Donny Adiwilaga on 5/17/2017.
 */

public class dealer_type implements Serializable {
    String id;//": 1,
    String name;//": "BigWing"
    String image;
    String body;
    String description;//": "deskripsi big wing",
    String image_description;//": ""


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImage_description() {
        return image_description;
    }

    public void setImage_description(String image_description) {
        this.image_description = image_description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
