package com.langit7.welovehonda.object;

public class LatLonObject {

	String nid;
	String title;
	String address;
	String phone;
	String city;
	String lat;
	String lon;
	String distance;
	String tid;
	String linkdetail;
	String detaillink;
	String cid;
	String comment_count;
	String comment;
	String created;
	String desc;
	String fullname;
	String email;
	String photo;
	String name;
	String uid;
	String username;
	String trackid;
	String datetime;
	String gid;
	String status;
	
	
	public LatLonObject(String nid, String title, String address, String phone,
			String city, String lat, String lon, String distance, String tid,
			String linkdetail, String detaillink, String cid,
			String comment_count, String comment, String created, String desc,
			String fullname, String email, String photo, String name,
			String uid, String username, String trackid, String datetime,
			String gid, String status) {
		super();
		this.nid = nid;
		this.title = title;
		this.address = address;
		this.phone = phone;
		this.city = city;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
		this.tid = tid;
		this.linkdetail = linkdetail;
		this.detaillink = detaillink;
		this.cid = cid;
		this.comment_count = comment_count;
		this.comment = comment;
		this.created = created;
		this.desc = desc;
		this.fullname = fullname;
		this.email = email;
		this.photo = photo;
		this.name = name;
		this.uid = uid;
		this.username = username;
		this.trackid = trackid;
		this.datetime = datetime;
		this.gid = gid;
		this.status = status;
	}

	public String getNid() {
		return nid;
	}


	public void setNid(String nid) {
		this.nid = nid;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getLat() {
		return lat;
	}


	public void setLat(String lat) {
		this.lat = lat;
	}


	public String getLon() {
		return lon;
	}


	public void setLon(String lon) {
		this.lon = lon;
	}


	public String getDistance() {
		return distance;
	}


	public void setDistance(String distance) {
		this.distance = distance;
	}


	public String getTid() {
		return tid;
	}


	public void setTid(String tid) {
		this.tid = tid;
	}


	public String getLinkdetail() {
		return linkdetail;
	}


	public void setLinkdetail(String linkdetail) {
		this.linkdetail = linkdetail;
	}


	public String getDetaillink() {
		return detaillink;
	}


	public void setDetaillink(String detaillink) {
		this.detaillink = detaillink;
	}


	public String getCid() {
		return cid;
	}


	public void setCid(String cid) {
		this.cid = cid;
	}


	public String getComment_count() {
		return comment_count;
	}


	public void setComment_count(String comment_count) {
		this.comment_count = comment_count;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getCreated() {
		return created;
	}


	public void setCreated(String created) {
		this.created = created;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public String getFullname() {
		return fullname;
	}


	public void setFullname(String fullname) {
		this.fullname = fullname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUid() {
		return uid;
	}


	public void setUid(String uid) {
		this.uid = uid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getTrackid() {
		return trackid;
	}


	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}


	public String getDatetime() {
		return datetime;
	}


	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}


	public String getGid() {
		return gid;
	}


	public void setGid(String gid) {
		this.gid = gid;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
