package com.langit7.welovehonda.object;

public class NotifDetailsObject {
	
	String nid;
	String title;
	String body;
	String image_url;
	String date;
	String link_detail;
	
	public NotifDetailsObject(String title, String body, String img, String date) {
		super();
		this.title = title;
		this.body = body;
		this.image_url = img;
		this.date = date;
	}
	
	
	public NotifDetailsObject(String nid, String title, String body,
			String image_url, String date, String url) {
		super();
		this.nid = nid;
		this.title = title;
		this.body = body;
		this.image_url = image_url;
		this.date = date;
		this.link_detail = url;
	}



	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getImg() {
		return image_url;
	}
	public void setImg(String img) {
		this.image_url = img;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUrl() {
		return link_detail;
	}

	public void setUrl(String url) {
		this.link_detail = url;
	}

}
