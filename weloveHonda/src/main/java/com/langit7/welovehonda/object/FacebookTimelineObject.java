package com.langit7.welovehonda.object;

public class FacebookTimelineObject {
	
	String text;
	String id;
	String date;
	String image;
	
	public FacebookTimelineObject(String text, String id, String date,
			String image) {
		super();
		this.text = text;
		this.id = id;
		this.date = date;
		this.image = image;
	}

	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	

}
