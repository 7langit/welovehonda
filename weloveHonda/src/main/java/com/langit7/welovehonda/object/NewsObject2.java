package com.langit7.welovehonda.object;

public class NewsObject2 {
	
	String url;
	String title;
	String content;
	String img;
	
	public NewsObject2(String url, String title, String content, String img) {
		super();
		this.url = url;
		this.title = title;
		this.content = content;
		this.img = img;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

}
