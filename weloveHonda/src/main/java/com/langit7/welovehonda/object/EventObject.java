package com.langit7.welovehonda.object;

public class EventObject {
	
	String ongoing;
	String expired;
	
	public EventObject(String ongoing, String expired) {
		super();
		this.ongoing = ongoing;
		this.expired = expired;
	}

	public String getOngoing() {
		return ongoing;
	}

	public void setOngoing(String ongoing) {
		this.ongoing = ongoing;
	}

	public String getExpired() {
		return expired;
	}

	public void setExpired(String expired) {
		this.expired = expired;
	}
	
}
