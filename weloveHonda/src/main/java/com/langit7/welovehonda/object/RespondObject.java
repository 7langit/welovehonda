package com.langit7.welovehonda.object;

public class RespondObject {

	String status;
	String message;
	String trackid;
	
	public RespondObject(String status, String message, String trackid) {
		super();
		this.status = status;
		this.message = message;
		this.trackid = trackid;
	}
	
	public String getTrackid() {
		return trackid;
	}

	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
