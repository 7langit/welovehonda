package com.langit7.welovehonda.object;

public class SubTypeProductCatalogueObject {
	String nid;
	String name;
	String image;
	String link_detail;
	String desc;
	Integer comment_count;
	

	public SubTypeProductCatalogueObject(String nid, String name, String image,
			String link_detail, String desc, Integer comment_count) {
		super();
		this.nid = nid;
		this.name = name;
		this.image = image;
		this.link_detail = link_detail;
		this.desc = desc;
		this.comment_count = comment_count;
	}
	public String getNid() {
		return nid;
	}
	public void setNid(String nid) {
		this.nid = nid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLink_detail() {
		return link_detail;
	}
	public void setLink_detail(String link_detail) {
		this.link_detail = link_detail;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getComment_count() {
		return comment_count;
	}

	public void setComment_count(Integer comment_count) {
		this.comment_count = comment_count;
	}
}
