package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class CatalogueObject {

	String xs;

	ArrayList<CatalogueDetailObject> listDetail2Object;

	public CatalogueObject(String xs,
			ArrayList<CatalogueDetailObject> listDetail2Object) {
		super();
		this.xs = xs;
		this.listDetail2Object = listDetail2Object;
	}

	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs = xs;
	}

	public ArrayList<CatalogueDetailObject> getListDetail2Object() {
		return listDetail2Object;
	}

	public void setListDetail2Object(
			ArrayList<CatalogueDetailObject> listDetail2Object) {
		this.listDetail2Object = listDetail2Object;
	}
	
}
