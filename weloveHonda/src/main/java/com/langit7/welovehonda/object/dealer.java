package com.langit7.welovehonda.object;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Donny Adiwilaga on 4/11/2017.
 */

public class dealer implements Parcelable{
    String id;//": 1,
    String title;//": "Nusantara Sakti",
    nv category;//": {
    String address;//": "Jl. Brigjend Katamso No.5 Slipi",
    nv province;//": {
    nv city;//": {
    //String phone;//": "021-5686666",
    List<dealer_detail> dealer_detail;
    String created_date;//": "07-04-2017 10:30"
    String bigwing_grouping;


    public String getBigwing_grouping() {
        return bigwing_grouping;
    }

    public void setBigwing_grouping(String bigwing_grouping) {
        this.bigwing_grouping = bigwing_grouping;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public nv getCategory() {
        return category;
    }

    public void setCategory(nv category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public nv getProvince() {
        return province;
    }

    public void setProvince(nv province) {
        this.province = province;
    }

    public nv getCity() {
        return city;
    }

    public void setCity(nv city) {
        this.city = city;
    }

    public String getPhone() {

        if(dealer_detail!=null && dealer_detail.size()>0)
            return dealer_detail.get(0).getPhone();
        else
            return "";
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
