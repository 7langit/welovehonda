package com.langit7.welovehonda.object;

/**
 * Created by Donny Adiwilaga on 6/7/2017.
 */

public class authentication {
    String token;//": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im10ZWd1aDgxQGdtYWlsLmNvbSIsInVzZXJfaWQiOjMsImVtYWlsIjoibXRlZ3VoODFAZ21haWwuY29tIiwiZXhwIjoxNDk2ODY4ODY1fQ.ESVjHmTyjBpMlftQIlj2Ps4W2IhZtAlDl4Zv8Dlw-Pg",
    String expired;//": 1496868865,
    user user;//


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public user getUser() {
        return user;
    }

    public void setUser(user user) {
        this.user = user;
    }
}
