package com.langit7.welovehonda.object;

import java.io.Serializable;

/**
 * Created by Donny Adiwilaga on 5/29/2017.
 */

public class user implements Serializable {
    String id;//": 3,
    String first_name;//": "Mochamad",
    String last_name;//": "Teguh",
    String email;//": "mteguh81@gmail.com"
    String username;//": "donny@7langit.com",
    String assword;//": "qweasd",
    String is_active;//": false,
    String date_joined;//": "07-06-2017 16:05",
    user_profile profile_user;//": {
    String top_journey_rank;//": 2,
    String top_check_in_rank;//": 2,
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTop_journey_rank() {
        return top_journey_rank;
    }

    public void setTop_journey_rank(String top_journey_rank) {
        this.top_journey_rank = top_journey_rank;
    }

    public String getTop_check_in_rank() {
        return top_check_in_rank;
    }

    public void setTop_check_in_rank(String top_check_in_rank) {
        this.top_check_in_rank = top_check_in_rank;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAssword() {
        return assword;
    }

    public void setAssword(String assword) {
        this.assword = assword;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getDate_joined() {
        return date_joined;
    }

    public void setDate_joined(String date_joined) {
        this.date_joined = date_joined;
    }

    public user_profile getProfile_user() {
        return profile_user;
    }

    public void setProfile_user(user_profile profile_user) {
        this.profile_user = profile_user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
