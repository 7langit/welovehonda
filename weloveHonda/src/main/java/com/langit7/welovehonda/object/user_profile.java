package com.langit7.welovehonda.object;

import java.io.Serializable;

/**
 * Created by Donny Adiwilaga on 6/7/2017.
 */

public class user_profile implements Serializable{
    String phone;//": "085720452768",
    String city;//": 1,
    String motor_type;//": 1,
    String status;//": "2",
    String desc_status;//": "Non Community",
    String total_point;//": 0,
    String hcid;//": "",
    String created_date;//": "07-06-2017 16:05",
    String updated_date;//": "07-06-2017 16:05"
    String id;//": 1,
    String total_point_hcid;//": 0,
    String total_point_welovehonda;//": 54,
    String medal_type;//": "1",
    String desc_medal_type;//": "Gold",


    public String getMedal_type() {
        return medal_type;
    }

    public void setMedal_type(String medal_type) {
        this.medal_type = medal_type;
    }

    public String getDesc_medal_type() {
        return desc_medal_type;
    }

    public void setDesc_medal_type(String desc_medal_type) {
        this.desc_medal_type = desc_medal_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal_point_hcid() {
        return total_point_hcid;
    }

    public void setTotal_point_hcid(String total_point_hcid) {
        this.total_point_hcid = total_point_hcid;
    }

    public String getTotal_point_welovehonda() {
        return total_point_welovehonda;
    }

    public void setTotal_point_welovehonda(String total_point_welovehonda) {
        this.total_point_welovehonda = total_point_welovehonda;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMotor_type() {
        return motor_type;
    }

    public void setMotor_type(String motor_type) {
        this.motor_type = motor_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc_status() {
        return desc_status;
    }

    public void setDesc_status(String desc_status) {
        this.desc_status = desc_status;
    }

    public String getTotal_point() {
        return total_point;
    }

    public void setTotal_point(String total_point) {
        this.total_point = total_point;
    }

    public String getHcid() {
        return hcid;
    }

    public void setHcid(String hcid) {
        this.hcid = hcid;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }
}
