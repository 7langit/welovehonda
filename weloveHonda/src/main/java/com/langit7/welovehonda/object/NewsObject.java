package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class NewsObject {

	String xs;
	private ArrayList<NewsDetailObject> listDetailObject;
	
	public NewsObject(String xs, ArrayList<NewsDetailObject> listDetailObject) {
		super();
		this.xs = xs;
		this.listDetailObject = listDetailObject;
	}

	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs = xs;
	}

	public ArrayList<NewsDetailObject> getListDetailObject() {
		return listDetailObject;
	}

	public void setListDetailObject(ArrayList<NewsDetailObject> listDetailObject) {
		this.listDetailObject = listDetailObject;
	}
	
}
