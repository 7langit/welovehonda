package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class TypeProductCatalogueObject {
	String type_cat;
	String icon_cat;
	String items_cat;
	
	ArrayList<SubTypeProductCatalogueObject> children = new ArrayList<SubTypeProductCatalogueObject>();
	
	public TypeProductCatalogueObject(String type_cat, String icon_cat,
			String items_cat, ArrayList<SubTypeProductCatalogueObject> children) {
		super();
		this.type_cat = type_cat;
		this.icon_cat = icon_cat;
		this.items_cat = items_cat;
		this.children = children;
	}

	public String getType_cat() {
		return type_cat;
	}

	public void setType_cat(String type_cat) {
		this.type_cat = type_cat;
	}

	public String getIcon_cat() {
		return icon_cat;
	}

	public void setIcon_cat(String icon_cat) {
		this.icon_cat = icon_cat;
	}

	public String getItems_cat() {
		return items_cat;
	}

	public void setItems_cat(String items_cat) {
		this.items_cat = items_cat;
	}

	public ArrayList<SubTypeProductCatalogueObject> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<SubTypeProductCatalogueObject> children) {
		this.children = children;
	}
}
