package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class CatalogueDetailObject {

	String nid;
	String title;
	String sparepart;
	String accessoris;
	String images;
	String imagescover;
	String month;
	String link;

	ArrayList<CataloguDetail2Object> listDetail4Object;
	ArrayList<CatalogueDetail3Object> listDetail3Object;
	ArrayList<CataloguDetailMainFeaturesObject> cataloguDetailMainFeaturesObjects;

	public CatalogueDetailObject(
			String nid,
			String title,
			String sparepart,
			String month,
			String accessoris,
			String images,
			String imagescover,
			String link,
			ArrayList<CataloguDetail2Object> listDetail4Object,
			ArrayList<CatalogueDetail3Object> listDetail3Object,
			ArrayList<CataloguDetailMainFeaturesObject> cataloguDetailMainFeaturesObjects) {
		super();
		this.nid = nid;
		this.title = title;
		this.sparepart = sparepart;
		this.accessoris = accessoris;
		this.month = month;
		this.images = images;
		this.link = link;
		this.imagescover = imagescover;
		this.listDetail4Object = listDetail4Object;
		this.listDetail3Object = listDetail3Object;
		this.cataloguDetailMainFeaturesObjects = cataloguDetailMainFeaturesObjects;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSparepart() {
		return sparepart;
	}

	public void setSparepart(String sparepart) {
		this.sparepart = sparepart;
	}

	public String getAccessoris() {
		return accessoris;
	}

	public void setAccessoris(String accessoris) {
		this.accessoris = accessoris;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getImagescover() {
		return imagescover;
	}

	public void setImagescover(String imagescover) {
		this.imagescover = imagescover;
	}

	public ArrayList<CataloguDetail2Object> getListDetail4Object() {
		return listDetail4Object;
	}

	public void setListDetail4Object(
			ArrayList<CataloguDetail2Object> listDetail4Object) {
		this.listDetail4Object = listDetail4Object;
	}

	public ArrayList<CatalogueDetail3Object> getListDetail3Object() {
		return listDetail3Object;
	}

	public void setListDetail3Object(
			ArrayList<CatalogueDetail3Object> listDetail3Object) {
		this.listDetail3Object = listDetail3Object;
	}

	public ArrayList<CataloguDetailMainFeaturesObject> getCataloguDetailMainFeaturesObjects() {
		return cataloguDetailMainFeaturesObjects;
	}

	public void setCataloguDetailMainFeaturesObjects(
			ArrayList<CataloguDetailMainFeaturesObject> cataloguDetailMainFeaturesObjects) {
		this.cataloguDetailMainFeaturesObjects = cataloguDetailMainFeaturesObjects;
	}

}
