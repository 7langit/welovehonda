package com.langit7.welovehonda.object;

import java.util.ArrayList;

public class LatLonObject2 {

	String uid;
	String trackid;
	String datetime;
	String gid;
	String status;
	private ArrayList<CataloguDetail2Object>listLatlon;

	public LatLonObject2(String uid, String trackid, String datetime,
			String gid, String status, ArrayList<CataloguDetail2Object> listLatlon) {
		super();
		this.uid = uid;
		this.trackid = trackid;
		this.datetime = datetime;
		this.gid = gid;
		this.status = status;
		this.listLatlon = listLatlon;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getTrackid() {
		return trackid;
	}

	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<CataloguDetail2Object> getListLatlon() {
		return listLatlon;
	}

	public void setListLatlon(ArrayList<CataloguDetail2Object> listLatlon) {
		this.listLatlon = listLatlon;
	}




}
