package com.langit7.welovehonda;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.langit7.welovehonda.object.EventObject;
import com.langit7.welovehonda.object.LatLonObject3;
import com.langit7.welovehonda.object.RespondObject;
import com.langit7.welovehonda.parse.ParseEvent;
import com.langit7.welovehonda.parse.ParseRespond;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.langit7.welovehonda.util.LocationUtils;

import eu.janmuller.android.simplecropimage.CropImage;

public class TourAndTrackingLocationActivity extends FragmentActivity implements
LocationListener{

	boolean networkSlow;
	ProgressBar pr;
	public GoogleMap map;
	private double latitude;
	private double longitude;
	private double latitude3;
	private double longitude3;
	public String url;
	MarkerOptions marker2;
	public Marker posMarker;
	private String insStr;
	private ImageButton FriendListButt;
	private ImageButton starttrack;
	private String message;
	ArrayList<RespondObject> listRespondObjects;
	ArrayList<EventObject> listEventObjects;
	private String gid;
	private String uid;
	private Handler handler;
	private int position;
	GPSTracker gps;
	private ImageButton buttpin;
	private Polyline polyLine;
	private LatLng newLatlong;
	private String user;
	private String ava;
	private ImageButton buttMyhistory;
	private boolean TrackState;
	private ArrayList<LatLonObject3> listLatlon;

	private LocationRequest mLocationRequest;
//	private LocationClient mLocationClient;
	SharedPreferences mPrefs;
	SharedPreferences.Editor mEditor;

	boolean mUpdatesRequested = false;
	protected Runnable r;
	private LatLng oldLatlong;
	private PolylineOptions rectOptions = new PolylineOptions();
	protected String gs;
	private CameraPosition cameraPosition;
	private ImageButton buttLeave;
	private int w;
	private Dialog dialogLeave;
	private int h;
	private String track;
	private String message2;
	private Dialog dialogshare1;
	private Dialog dialogshare2;
	private String titleevent;

	public static final String TEMP_PHOTO_FILE_NAME = "welovehonda.jpg";

	public static final int REQUEST_CODE_GALLERY      = 0x1;
	public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
	public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;

	private File mFileTemp;
	private Bitmap photo;
	public String ongoing;
	public String expired;
	private String KEY_DATA_FB;

	public String response;
	private static twitter4j.Twitter twitter;
	private static RequestToken requestToken;
	private static SharedPreferences mSharedPreferences;

	@SuppressWarnings("deprecation")
	private Facebook facebook = new Facebook(BaseID.FACEBOOK_APP_ID);
	private SharedPreferences mPrefs2;
	protected String stat;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Tour and Tracking Location Map");
//		mLocationClient.connect();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.track_location_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		gps = new GPSTracker(TourAndTrackingLocationActivity.this);

		KEY_DATA_FB = BaseID.KEY_DATA_FB;
		mPrefs2 = getSharedPreferences(KEY_DATA_FB, MODE_PRIVATE);
		mSharedPreferences = getSharedPreferences(BaseID.PREFERENCE_NAME, MODE_PRIVATE);

		init();

		Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_JOIN_GROUP_TRACK, "1");
		Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
		titleevent = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_TITLE_EVENT);

		listRespondObjects = new ArrayList<RespondObject>();
		listEventObjects = new ArrayList<EventObject>();

		FriendListButt = (ImageButton)findViewById(R.id.history);
		starttrack = (ImageButton)findViewById(R.id.startrack);
		buttpin = (ImageButton)findViewById(R.id.pin);
		buttMyhistory = (ImageButton)findViewById(R.id.friend);
		buttLeave = (ImageButton)findViewById(R.id.logout);

		gid = Credential.getCredentialData(this, BaseID.KEY_DATA_GID);
		uid = Credential.getCredentialData(this, BaseID.KEY_DATA_UID);
		position = (int) getIntent().getIntExtra("POS", 0);

		Credential.saveCredentialDataInt(this, BaseID.KEY_DATA_POSITION_TRACK, position);
		user = Credential.getCredentialData(this, BaseID.KEY_DATA_USERNAME);
		ava = Credential.getCredentialData(this, BaseID.KEY_DATA_AVA);
		pr = (ProgressBar)findViewById(R.id.progressBar);

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		w = metrics.widthPixels;
		h = metrics.heightPixels;

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
		}
		else {
			mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
		}

		buttLeave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				leaveDialog3();
			}
		});

		buttMyhistory.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String check = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK);
				if(check.equals("")){
					stopPeriodicUpdates();
					TourAndTrackingListUserTrackingActivity.show(TourAndTrackingLocationActivity.this, user, ava, gid, uid);
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				}else{
					leaveDialog2("3");
				}
			}
		});

		starttrack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!TrackState){
					starttrack.setBackgroundResource(R.drawable.bg_button_stoptrack);
					new OpenDataReg().execute();
					TrackState = true;
				}else{
					starttrack.setBackgroundResource(R.drawable.bg_button_starttrack);
					handler.removeCallbacks(r);
					newLatlong = new LatLng(latitude, longitude);
					addMarkerToMap(newLatlong);
					stopPeriodicUpdates();
					new stopTrack().execute();
					Gson g = new Gson();
					gs = g.toJson(listLatlon);
					new PostData().execute();
					TrackState = false;
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
					dialogshare1(titleevent);
				}
			}
		});

		buttpin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String check = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK);
				if(check.equals("")){
					stopPeriodicUpdates();
					TourAndTrackingListMeetingPointActivity.show(TourAndTrackingLocationActivity.this, position);
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				}else{
					leaveDialog2("2");
				}
			}
		});

		FriendListButt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String check = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK);
				if(check.equals("")){
					stopPeriodicUpdates();
					TourAndTrackingListFriendActivity.show(TourAndTrackingLocationActivity.this);
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				}else{
					leaveDialog2("4");
				}
			}
		});

	}

	public void gps(){

		if(gps.canGetLocation()){
			latitude3 = gps.getLatitude();
			longitude3 = gps.getLongitude();
			starttrack.setEnabled(true);
		}else{
			gps.showSettingsAlert();
			starttrack.setEnabled(false);
		}
		//		Log.e("gps", "lat="+latitude3+" lon="+longitude3);
	}

	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		map = mapFrag.getMap();
//		map.getUiSettings().setZoomControlsEnabled(false);
//
//		map.setMyLocationEnabled(true);
//
//		cameraPosition = new CameraPosition.Builder().target(
//				new LatLng(latitude3, longitude3)).zoom(17).build();
//
//		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	private Polyline initializePolyLine(LatLng latlon) {
		rectOptions.add(latlon);
		return map.addPolyline(rectOptions.color(Color.RED));
	}

	public void init(){

		listLatlon = new ArrayList<LatLonObject3>();
		// Create a new global location parameters object
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_SECONDS);
		// Use high accuracy
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the interval ceiling to one minute
		mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
		// Note that location updates are off until the user turns them on
		mUpdatesRequested = false;
		// Open Shared Preferences
		mPrefs = getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);
		// Get an editor
		mEditor = mPrefs.edit();
//		mLocationClient = new LocationClient(this, this, this);
	}

	public void startUpdates() {
		mUpdatesRequested = true;

		if (servicesConnected()) {
			startPeriodicUpdates();
		}
	}

	public void stopUpdates() {
		mUpdatesRequested = false;

		if (servicesConnected()) {
			stopPeriodicUpdates();
		}
	}

	private void startPeriodicUpdates() {
//		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	private void stopPeriodicUpdates() {
//		mLocationClient.removeLocationUpdates(this);
	}

	@Override
	public void onResume() {
		try {
			new CekEvent().execute();
			gps();
			initilizeMap();	
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onResume();
	}

	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode =
				GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(LocationUtils.APPTAG, getString(R.string.play_services_available));

			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			return false;
		}
	}

	private void addMarkerToMap(LatLng latLng) {
		posMarker = map.addMarker(new MarkerOptions().position(latLng));
	}

	class leaveGroup extends AsyncTask<Void, Void, Void>{

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progress = ProgressDialog.show(TourAndTrackingLocationActivity.this, "Loading",
					"Leave Group", true);
			progress.setCancelable(true);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			cekLoginProcess2();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			respondcek2();
			progress.dismiss();
			super.onPostExecute(result);
		}

	}
	class OpenDataReg extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(TourAndTrackingLocationActivity.this, "Loading",
					"Start Track", true);
			progress.setCancelable(true);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			cekLoginProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			respondcek();
			progress.dismiss();
		}
	}

	class stopTrack extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			stopProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
			} else {
			}
		}

	}

	public void respondcek(){

		try {
			if(!listRespondObjects.isEmpty()){
				message = listRespondObjects.get(0).getTrackid();

				if(message.equals("")){
					Toast.makeText(TourAndTrackingLocationActivity.this, "Track failed, please try again", Toast.LENGTH_LONG).show();
					starttrack.setBackgroundResource(R.drawable.bg_button_starttrack);
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
					TrackState = false;
				}else{
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "1");
					oldLatlong = new LatLng(latitude3, longitude3);
					addMarkerToMap(oldLatlong);
					polyLine = initializePolyLine(oldLatlong);
					startPeriodicUpdates();
					changeLocation();
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_TRACKID, message);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void respondcek2(){
		try {
			message2 = listRespondObjects.get(0).getStatus();
			if(message2.equals("1")){
				TourAndTrackingListActivity.show(TourAndTrackingLocationActivity.this);
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_JOIN_GROUP_TRACK, "");
				Toast.makeText(TourAndTrackingLocationActivity.this, "Leave group success", Toast.LENGTH_LONG).show();
				finish();
			}else{
				Toast.makeText(TourAndTrackingLocationActivity.this, "Leave group failed, please try again", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void stopProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getDataLog2(BaseID.URL_STOP_TRACK);
			if (is == null) {
				networkSlow = true;
			} else {
				parsingCekLogin(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cekLoginProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getDataLog(BaseID.URL_START_TRACK);
			if (is == null) {
				networkSlow = true;
			} else {
				parsingCekLogin(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cekLoginProcess2() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getDataLog(BaseID.URL_LEAVE_GROUP);
			if (is == null) {
				networkSlow = true;
			} else {
				parsingCekLogin(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cekLoginProcess3() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getDataLog3("http://ahm.cms.langit7.com/rest/group-tracking/get_group_status.json");
			if (is == null) {
				networkSlow = true;
			} else {
				parsingCekEvent(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private InputStream getDataLog(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("gid", gid));
		nameValuePairs.add(new BasicNameValuePair("uid", uid));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private InputStream getDataLog2(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("trackid", track));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private InputStream getDataLog3(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("gid", gid));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsingCekLogin(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParseRespond parseRespond = new ParseRespond();
		try {
			listRespondObjects = parseRespond.getArrayListRespondObject(insStr); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parsingCekEvent(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParseEvent parseEvent = new ParseEvent();
		try {
			listEventObjects = parseEvent.getArrayListRespondObject(insStr); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class CekEvent extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			starttrack.setEnabled(false);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			cekLoginProcess3();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			ongoing = listEventObjects.get(0).getOngoing();
			expired = listEventObjects.get(0).getExpired();

			if(ongoing.equals("1") && expired.equals("0")){
				starttrack.setEnabled(true);
				Toast.makeText(TourAndTrackingLocationActivity.this, "Event has started", Toast.LENGTH_LONG).show();
			}else if (ongoing.equals("0") && expired.equals("1")){
				TourAndTrackingListActivity.show(TourAndTrackingLocationActivity.this);
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_JOIN_GROUP_TRACK, "");
				Toast.makeText(TourAndTrackingLocationActivity.this, "Event has ended", Toast.LENGTH_LONG).show();
			}else if (ongoing.equals("0") && expired.equals("0")){
				starttrack.setEnabled(false);
				Toast.makeText(TourAndTrackingLocationActivity.this, "Event hasn't started", Toast.LENGTH_LONG).show();
			}else if (ongoing.equals("1") && expired.equals("1")){
				TourAndTrackingListActivity.show(TourAndTrackingLocationActivity.this);
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_JOIN_GROUP_TRACK, "");
				Toast.makeText(TourAndTrackingLocationActivity.this, "Event has ended", Toast.LENGTH_LONG).show();
			}
			super.onPostExecute(result);
		}

	}

	public void changeLocation(){
		handler = new Handler();
		r = new Runnable() {

			@Override
			public void run() {
				newLatlong = new LatLng(latitude, longitude);

				initializePolyLine(newLatlong);
				updatePolyLine(newLatlong);

				Gson g = new Gson();
				gs = g.toJson(listLatlon);
				Log.e("gs", "gs"+gs);
				new PostData().execute();
				Toast.makeText(TourAndTrackingLocationActivity.this, "track in progress", Toast.LENGTH_SHORT).show();
				handler.postDelayed(this, 30000);
			}
		};
		handler.postDelayed(r, 1000);
	}

	public void PostData(){

		InputStream inputStream = null;
		String result = "";
		try {
			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();
			// 2. make POST request to the given URL
			HttpPost httpPost = new HttpPost(BaseID.URL_TRACKING);
			String json = "";

			track = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_TRACKID);
			// 3. build jsonObject
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("latlon", new JSONArray(gs));
			jsonObject.put("trackid", track);

			// 4. convert JSONObject to JSON to String
			json = jsonObject.toString().replaceAll("\\\\", "");

			// 5. set json to StringEntity
			StringEntity se = new StringEntity(json);
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			// 6. set httpPost Entity
			httpPost.setEntity(se);

			// 7. Set some headers to inform server about the type of the content   
			httpPost.setHeader("Content-type", "application/json");

			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);

			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// 10. convert inputstream to string
			if(inputStream != null){
				result = convertInputStreamToString(inputStream);
				Log.e("result","result"+result);
			}else{
				result = "Did not work!";
			}

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

	}

	private void updatePolyLine(LatLng latLng) {
		List<LatLng> points = polyLine.getPoints();
		points.add(latLng);
		polyLine.setPoints(points);
	}  

	public void addListLatlong(double lat, double lot){
		LatLonObject3 poin = new LatLonObject3(String.valueOf(lat), String.valueOf(lot));
		listLatlon.add(poin);
	}

	class PostData extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			PostData();
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	}

//	@Override
//	public void onConnectionFailed(ConnectionResult arg0) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void onConnected(Bundle arg0) {
//		// TODO Auto-generated method stub
//		if (mUpdatesRequested) {
//			startPeriodicUpdates();
//		}
//	}
//
//	@Override
//	public void onDisconnected() {
//		// TODO Auto-generated method stub
//	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		latitude = location.getLatitude();
		longitude = location.getLongitude();

		addListLatlong(latitude, longitude);
	}

	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	} 

	//	private void dialogNoNetwork() {
	//		// TODO Auto-generated method stub
	//		networkSlow = false;
	//		Toast.makeText(TourAndTrackingLocationActivity.this, "Please try again in a few minutes", Toast.LENGTH_LONG).show();
	//	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		String check = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK);
		if(check.equals("")){
			DashboardActivity.show(TourAndTrackingLocationActivity.this);
			stopPeriodicUpdates();
			Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
			finish();
		}else{
			leaveDialog("");
		}
	}

	private void leaveDialog(final String text) {
		dialogLeave = new Dialog(TourAndTrackingLocationActivity.this, R.style.Theme_Transparent);
		dialogLeave.setContentView(R.layout.dialog_text_and_2button);
		dialogLeave.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogLeave.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogLeave.findViewById(R.id.dialog_message);
		message.setText("You are currently on tracking. Are you sure want to stop tracking and go back to previous menu?");
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogLeave.findViewById(R.id.text_positive);
		textPositive.setText("YES");

		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
				handler.removeCallbacks(r);
				stopPeriodicUpdates();
				new stopTrack().execute();
				Gson g = new Gson();
				gs = g.toJson(listLatlon);
				new PostData().execute();
				DashboardActivity.show(TourAndTrackingLocationActivity.this);
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
			}

		});

		TextView textNegative = (TextView) dialogLeave.findViewById(R.id.text_negative);
		textNegative.setText("NO");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}
		});

		dialogLeave.show();
	}

	private void leaveDialog2(final String code) {
		dialogLeave = new Dialog(TourAndTrackingLocationActivity.this, R.style.Theme_Transparent);
		dialogLeave.setContentView(R.layout.dialog_text_and_2button);
		dialogLeave.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogLeave.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogLeave.findViewById(R.id.dialog_message);
		message.setText("You are currently on tracking. Are you sure want to stop tracking and go back to previous menu?");
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogLeave.findViewById(R.id.text_positive);
		textPositive.setText("YES");

		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();

				handler.removeCallbacks(r);
				stopPeriodicUpdates();
				new stopTrack().execute();
				Gson g = new Gson();
				gs = g.toJson(listLatlon);
				new PostData().execute();

				if(code.equals("1")){
					TourAndTrackingHowActivity.show(TourAndTrackingLocationActivity.this);
				}else if(code.equals("2")){
					TourAndTrackingListMeetingPointActivity.show(TourAndTrackingLocationActivity.this, position);
				}else if(code.equals("3")){
					TourAndTrackingListUserTrackingActivity.show(TourAndTrackingLocationActivity.this, user, ava, gid, uid);
				}else if(code.equals("4")){
					TourAndTrackingListFriendActivity.show(TourAndTrackingLocationActivity.this);
				}else if(code.equals("5")){
					stopPeriodicUpdates();
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_JOIN_GROUP_TRACK, "");
					new leaveGroup().execute();
					finish();
				}
				Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
			}
		});

		TextView textNegative = (TextView) dialogLeave.findViewById(R.id.text_negative);
		textNegative.setText("NO");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}
		});

		dialogLeave.show();
	}

	private void leaveDialog3() {
		dialogLeave = new Dialog(TourAndTrackingLocationActivity.this, R.style.Theme_Transparent);
		dialogLeave.setContentView(R.layout.dialog_text_and_2button);
		dialogLeave.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogLeave.findViewById(R.id.area_dialog);
		TextView message = (TextView) dialogLeave.findViewById(R.id.dialog_message);
		message.setText("Are you sure want to leave this group?");
		message.setVisibility(View.VISIBLE);

		TextView textPositive = (TextView) dialogLeave.findViewById(R.id.text_positive);
		textPositive.setText("YES");

		textPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
				String check = Credential.getCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK);
				if(check.equals("")){
					stopPeriodicUpdates();
					new leaveGroup().execute();
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_START_TRACK, "");
				}else{
					leaveDialog2("5");
				}
			}
		});

		TextView textNegative = (TextView) dialogLeave.findViewById(R.id.text_negative);
		textNegative.setText("NO");
		textNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogLeave.dismiss();
			}
		});

		dialogLeave.show();
	}

	private void dialogshare1(String title) {
		dialogshare1 = new Dialog(TourAndTrackingLocationActivity.this, R.style.Theme_Transparent);
		dialogshare1.setContentView(R.layout.dialog_field_and_2button4);
		dialogshare1.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogshare1.findViewById(R.id.area_dialog);

		TextView message = (TextView) dialogshare1.findViewById(R.id.tweet);
		message.setText("Congratulations! \r\nYou just riding with "+titleevent);
		message.setVisibility(View.VISIBLE);

		ImageButton twitterButt = (ImageButton) dialogshare1.findViewById(R.id.twitter);
		twitterButt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogshare1.dismiss();
				dialogshare2("1");
			}
		});

		ImageButton fbButt = (ImageButton) dialogshare1.findViewById(R.id.facebook);
		fbButt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogshare1.dismiss();
				dialogshare2("2");
			}});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogshare1.dismiss();
			}
		});

		dialogshare1.show();
	}

	private void dialogshare2(final String code) {
		dialogshare2 = new Dialog(TourAndTrackingLocationActivity.this, R.style.Theme_Transparent);
		dialogshare2.setContentView(R.layout.dialog_field_and_2button3);
		dialogshare2.getWindow().setLayout(w, h);

		RelativeLayout areaAdapter = (RelativeLayout) dialogshare2.findViewById(R.id.area_dialog);
		final EditText tweet = (EditText)dialogshare2.findViewById(R.id.tweet);
		tweet.setPadding(10, 10, 10, 10);
		tweet.setLines(3);

		ImageButton shareButton = (ImageButton) dialogshare2.findViewById(R.id.share);
		ImageButton galleryButton = (ImageButton) dialogshare2.findViewById(R.id.gallery);
		ImageButton cameraButton = (ImageButton) dialogshare2.findViewById(R.id.camera);

		galleryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				openGallery();
			}
		});

		cameraButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				takePicture();
			}
		});

		shareButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogshare2.dismiss();
				stat = tweet.getText().toString();
				if (stat.length()<141) {
					if(code.equals("1")){
						if (isConnected()) {
							new UpdateTwitterStatusImage().execute(stat);
						} else {
							new AskOAuth().execute();
						}
					}else{
						login(stat);
					}
				}else {
					Toast.makeText(getApplicationContext(), "Status Is Over 140 Characters", Toast.LENGTH_SHORT).show();
				}
			}
		});

		areaAdapter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogshare2.dismiss();
			}
		});

		dialogshare2.show();
	}

	private void takePicture() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {
			Uri mImageCaptureUri = null;
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				mImageCaptureUri = Uri.fromFile(mFileTemp);
			}
			else {
				mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
			}	
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
		} catch (ActivityNotFoundException e) {

		}
	}

	private void openGallery() {

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
	}

	private void startCropImage() {

		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);
		intent.putExtra(CropImage.OUTPUT_X, 300);
		intent.putExtra(CropImage.OUTPUT_Y, 300);

		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}


	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		facebook.authorizeCallback(requestCode, resultCode, data);
		if (resultCode != RESULT_OK) {

			return;
		}

		switch (requestCode) {
		case REQUEST_CODE_GALLERY:

			try {

				InputStream inputStream = getContentResolver().openInputStream(data.getData());
				FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
				copyStream(inputStream, fileOutputStream);
				fileOutputStream.close();
				inputStream.close();

				startCropImage();

			} catch (Exception e) {

			}

			break;
		case REQUEST_CODE_TAKE_PICTURE:

			startCropImage();
			break;
		case REQUEST_CODE_CROP_IMAGE:

			String path = data.getStringExtra(CropImage.IMAGE_PATH);
			if (path == null) {

				return;
			}

			photo = BitmapFactory.decodeFile(mFileTemp.getPath());
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@SuppressWarnings("deprecation")
	public void login(final String message) {

		String access_token = mPrefs2.getString(BaseID.FACEBOOK_ATOK, null);
		long expires = mPrefs2.getLong(BaseID.FACEBOOK_AEX, 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			new ShareStatus(message).execute();
			Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_LOGIN_FB, "1");
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "publish_stream", "user_status", "email", "read_stream", "user_interests", "publish_actions", "publish_actions"  },
					new DialogListener() {

				@Override
				public void onCancel() {
				}
				@Override
				public void onComplete(Bundle values) {
					SharedPreferences.Editor editor = mPrefs2.edit();
					editor.putString(BaseID.FACEBOOK_ATOK, facebook.getAccessToken());
					editor.putLong(BaseID.FACEBOOK_AEX, facebook.getAccessExpires());
					editor.commit();
					new ShareStatus(message).execute();
					Credential.saveCredentialData(TourAndTrackingLocationActivity.this, BaseID.KEY_DATA_LOGIN_FB, "1");
				}

				@Override
				public void onError(DialogError error) {
				}

				@Override
				public void onFacebookError(FacebookError fberror) {
				}
			});
		}
	}

	class ShareStatus extends AsyncTask<Void, Void, Void>{

		private String message;
		private String response;

		public ShareStatus(String message) {
			this.message = message;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {

			byte[] data = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			photo.compress(Bitmap.CompressFormat.JPEG, 90, baos);
			data = baos.toByteArray();

			Bundle parameters = new Bundle();
			parameters.putString("message", message);
			parameters.putByteArray("source", data);
			try {
				facebook.request("me");
				response = facebook.request("me/photos", parameters,"POST");
				System.out.println("response="+response);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			JSONObject jobjectMessage;
			try {
				jobjectMessage = new JSONObject(response);
				if (jobjectMessage.has("id")) {
					Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
				}else if (jobjectMessage.has("error")) {
					String strMessage = jobjectMessage.getJSONObject("error").optString("message");
					if (strMessage.equals("(#200) You do not have sufficient to permissions to perform this action")) {
						Toast.makeText(TourAndTrackingLocationActivity.this, "Please wait..", Toast.LENGTH_LONG).show();

					}else if (strMessage.contains("An active access token must be used to query information about the current user.")) {
						Toast.makeText(TourAndTrackingLocationActivity.this, "Please wait..", Toast.LENGTH_LONG).show();

					}else if(strMessage.contains("Error validating access token: " +
							"The session has been invalidated because the user has changed the password.")) {
						Toast.makeText(TourAndTrackingLocationActivity.this, 
								"Please sign in through facebook.com because the password has been changed.", Toast.LENGTH_LONG).show();

					}else {
						Toast.makeText(TourAndTrackingLocationActivity.this, strMessage, Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(TourAndTrackingLocationActivity.this,"Please try again later" , Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				//				e.printStackTrace();
				Toast.makeText(TourAndTrackingLocationActivity.this,"Please try again, "+e.getMessage() , Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Uri uri = intent.getData();

		if (uri != null && uri.toString().startsWith(BaseID.CALLBACK_URL2)) {
			String verifier = uri
					.getQueryParameter(BaseID.IEXTRA_OAUTH_VERIFIER);
			//			Log.e("verifrequestTokenier", "requestToken= "+requestToken.getAuthenticationURL());
			new GetAccessToken().execute(verifier);
		} else {
			//			Log.e("Error", "welovehonda has failed authorize Twitter=");
		}
	}

	private class GetAccessToken extends AsyncTask<String, Void, String> {
		private AccessToken accessToken;
		private String response;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {
			String verifierget = urls[0];
			try {
				accessToken = twitter.getOAuthAccessToken(
						requestToken, verifierget);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			Editor e = mSharedPreferences.edit();
			e.putString(BaseID.PREF_KEY_TOKEN, accessToken.getToken()); 
			e.putString(BaseID.PREF_KEY_SECRET, accessToken.getTokenSecret()); 
			e.commit(); 
			new UpdateTwitterStatusImage().execute(stat);
		}
	}

	/**
	 * check if the account is authorized
	 * @return
	 */
	private boolean isConnected() {
		return mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, null) != null;
	}

	private class AskOAuth extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {

			ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
			configurationBuilder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
			configurationBuilder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);
			twitter4j.conf.Configuration configuration = configurationBuilder.build();
			twitter = new TwitterFactory(configuration).getInstance();
			try {
				requestToken = twitter.getOAuthRequestToken(BaseID.CALLBACK_URL2);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				Toast.makeText(getApplicationContext(), "Please authorize this app!", Toast.LENGTH_LONG).show();
				startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse(requestToken.getAuthenticationURL())));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	class UpdateTwitterStatusImage extends AsyncTask<String, String, String> {

		ProgressDialog pDialog;
		private String response;
		//		private String responseToast;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(TourAndTrackingLocationActivity.this);
			pDialog.setMessage("Updating to twitter...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			Log.d("Tweet Text", "> " + args[0]);
			String status = args[0];
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
			builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);

			String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
			String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

			AccessToken accessToken = new AccessToken(access_token, access_token_secret);
			twitter4j.Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

			byte[] data = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			photo.compress(Bitmap.CompressFormat.JPEG, 90, baos);
			data = baos.toByteArray();
			InputStream is = new ByteArrayInputStream(data);

			try {
				StatusUpdate status2 = new StatusUpdate(status);
				status2.setMedia(status, is);// 
				twitter.updateStatus(status2);
				//				if (!(""+status2.getStatus()).equals("")) {
				//					responseToast = "Tweet success";
				//				}else{
				//					responseToast = "Failed to Tweet";
				//				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("Showing home timeline.");
			return response;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			//			Toast.makeText(TourAndTrackingLocationActivity.this, responseToast, Toast.LENGTH_SHORT).show();
		}
	}
	

	public static void show(Context context, int pos) {
		Intent intent = new Intent(context, TourAndTrackingLocationActivity.class);
		Bundle b = new Bundle();
		b.putInt("POS", pos);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
