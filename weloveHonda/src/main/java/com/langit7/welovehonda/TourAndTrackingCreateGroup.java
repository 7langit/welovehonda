package com.langit7.welovehonda;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.langit7.welovehonda.object.RespondObject;
import com.langit7.welovehonda.parse.ParseRespond;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class TourAndTrackingCreateGroup extends Activity{
	
	private EditText username;
	private EditText email;
	private EditText groupname;
	private EditText groupdesc;
	private Button buttSubmit;
	public boolean networkSlow;
	public String insStr;
	private String user1;
	private String email1;
	private String groupname1;
	private String groupdesc1;
	private ArrayList<RespondObject> listForumObject;
	private String message;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.createactivity_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====
		
		username = (EditText)findViewById(R.id.username);
		email = (EditText)findViewById(R.id.email);
		groupname = (EditText)findViewById(R.id.groupname);
		groupdesc = (EditText)findViewById(R.id.groupdesc);
		buttSubmit = (Button)findViewById(R.id.buttonreg);
		listForumObject = new ArrayList<RespondObject>();
		
		buttSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				user1 = username.getText().toString();
				email1 = email.getText().toString();
				groupname1 = groupname.getText().toString();
				groupdesc1 = groupdesc.getText().toString();
				
				if (groupdesc.equals("") || groupname.equals("")){
					Toast.makeText(TourAndTrackingCreateGroup.this, "Field masih ada yang kosong", Toast.LENGTH_LONG).show();
				}else if(user1.equals("") || user1.contains(" ")){
					Toast.makeText(TourAndTrackingCreateGroup.this, "Kolom username tidak boleh kosong dan mengandung spasi", Toast.LENGTH_SHORT).show();
				}else if(isEmailValid(email1)==false){
					Toast.makeText(TourAndTrackingCreateGroup.this, "Format email masih salah", Toast.LENGTH_SHORT).show();	
				}else{
					new OpenDataReg().execute();	
				}
			}
		});
	}
	
	class OpenDataReg extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(TourAndTrackingCreateGroup.this, "Loading",
					"Sent Process", true);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			insertProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				if (insStr.equals("[]")){
					Toast.makeText(TourAndTrackingCreateGroup.this, "Failed", Toast.LENGTH_SHORT).show();
				}else{
					respondcek();
				}
			}
			progress.dismiss();
		}
	}

	public void insertProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getData(BaseID.URL_CREATE_GROUP);
			if (is == null) {
				networkSlow = true;
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
		nameValuePairs.add(new BasicNameValuePair("username", user1));
		nameValuePairs.add(new BasicNameValuePair("email", email1));
		nameValuePairs.add(new BasicNameValuePair("group_name", groupname1));
		nameValuePairs.add(new BasicNameValuePair("group_desc", groupdesc1));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException,JSONException{
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParseRespond parsingForum = new ParseRespond();
		try {
			listForumObject = parsingForum.getArrayListRespondObject(insStr);
			if(!listForumObject.isEmpty()){
				message = listForumObject.get(0).getStatus();
				Log.d("message", "message"+message);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(TourAndTrackingCreateGroup.this, "Please check your network connection", Toast.LENGTH_LONG).show();
	}

	private void respondcek() {
		try {
			if(message.equals("1")){
				Toast.makeText(TourAndTrackingCreateGroup.this, "Email has been sent", Toast.LENGTH_LONG).show();
				finish();
			}else{
				Toast.makeText(TourAndTrackingCreateGroup.this, "Failed to send email", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}
	
	public static void show(Context context) {
		Intent intent = new Intent(context, TourAndTrackingCreateGroup.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
