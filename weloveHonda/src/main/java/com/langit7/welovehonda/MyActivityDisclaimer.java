package com.langit7.welovehonda;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MyActivityDisclaimer extends Activity{

	private TextView discliamer;
	private Button buttForum;
	private Button buttActivity;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - My Activity Forum Disclaimer Page");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_activity_disclaimer_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====
		
		buttActivity = (Button)findViewById(R.id.activity);
		buttForum = (Button)findViewById(R.id.forum);
		buttForum.setBackgroundResource(R.drawable.button_forumactivityf);

		buttActivity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				MyActivityEvent.show(MyActivityDisclaimer.this);
			}
		});

		discliamer = (TextView)findViewById(R.id.disc);
		discliamer.setText("Terms of Use \r\n\r\nDisclaimer & Peraturan Forum Welovehonda.\r\n\r\nPeraturan dan Disclaimer berikut di bawah ini berlaku pada dan mengatur seluruh forum diskusi yang terdapat di WELOVEHONDA. Mohon dibaca dengan teliti sebelum Anda mendaftarkan diri dan berpartisipasi pada forum di WELOVEHONDA. Dengan mengakses bergabung di WELOVEHONDA maka Anda menyatakan bahwa Anda setuju untuk mematuhi peraturan-peraturan di bawah ini: \r\n\r\n�	Anggota situs dan pengunjung bertanggung jawab secara pribadi dan penuh atas konten/materi yang ditulis dan diterbitkan oleh mereka, dan Anda setuju untuk membebaskan serta tidak membebani forum ini beserta para pengelola termasuk administrator, moderator dan pemiliknya atas segala klaim yang terjadi yang disebabkan oleh pesan yang Anda terbitkan di forum. WELOVEHONDA berhak untuk menerbitkan segala informasi yang kami miliki tentang diri Anda pada saat terjadi keluhan dan permasalahan hukum yang disebabkan oleh pesan apapun yang Anda terbitkan. Harap diperhatikan bahwa seluruh alamat IP akan direkam untuk semua pesan yang diterbitkan.\r\n\r\n�	WELOVEHONDA tidak dapat dituntut untuk segala pernyataan, kekeliruan, ketidak tepatan atau kekurangan pada segala konten yang dikirim oleh anggota situs atau pengunjung pada forum-forum di WELOVEHONDA. WELOVEHONDA tidak bertanggung jawab atas ketepatan isi informasi dari peserta forum manapun, dan tidak akan mempunyai tanggung jawab hukum untuk hasil diskusi di forum.\r\n\r\n�	Apabila Anda berpartisipasi pada forum ini, Anda menjamin bahwa Anda tidak akan: \r\n\r\n�	Merusak nama baik produk dan brand HONDA \r\n\r\n�	Merusak nama baik, mengancam, melecehkan atau menghina orang lain \r\n\r\n�	Mengeluarkan pernyataan yang berbau SARA (Suku, Agama, Ras) \r\n\r\n�	Menyarankan tindakan melanggar hukum atau berdiskusi yang mengarahkan pada tindakan melanggar hukum \r\n\r\n�	Menerbitkan dan menyebarkan materi yang melanggar hak cipta pihak ketiga atau melanggar hukum \r\n\r\n�	Menerbitkan atau menyebarkan materi dan bahasa yang bersifat pornografi, vulgar dan tidak etis. \r\n\r\n�	Anda wajib memastikan bahwa setiap materi yang Anda terbitkan di WELOVEHONDA baik berupa tulisan, gambar maupun materi multimedia lainnya tidak melanggar hak cipta, paten, merek atau hak pribadi dan kepemilikan intelektual lainnya dari pihak ketiga, dan diterbitkan hanya dengan izin daripada pihak ketiga tersebut. \r\n\r\n�	WELOVEHONDA berhak untuk menghapus pesan yang dianggap tidak sesuai dan melanggar konteks. Apabila Anda menemukan ada pesan yang tidak sesuai dan melanggar, harap hubungi Administrator situs. \r\n\r\nApabila Anda tidak mampu memenuhi peraturan-peraturan yang dicantumkan diatas, WELOVEHONDA berhak untuk melakukan tindakan lebih lanjut. Bagi para pihak yang melanggar peraturan-peraturan ini dapat dihentikan keanggotaannya atau aksesnya tanpa peringatan terlebih dahulu.\r\n\r\nPeraturan-peraturan yang disebut di atas dapat dimodifikasi dari waktu ke waktu oleh WELOVEHONDA tanpa pemberitahuan.");
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context) {
		Intent intent = new Intent(context, MyActivityDisclaimer.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
