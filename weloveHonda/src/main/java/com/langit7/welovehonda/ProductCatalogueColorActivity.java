package com.langit7.welovehonda;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.langit7.welovehonda.adapter.ListAdapterCatalogue2;
import com.langit7.welovehonda.object.CataloguDetail2Object;
import com.langit7.welovehonda.object.CatalogueDetail3Object;
import com.langit7.welovehonda.object.CatalogueDetailObject;
import com.langit7.welovehonda.object.CatalogueObject;
import com.langit7.welovehonda.parse.ParseCatalogue;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.TouchImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

@SuppressWarnings("deprecation")
public class ProductCatalogueColorActivity extends Activity {

	Gallery picGallery;
	private Button buttonSpesification;
	private Button buttonMainFeatures;
	ViewPager pager;
	View.OnTouchListener gestureListener;
	private DisplayImageOptions options;
	private DisplayImageOptions options2;
	private ImageLoader imageLoader = ImageLoader.getInstance();
	String insStr;
	int pos;

	ListView listViewMenu;
	ListAdapterCatalogue2 listMenu;

	ArrayList<CatalogueObject> listCataObject;
	ArrayList<CatalogueDetailObject> listCataloObject;
	ArrayList<CataloguDetail2Object> listMainObject;
	ArrayList<CatalogueDetail3Object> listArray;
	private ViewPager imageDefault;
	private PicAdapter imgAdapt;
	private String img2;
	private Button buttonColor;
	private ImageView imgView;
	private String code;
	private String tid;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
		Tracker myTracker = EasyTracker.getTracker();
		myTracker.sendView("Android - Menu Product Catalogue Color");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}

	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_catalogue_color);

		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		listViewMenu = (ListView) findViewById(R.id.listView);
		pos = (int) getIntent().getIntExtra("POS", 0);
		imageDefault = (ViewPager) findViewById(R.id.news_image_url);

		buttonSpesification = (Button) findViewById(R.id.button1);
		buttonColor = (Button) findViewById(R.id.button2);
		imgView = (ImageView) findViewById(R.id.imageView);
		buttonMainFeatures = (Button) findViewById(R.id.button3);
		buttonColor.setBackgroundResource(R.drawable.button_colorsf);
		img2 = (String) getIntent().getSerializableExtra("URL");
		code = (String) getIntent().getSerializableExtra("CODE");
		tid = (String) getIntent().getSerializableExtra("TID");

		try {
			init();
			parse();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			init();
			parse();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void init() {

		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder().cacheOnDisc()
				.showStubImage(R.drawable.imagedefault_slidescreen)
				.showImageOnLoading(R.drawable.imagedefault_slidescreen)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		options2 = new DisplayImageOptions.Builder().cacheOnDisc()
				.displayer(new RoundedBitmapDisplayer(300))
				.showImageOnLoading(R.drawable.imagedefault_slidescreen)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public void parse() {

		insStr = Credential.getCredentialData(
				ProductCatalogueColorActivity.this, BaseID.KEY_DATA_CATALOGUE2);
		ParseCatalogue parsingCata = new ParseCatalogue();

		try {

			listCataObject = parsingCata.getArrayListObject(insStr);
			listCataloObject = listCataObject.get(0).getListDetail2Object();
			listArray = listCataloObject.get(pos).getListDetail3Object();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showView() {

		imageDefault.setAdapter(new ImagePagerAdapter(this));

		picGallery = (Gallery) findViewById(R.id.gallery);
		imgAdapt = new PicAdapter(this);
		picGallery.setAdapter(imgAdapt);
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;

		imageLoader.displayImage(img2 + "&w=" + width, imgView, options);

		picGallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				imageDefault.setCurrentItem(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		buttonSpesification.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				ProductCatalogueMainActivity.show(
						ProductCatalogueColorActivity.this, tid, code);
			}
		});

		buttonMainFeatures.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ProductCatalogueMainFeaturesActivity.show(
						ProductCatalogueColorActivity.this, code);

			}
		});

	}

	public class ViewHolder {
		public ImageView areaColor;
		public RelativeLayout areaAdapter;
	}

	public class PicAdapter extends BaseAdapter {

		int defaultItemBackground;
		@SuppressWarnings("unused")
		private Context galleryContext;

		public PicAdapter(Context c) {
			galleryContext = c;
		}

		@Override
		public int getCount() {
			return listArray.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View rowView = convertView;

			final CatalogueDetail3Object listArrayPos = listArray.get(position);
			LayoutInflater inflater = (LayoutInflater) getApplicationContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.listcolor, null);

			ViewHolder holder = new ViewHolder();
			holder.areaAdapter = (RelativeLayout) rowView
					.findViewById(R.id.area_adapter);
			holder.areaColor = (ImageView) rowView
					.findViewById(R.id.area_color);

			String strColor = listArrayPos.getIcon();
			imageLoader.displayImage(strColor, holder.areaColor, options2);

			rowView.setTag(holder);
			return rowView;
		}
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private LayoutInflater inflater;
		@SuppressWarnings("unused")
		private Context mcontext;

		ImagePagerAdapter(Context c) {
			mcontext = c;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			if(listArray!=null)
				return listArray.size();
			else
				return 0;
		}

		@Override
		public Object instantiateItem(View view, int position) {
			final View imageLayout = inflater.inflate(R.layout.list_item_image,
					null);
			final TouchImageView imageView = (TouchImageView) imageLayout
					.findViewById(R.id.image);
			final TextView text = (TextView) imageLayout.findViewById(R.id.text);
			imageView.setMaxZoom(4f);

			final CatalogueDetail3Object listArrayPos = listArray.get(position);

			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int width = metrics.widthPixels;
			
			text.setText(listArrayPos.getDescription());

			final ProgressBar spinner = (ProgressBar) imageLayout
					.findViewById(R.id.loading);
			imageLoader.displayImage(listArrayPos.getMotor() + "&w=" + width,
					imageView, options, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							spinner.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							String message = null;
							switch (failReason.getType()) {
							case IO_ERROR:
								message = "Input/Output error";
								break;
							case DECODING_ERROR:
								message = "Image can't be decoded";
								break;
							case NETWORK_DENIED:
								message = "Downloads are denied";
								break;
							case OUT_OF_MEMORY:
								message = "Out Of Memory error";
								break;
							case UNKNOWN:
								message = "Unknown error";
								break;
							}
							Toast.makeText(getApplicationContext(), message,
									Toast.LENGTH_SHORT).show();
							spinner.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							spinner.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingCancelled(String imageUri,
								View view) {
							spinner.setVisibility(View.GONE);
						}
					});

			((ViewPager) view).addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		// overridePendingTransition(R.anim.slide_in_left,
		// R.anim.slide_out_right);
		overridePendingTransition(0, 0);
		super.onBackPressed();
	}

	public static void show(Context context, int pos, String img, String code2,
			String tid2) {
		Intent intent = new Intent(context, ProductCatalogueColorActivity.class);
		Bundle b = new Bundle();
		b.putInt("POS", pos);
		b.putString("URL", img);
		b.putString("TID", tid2);
		b.putString("CODE", code2);
		intent.putExtras(b);
		context.startActivity(intent);
		// ((Activity) context).overridePendingTransition(R.anim.slide_in_right,
		// R.anim.slide_out_left);
		((Activity) context).overridePendingTransition(0, 0);
	}
}
