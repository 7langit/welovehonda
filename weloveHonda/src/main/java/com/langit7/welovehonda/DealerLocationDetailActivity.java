package com.langit7.welovehonda;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DealerLocationDetailActivity extends FragmentActivity{

	public GoogleMap map;
	double latitude;
	double longitude;
	String title;
	String phone;
	String email;
	String addr;
	private TextView judul;
	private TextView add;
	private TextView telp;
	private ImageButton callButton;
	private ImageButton callEmail;
	private AlertDialog dialog;
	private String latit;
	private String longit;
	private BroadcastReceiver logoutReceiver;

	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dealer_detail2_layout);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		judul = (TextView)findViewById(R.id.title);
		add = (TextView)findViewById(R.id.address);
		telp = (TextView)findViewById(R.id.telp);
		callButton = (ImageButton)findViewById(R.id.buttoncall);
		callEmail = (ImageButton)findViewById(R.id.buttonemail);

	}

	public void init(){

		title = (String)getIntent().getSerializableExtra("TITLE");
		phone = (String)getIntent().getSerializableExtra("PHONE");
		email = (String)getIntent().getSerializableExtra("EMAIL");
		addr =  (String)getIntent().getSerializableExtra("ADDR");
		latit = (String)getIntent().getSerializableExtra("LAT");
		longit = (String)getIntent().getSerializableExtra("LON");

		latitude = Double.parseDouble(latit);
		longitude = Double.parseDouble(longit);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			init();
			initilizeMap();
			showView();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void showView(){

		LatLng newLatLong = new LatLng(latitude, longitude);
		MarkerOptions marker = new MarkerOptions().position(newLatLong)
				.title(title).snippet(addr);
		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_dealer));
		map.addMarker(marker);

		judul.setText(title);
		add.setText("Alamat: "+addr);
		telp.setText("No Telp: "+phone);

		callButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(phone.contains(",")){
					phoneCall();
				}else if(phone.contains("-")){
					Toast.makeText(DealerLocationDetailActivity.this, "Phone number empty", Toast.LENGTH_LONG).show();;
				}else{
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + phone));
					startActivity(callIntent);
				}
			}
		});

		callEmail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				emailus();
			}
		});
	}

	public void phoneCall() {

		ArrayList<String> items = new ArrayList<String>(Arrays.asList(phone.split(",")));

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final ArrayAdapter<String> ph = new ArrayAdapter<String>(getApplicationContext(), R.layout.listceklist, items);
		builder.setSingleChoiceItems(ph, 0, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int position) {

				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:" + ph.getItem(position)));
				startActivity(callIntent);
				dialog.dismiss();
			}

		});
		dialog = builder.create();
		dialog.show();
	}


	public void emailus() {

		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
		sendIntent.setType("message/rfc822");
		startActivity(Intent.createChooser(sendIntent, "Choose An Email Client:"));

	}

	private void initilizeMap() {
//		SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
//				.findFragmentById(R.id.map);
//
//		map = mapFrag.getMap();
//
//		CameraPosition cameraPosition = new CameraPosition.Builder().target(
//				new LatLng(latitude, longitude)).zoom(18).build();
//
//		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	public static void show(Context context, String title2, String phone2, String email2,String addr2, String latt, String lonn ) {
		Intent intent = new Intent(context, DealerLocationDetailActivity.class);
		Bundle b = new Bundle();
		b.putString("TITLE", title2);
		b.putString("PHONE", phone2);
		b.putString("EMAIL", email2);
		b.putString("ADDR", addr2);
		b.putString("LAT", latt);
		b.putString("LON", lonn);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
