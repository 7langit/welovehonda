package com.langit7.welovehonda;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.langit7.welovehonda.adapter.ListAdapterForumTopicDetail;
import com.langit7.welovehonda.object.LatLonObject;
import com.langit7.welovehonda.parse.ParseLatLon;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetInputStream;
import com.langit7.welovehonda.util.GetStringFromInputStream;

public class MyActivityForumTopicDetail extends Activity{

	private Button buttActivity;
	private Button buttForum;
	public ProgressBar pr;
	private String KEY_DATA;
	private SharedPreferences prefData;
	ArrayList<LatLonObject>listForumObject;
	private boolean networkSlow;
	private ListAdapterForumTopicDetail listMenu;
	private ListView listViewMenu;
	private String NID;
	private String TITLE;
	private TextView topic;
	private Button buttReply;
	private String URL;
	public String insStr;
	private String user;
	private String email;
	private String post;
	private EditText editComm;
	private String DESC;
	private TextView desc2;
	private BroadcastReceiver logoutReceiver;
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(logoutReceiver);
	}
	
	public class LogoutReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("com.package.ACTION_LOGOUT")) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_activity_layout4);
		
		logoutReceiver = new LogoutReceiver();
		// Register the logout receiver
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.package.ACTION_LOGOUT");
		registerReceiver(logoutReceiver, intentFilter);
		// ==== end message logout ====

		buttActivity = (Button)findViewById(R.id.activity);
		buttForum = (Button)findViewById(R.id.forum);
		pr = (ProgressBar) findViewById(R.id.progressBar);
		buttForum.setBackgroundResource(R.drawable.button_forumactivityf);
		listViewMenu = (ListView)findViewById(R.id.listView);
		topic = (TextView)findViewById(R.id.titletopic);
		desc2 = (TextView)findViewById(R.id.desc);
		buttReply = (Button)findViewById(R.id.button_send);
		editComm = (EditText)findViewById(R.id.edit_text_out);
		
		user = Credential.getCredentialData(this, BaseID.KEY_DATA_USERNAME);
		email = Credential.getCredentialData(this, BaseID.KEY_DATA_EMAIL);

		buttActivity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				MyActivityEvent.show(MyActivityForumTopicDetail.this);
			}
		});

		try {
			init();
			new OpenData().execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		init();
		new OpenData().execute();
		super.onResume();
	}
	
	public void init(){
		NID = (String) getIntent().getSerializableExtra("NID");
		TITLE = (String) getIntent().getSerializableExtra("TITLE");
		URL = (String) getIntent().getSerializableExtra("URL");
		DESC = (String) getIntent().getSerializableExtra("DESC");
		
		topic.setText(TITLE);
		desc2.setText(DESC);

		KEY_DATA = BaseID.KEY_DATA_FORUM_TOPIC_DET+NID;
		prefData = getPreferences(MODE_PRIVATE);
		listForumObject = new ArrayList<LatLonObject>();
	}

	class OpenData extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			if (prefData.contains(KEY_DATA)) {
				prefData = getPreferences(MODE_PRIVATE);
				String coba = prefData.getString(KEY_DATA, "");
				try {
					if (!coba.equals("")) {
						parsing(new ByteArrayInputStream(coba.getBytes()));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				showView();
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchDataListField();
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
		}
	}

	public void fetchDataListField() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			is = getData(URL);
			if (is == null) {
				//cek data apakah ada di dalam preference
				if (!prefData.contains(KEY_DATA)) {
					networkSlow = true;
				} else {
					//Ambil dari persistent
					prefData = getPreferences(MODE_PRIVATE);
					String coba = prefData.getString(KEY_DATA, "");
					parsing(new ByteArrayInputStream(coba.getBytes()));
				}
			} else {
				parsing(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		nameValuePairs.add(new BasicNameValuePair("nid", NID));
		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing(InputStream is) throws IOException{
		// TODO Auto-generated method stub
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		saveInputStreamToPreferenceBuzz(insStr);

		ParseLatLon parsingForum = new ParseLatLon();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveInputStreamToPreferenceBuzz(String insStr) {
		// TODO Auto-generated method stub
		try {
			SharedPreferences prefData = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor prefDataEditor = prefData.edit();
			prefDataEditor.putString(KEY_DATA, insStr);
			prefDataEditor.commit();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	class OpenDataPost extends AsyncTask<Void, Void, Void> {

		ProgressDialog progress;
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			progress = ProgressDialog.show(MyActivityForumTopicDetail.this, "Loading",
					"Posting Comment", true);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			insertProcess();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				if (insStr.equals("[]")){
					Toast.makeText(MyActivityForumTopicDetail.this, "Posting Failed", Toast.LENGTH_SHORT).show();
				}else{
					new OpenData().execute();
				}
			}
			progress.dismiss();
		}
	}

	public void insertProcess() {
		// TODO Auto-generated method stub
		InputStream is;

		try {
			// parser xml dari server menggunakan XMLPullParser dan cara ini lebih ditekankan untuk digunakan pada android
			is = getData2(BaseID.URL_POST);
			if (is == null) {
				networkSlow = true;
			} else {
				parsing2(is);
			}//end else
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private InputStream getData2(String url2) {
		// TODO Auto-generated method stub
		InputStream is = null;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
		nameValuePairs.add(new BasicNameValuePair("nid", NID));
		nameValuePairs.add(new BasicNameValuePair("username", user));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("comment_value", post));

		GetInputStream getIS = new GetInputStream(this);
		is = getIS.postInputStream(url2, nameValuePairs);
		return is;
	}

	private void parsing2(InputStream is) throws IOException,JSONException{
		// TODO Auto-generated method stub
		insStr = GetStringFromInputStream.parsingInputStreamToString(is);

		ParseLatLon parsingForum = new ParseLatLon();
		try {
			listForumObject = parsingForum.getArrayListObject(insStr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showView(){

		listMenu = new ListAdapterForumTopicDetail(this, R.layout.listitem, listForumObject);
		listViewMenu.setAdapter(listMenu);

		buttReply.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				post =editComm.getText().toString();
				editComm.setText("");
				new OpenDataPost().execute();
			}
		});

	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(MyActivityForumTopicDetail.this, "You have network problem, please check your network setting", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String nid,String title,String url,String desc) {
		Intent intent = new Intent(context, MyActivityForumTopicDetail.class);
		Bundle b = new Bundle();
		b.putString("NID", nid);
		b.putString("TITLE", title);
		b.putString("URL", url);
		b.putString("DESC", desc);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
