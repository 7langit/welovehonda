package com.langit7.welovehonda.social;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Paging;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.langit7.welovehonda.util.BaseID;

public class Twitter extends Activity {

	public String response;
	private static twitter4j.Twitter twitter;
	private static RequestToken requestToken;
	private static SharedPreferences mSharedPreferences;

	String tweet;
	String code;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		tweet = (String) getIntent().getSerializableExtra("TWEET");
		code = (String) getIntent().getSerializableExtra("CODE");

		mSharedPreferences = getSharedPreferences(BaseID.PREFERENCE_NAME, MODE_PRIVATE);

		if (isConnected()) {
			if(code.equals("1")){
				new UpdateTwitterStatus().execute(tweet);
			}else{
				new GetTimeline().execute();
			}
		} else {
			new AskOAuth().execute();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Uri uri = intent.getData();

		if (uri != null && uri.toString().startsWith(BaseID.CALLBACK_URL)) {
			String verifier = uri
					.getQueryParameter(BaseID.IEXTRA_OAUTH_VERIFIER);
			new GetAccessToken().execute(verifier);
		} else {
		}
	}

	private class GetAccessToken extends AsyncTask<String, Void, String> {
		private AccessToken accessToken;
		private String response;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {
			String verifierget = urls[0];
			try {
				accessToken = twitter.getOAuthAccessToken(
						requestToken, verifierget);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			Editor e = mSharedPreferences.edit();
			e.putString(BaseID.PREF_KEY_TOKEN, accessToken.getToken()); 
			e.putString(BaseID.PREF_KEY_SECRET, accessToken.getTokenSecret()); 
			e.commit(); 
			if(code.equals("1")){
				new UpdateTwitterStatus().execute(tweet);
			}else{
				new GetTimeline().execute();
			}
		}
	}

	private boolean isConnected() {
		return mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, null) != null;
	}

	private class AskOAuth extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {

			ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
			configurationBuilder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
			configurationBuilder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);
			twitter4j.conf.Configuration configuration = configurationBuilder.build();
			twitter = new TwitterFactory(configuration).getInstance();
			try {
				requestToken = twitter.getOAuthRequestToken(BaseID.CALLBACK_URL);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				Toast.makeText(getApplicationContext(), "Please authorize this app!", Toast.LENGTH_LONG).show();
				startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse(requestToken.getAuthenticationURL())));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	private class GetTimeline extends AsyncTask<Void, Void, List<twitter4j.Status>> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected List<twitter4j.Status> doInBackground(Void... ignore) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
			builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);
			// Access Token 
			String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
			String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

			AccessToken accessToken = new AccessToken(access_token, access_token_secret);
			twitter4j.Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

			Paging page = new Paging();
			page.setCount(50);

			List<twitter4j.Status> statuses = new ArrayList<twitter4j.Status>();
			try {
				statuses = (List<twitter4j.Status>) twitter.getUserTimeline("welovehonda", page);
			} catch (TwitterException e) {

			}
			return statuses;
		}

		/* 
		 * {@inheritDoc}
		 */
		protected void onPostExecute(List<twitter4j.Status> result) {
			finish();
		}
	}

	class UpdateTwitterStatus extends AsyncTask<String, String, String> {

		ProgressDialog pDialog;
		private String response;
		private String responseToast;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Twitter.this);
			pDialog.setMessage("Updating to twitter...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			Log.d("Tweet Text", "> " + args[0]);
			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
				builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);

				// Access Token 
				String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

				// Update status
				twitter4j.Status response = twitter.updateStatus(status);
				if (!(""+response.getId()).equals("")) {
					responseToast = "Tweet success";
				}else{
					responseToast = "Failed to Tweet";
				}
				Log.d("Status", "> " + response.getText());
			} catch (TwitterException e) {
				e.printStackTrace();
				responseToast = "Failed to Tweet";
				Log.e("error", "err"+e.getStatusCode());
				if (e.getErrorMessage() != null) {
					responseToast = e.getErrorMessage();
				} else {
					if (e.getStatusCode() == 403) {
						responseToast = "Status is a duplicate.";
						Log.d("status code1", "msg"+e.getStatusCode()); 
					} else {
						responseToast = "There was an issue when sending your Tweet. Please try again later.";
						Log.d("status code2", "msg"+e.getStatusCode()); 
					}
				}
			}
			System.out.println("Showing home timeline.");
			return response;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			Toast.makeText(Twitter.this, responseToast, Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	public static void show(Context context, String tweet,String code2) {
		Intent intent = new Intent(context, Twitter.class);
		Bundle b = new Bundle();
		b.putString("TWEET", tweet);
		b.putString("CODE", code2);
		intent.putExtras(b);
		context.startActivity(intent);
	}
}
