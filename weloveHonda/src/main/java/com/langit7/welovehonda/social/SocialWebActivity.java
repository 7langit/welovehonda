package com.langit7.welovehonda.social;

import com.langit7.welovehonda.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class SocialWebActivity extends Activity{

	private WebView webView;
	String url2;
	private ProgressBar progressBar2;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social_timeline_layout2);

		url2 = (String)getIntent().getSerializableExtra("URL");
		progressBar2 = (ProgressBar)findViewById(R.id.progressBar);

		webView = (WebView) findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.setWebViewClient(new WebViewClient() {
			
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);
				progressBar2.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				if (progressBar2.getVisibility() == View.VISIBLE) {
					progressBar2.setVisibility(View.INVISIBLE);
				}
			}
			
			@Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
				
				// Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
	            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	            startActivity(intent);
	            return false;
	            
	        }
			
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			}
		});

		webView.loadUrl(url2);

	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}

	public static void show(Context context, String url) {
		Intent intent = new Intent(context, SocialWebActivity.class);
		Bundle b = new Bundle();
		b.putString("URL", url);
		intent.putExtras(b);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
}
