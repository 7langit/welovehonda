package com.langit7.welovehonda.social;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.langit7.welovehonda.DashboardActivity;
import com.langit7.welovehonda.R;
import com.langit7.welovehonda.adapter.ListAdapterFacebookTimeline;
import com.langit7.welovehonda.adapter.ListAdapterFacebookTimeline.FBookTaskListener;
import com.langit7.welovehonda.object.FacebookTimelineObject;
import com.langit7.welovehonda.parse.ParsingTimelineFacebook;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;
import com.langit7.welovehonda.util.GetStringFromInputStream;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class FacebookTimeline extends Activity {

	boolean networkSlow;

	String response;
	String urlhit;
	String KEY_DATA;
	String KEY_DATA_FB;

	ArrayList<FacebookTimelineObject> listFacebookTimelineObject;

	private static String APP_ID = BaseID.FACEBOOK_APP_ID;
	@SuppressWarnings("deprecation")
	private Facebook facebook = new Facebook(APP_ID);
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;

	private Button buttonFB;
	private Button buttonTwit;
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;

	private int counter;
	private ImageButton buttConnect;
	private String isLogin;

	private ProgressBar pr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social_timeline_layout);

		buttonFB = (Button) findViewById(R.id.buttonfb);
		buttonTwit = (Button) findViewById(R.id.buttontwitter);
		buttConnect = (ImageButton) findViewById(R.id.buttonconnect);
		isLogin = Credential.getCredentialData(FacebookTimeline.this,
				BaseID.KEY_DATA_LOGIN_FB);

		buttConnect.setBackgroundResource(R.drawable.bg_connect_fb);
		buttonFB.setBackgroundResource(R.drawable.button_sharefacebookf);
		pr = (ProgressBar) findViewById(R.id.progressBar);

		try {
			init();

			if (isLogin.equals("")) {
				pr.setVisibility(View.INVISIBLE);
				buttConnect.setVisibility(View.VISIBLE);
			} else {
				buttConnect.setVisibility(View.INVISIBLE);
				openDataTimeline();
			}

			buttonFB.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					FacebookTimeline.show(FacebookTimeline.this);
				}
			});

			buttonTwit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					TwitterTimeline.show(FacebookTimeline.this);
				}
			});

			buttConnect.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					openDataTimeline();
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			if (isLogin.equals("")) {
				pr.setVisibility(View.INVISIBLE);
				buttConnect.setVisibility(View.VISIBLE);
			} else {
				buttConnect.setVisibility(View.INVISIBLE);
				openDataTimeline();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void init() {
		KEY_DATA = BaseID.KEY_DATA_FB;

		listFacebookTimelineObject = new ArrayList<FacebookTimelineObject>();
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));

		options = new DisplayImageOptions.Builder().cacheOnDisc()
				.showStubImage(R.drawable.imagedefault)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
		buttConnect.setVisibility(View.INVISIBLE);
	}

	@SuppressWarnings("deprecation")
	public void openDataTimeline() {

		mPrefs = getSharedPreferences("facebook", MODE_PRIVATE);
		String access_token = mPrefs.getString(BaseID.FACEBOOK_ATOK, null);
		long expires = mPrefs.getLong(BaseID.FACEBOOK_AEX, 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			buttConnect.setVisibility(View.INVISIBLE);
			new OpenFacebookTimeline().execute();
			Credential.saveCredentialData(FacebookTimeline.this,
					BaseID.KEY_DATA_LOGIN_FB, "1");
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
			buttConnect.setVisibility(View.INVISIBLE);
		}

		if (!facebook.isSessionValid()) {
			buttConnect.setVisibility(View.VISIBLE);
			facebook.authorize(this, new String[] { "email" },
					new DialogListener() {

						@Override
						public void onCancel() {
							// Function to handle cancel event
						}

						@Override
						public void onComplete(Bundle values) {
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString(BaseID.FACEBOOK_ATOK,
									facebook.getAccessToken());
							editor.putLong(BaseID.FACEBOOK_AEX,
									facebook.getAccessExpires());
							editor.commit();
							Credential.saveCredentialData(
									FacebookTimeline.this,
									BaseID.KEY_DATA_LOGIN_FB, "1");
							new OpenFacebookTimeline().execute();
						}

						@Override
						public void onError(DialogError error) {
							// Function to handle error
						}

						@Override
						public void onFacebookError(FacebookError fberror) {
							// Function to handle Facebook errors
						}
					});
		}
	}

	class OpenFacebookTimeline extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pr.setVisibility(View.VISIBLE);
			buttConnect.setVisibility(View.INVISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			fetchDataTimeline();
			return null;
		}

		@SuppressWarnings("deprecation")
		public void fetchDataTimeline() {
			String response = null;
			try {
				response = facebook.request("welovehonda" + "/posts");
				try {
					parsing(new ByteArrayInputStream(response.getBytes()));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (networkSlow) {
				dialogNoNetwork();
			} else {
				showView();
			}
			pr.setVisibility(View.INVISIBLE);
			buttConnect.setVisibility(View.INVISIBLE);
		}
	}

	private void parsing(InputStream is) throws JSONException, IOException {
		String insStr = GetStringFromInputStream.parsingInputStreamToString(is);
		ParsingTimelineFacebook parsingTimelineFacebook = new ParsingTimelineFacebook();
		listFacebookTimelineObject = parsingTimelineFacebook
				.getArrayListFacebookTimelineObjects(insStr);
	}

	private void showView() {

		ListView listViewTimeline = (ListView) findViewById(R.id.listView);
		// Creating a button - Load More
		Button btnLoadMore = new Button(this);
		btnLoadMore.setText("Load More");

		ListAdapterFacebookTimeline adapter = new ListAdapterFacebookTimeline(
				FacebookTimeline.this, R.layout.socialitem_fb,
				listFacebookTimelineObject, imageLoader, options);

		if (counter == 0) {

			listViewTimeline.addFooterView(btnLoadMore);

		} else {

		}
		counter++;

		listViewTimeline.setAdapter(adapter);

		adapter.setTaskListener(new FBookTaskListener() {

			@Override
			public void doAuthenticationComment(String idFb, String message) {
				// TODO Auto-generated method stub
				buttConnect.setVisibility(View.INVISIBLE);
				loginToFacebookLike(idFb, message, "comment");
			}

			@Override
			public void doAuthenticationLike(String idFb) {
				// TODO Auto-generated method stub
				buttConnect.setVisibility(View.INVISIBLE);
				loginToFacebookLike(idFb, "", "like");
			}

		});

		btnLoadMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				SocialWebActivity.show(FacebookTimeline.this,
						"https://www.facebook.com/welovehonda");
			}
		});
	}

	@SuppressWarnings("deprecation")
	public void loginToFacebookLike(final String idFb, String message,
			final String type) {
		mPrefs = getSharedPreferences("facebook", MODE_PRIVATE);
		String access_token = mPrefs.getString(BaseID.FACEBOOK_ATOK, null);
		long expires = mPrefs.getLong(BaseID.FACEBOOK_AEX, 0);
		if (access_token != null) {
			facebook.setAccessToken(access_token);
			if (type.equals("like")) {
				new LikeComment(idFb).execute();
			} else {
				new Comment(idFb, message).execute();
			}

		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this, new String[] { "publish_stream",
					"user_status", "email", "read_stream", "user_interests",
					"publish_actions", "publish_actions" },
					new DialogListener() {

						@Override
						public void onCancel() {
						}

						@Override
						public void onComplete(Bundle values) {
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString(BaseID.FACEBOOK_ATOK,
									facebook.getAccessToken());
							editor.putLong(BaseID.FACEBOOK_AEX,
									facebook.getAccessExpires());
							editor.commit();
							if (type.equals("like")) {
								try {
									new LikeComment(idFb).execute();
								} catch (Exception e) {
									// TODO: handle exception
								}
							} else {

							}
						}

						@Override
						public void onError(DialogError error) {
							// Function to handle error
						}

						@Override
						public void onFacebookError(FacebookError fberror) {
							// Function to handle Facebook errors
						}
					});
		}
	}

	class LikeComment extends AsyncTask<Void, Void, Void> {

		private String idFb;
		private String response;

		public LikeComment(String idFb) {
			this.idFb = idFb;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {
			Bundle parameters = new Bundle();
			parameters.putString("id", idFb);
			try {
				response = facebook.request("likes", parameters, "POST");
				System.out.println("responseLike=" + response);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (response.equals("true")) {
				Toast.makeText(FacebookTimeline.this, "Success",
						Toast.LENGTH_SHORT).show();
			} else {
				String json = response;
				try {
					JSONObject jobjectError = new JSONObject(json);
					final String strError = jobjectError.getJSONObject("error")
							.optString("message");
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (strError
									.equals("(#200) You do not have sufficient to permissions to perform this action")) {
								Toast.makeText(FacebookTimeline.this,
										"Please wait..", Toast.LENGTH_LONG)
										.show();
							} else if (strError
									.contains("An active access token must be used to query information about the current user.")) {
								Toast.makeText(FacebookTimeline.this,
										"Please wait..", Toast.LENGTH_LONG)
										.show();
							} else if (strError
									.contains("Error validating access token: "
											+ "The session has been invalidated because the user has changed the password.")) {
								Toast.makeText(
										FacebookTimeline.this,
										"Please sign in through facebook.com because the password has been changed.",
										Toast.LENGTH_LONG).show();
							} else {
								Toast.makeText(FacebookTimeline.this, strError,
										Toast.LENGTH_LONG).show();
							}
						}
					});

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	class Comment extends AsyncTask<Void, Void, Void> {

		private String idFb;
		private String response;
		private String message;

		public Comment(String idFb, String message) {
			this.idFb = idFb;
			this.message = message;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {
			Bundle parameters = new Bundle();
			parameters.putString("message", message);
			parameters.putString("id", idFb);
			try {
				response = facebook.request("comments", parameters, "POST");
				System.out.println(response);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			JSONObject jobjectMessage;
			try {
				jobjectMessage = new JSONObject(response);
				if (jobjectMessage.has("id")) {
					Toast.makeText(getApplicationContext(), "Success",
							Toast.LENGTH_SHORT).show();
				} else if (jobjectMessage.has("error")) {
					String strMessage = jobjectMessage.getJSONObject("error")
							.optString("message");
					if (strMessage
							.equals("(#200) You do not have sufficient to permissions to perform this action")) {
						Toast.makeText(FacebookTimeline.this, "Please wait..",
								Toast.LENGTH_LONG).show();

					} else if (strMessage
							.contains("An active access token must be used to query information about the current user.")) {
						Toast.makeText(FacebookTimeline.this, "Please wait..",
								Toast.LENGTH_LONG).show();

					} else if (strMessage
							.contains("Error validating access token: "
									+ "The session has been invalidated because the user has changed the password.")) {
						Toast.makeText(
								FacebookTimeline.this,
								"Please sign in through facebook.com because the password has been changed.",
								Toast.LENGTH_LONG).show();

					} else {
						Toast.makeText(FacebookTimeline.this, strMessage,
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(FacebookTimeline.this,
							"Please try again later", Toast.LENGTH_SHORT)
							.show();
				}
			} catch (JSONException e) {
				Toast.makeText(FacebookTimeline.this,
						"Please try again, " + e.getMessage(),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void dialogNoNetwork() {
		// TODO Auto-generated method stub
		networkSlow = false;
		Toast.makeText(FacebookTimeline.this,
				"Please try again in a few minutes", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		DashboardActivity.show(FacebookTimeline.this);
	}

	@SuppressWarnings("unused")
	private void saveInputStreamToPreference(String insStr) {
		SharedPreferences prefData = getPreferences(MODE_PRIVATE);
		SharedPreferences.Editor prefDataEditor = prefData.edit();
		prefDataEditor.putString(KEY_DATA, insStr);
		prefDataEditor.commit();
	}

	// private void dialogConfirm(final String idFb) {
	// String titleDialog = "Confirm";
	// String messageDialog = "Are you sure to like this post?";
	// AlertDialog.Builder builder = new AlertDialog.Builder(this);
	// builder.setTitle(titleDialog);
	// builder.setMessage(messageDialog);
	// builder.setCancelable(false);
	// builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// loginToFacebookLike(idFb, "", "like");
	// }
	// });
	// builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// return;
	// }
	// });
	// AlertDialog alert = builder.create();
	// alert.show();
	// }

	public static void show(Context context) {
		Intent intent = new Intent(context, FacebookTimeline.class);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.slide_in_right,
				R.anim.slide_out_left);
	}

}
