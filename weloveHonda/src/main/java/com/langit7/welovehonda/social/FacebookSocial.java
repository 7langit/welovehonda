package com.langit7.welovehonda.social;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.Credential;

public class FacebookSocial extends Activity {

	private String KEY_DATA_FB;
	@SuppressWarnings("deprecation")
	private Facebook facebook = new Facebook(BaseID.FACEBOOK_APP_ID);
	private SharedPreferences mPrefs;
	String msg;
	String code;
	private String link;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		msg = (String) getIntent().getSerializableExtra("MSG");
		link = (String) getIntent().getSerializableExtra("LINK");
		
		KEY_DATA_FB = BaseID.KEY_DATA_FB;
		mPrefs = getSharedPreferences(KEY_DATA_FB, MODE_PRIVATE);

		login(msg);
	}

	@SuppressWarnings("deprecation")
	public void login(final String message) {

		String access_token = mPrefs.getString(BaseID.FACEBOOK_ATOK, null);
		long expires = mPrefs.getLong(BaseID.FACEBOOK_AEX, 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			new ShareStatus(message).execute();
			Credential.saveCredentialData(FacebookSocial.this, BaseID.KEY_DATA_LOGIN_FB, "1");
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "publish_stream", "user_status", "email", "read_stream", "user_interests", "publish_actions", "publish_actions"  },
					new DialogListener() {

				@Override
				public void onCancel() {
				}
				@Override
				public void onComplete(Bundle values) {
					SharedPreferences.Editor editor = mPrefs.edit();
					editor.putString(BaseID.FACEBOOK_ATOK, facebook.getAccessToken());
					editor.putLong(BaseID.FACEBOOK_AEX, facebook.getAccessExpires());
					editor.commit();
					Credential.saveCredentialData(FacebookSocial.this, BaseID.KEY_DATA_LOGIN_FB, "1");
					new ShareStatus(message).execute();
				}

				@Override
				public void onError(DialogError error) {
				}

				@Override
				public void onFacebookError(FacebookError fberror) {
				}
			});
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}

	class ShareStatus extends AsyncTask<Void, Void, Void>{

		private String message;
		private String response;

		public ShareStatus(String message) {
			this.message = message;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {
			Bundle parameters = new Bundle();
			parameters.putString("message", message);
			parameters.putString("link", link);
			try {
				facebook.request("me");
				response = facebook.request("me/feed", parameters,"POST");
				System.out.println("response="+response);
				//				Log.d("response Share", "response Share"+response);
			} catch (IOException e) {
				e.printStackTrace();
			}
			//			Log.d("FB Sessions", "" + facebook.isSessionValid());
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			JSONObject jobjectMessage;
			try {
				jobjectMessage = new JSONObject(response);
				if (jobjectMessage.has("id")) {
					Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
				}else if (jobjectMessage.has("error")) {
					String strMessage = jobjectMessage.getJSONObject("error").optString("message");
					if (strMessage.equals("(#200) You do not have sufficient to permissions to perform this action")) {
						Toast.makeText(FacebookSocial.this, "Please wait..", Toast.LENGTH_LONG).show();

					}else if (strMessage.contains("An active access token must be used to query information about the current user.")) {
						Toast.makeText(FacebookSocial.this, "Please wait..", Toast.LENGTH_LONG).show();

					}else if(strMessage.contains("Error validating access token: " +
							"The session has been invalidated because the user has changed the password.")) {
						Toast.makeText(FacebookSocial.this, 
								"Please sign in through facebook.com because the password has been changed.", Toast.LENGTH_LONG).show();

					}else {
						Toast.makeText(FacebookSocial.this, strMessage, Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(FacebookSocial.this,"Please try again later" , Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				//				e.printStackTrace();
				Toast.makeText(FacebookSocial.this,"Please try again, "+e.getMessage() , Toast.LENGTH_SHORT).show();
			}
			finish();
		}
	}
	
	public static void show(Context context, String msg,String link2) {
		Intent intent = new Intent(context, FacebookSocial.class);
		Bundle b = new Bundle();
		b.putString("MSG", msg);
		b.putString("LINK", link2);
		intent.putExtras(b);
		context.startActivity(intent);
	}

}
