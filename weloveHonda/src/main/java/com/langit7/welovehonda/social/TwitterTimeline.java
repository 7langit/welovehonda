package com.langit7.welovehonda.social;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Paging;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.langit7.welovehonda.R;
import com.langit7.welovehonda.adapter.ListAdapterTwitter;
import com.langit7.welovehonda.util.BaseID;
import com.langit7.welovehonda.util.ConnectionDetector;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class TwitterTimeline extends Activity{

	private ProgressBar dialog;
	boolean networkSlow;
	ConnectionDetector connection;
	private ImageLoader imageLoader= ImageLoader.getInstance();

	private ListView list_place;
	public boolean sameData;
	private Button buttonFB;
	private Button buttonTwit;
	List<twitter4j.Status> statuses ;
	private int counter;
	private ImageButton buttConnect;
	private static SharedPreferences mSharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social_timeline_layout);

		buttonFB = (Button)findViewById(R.id.buttonfb);
		buttonTwit = (Button)findViewById(R.id.buttontwitter);
		mSharedPreferences = getSharedPreferences(BaseID.PREFERENCE_NAME, MODE_PRIVATE);
		buttConnect = (ImageButton)findViewById(R.id.buttonconnect);
		
		buttonTwit.setBackgroundResource(R.drawable.button_sharetwitterf);
		init();
		
		buttonFB.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				FacebookTimeline.show(TwitterTimeline.this);
			}
		});

		buttonTwit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				TwitterTimeline.show(TwitterTimeline.this);
			}
		});
		
		buttConnect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Twitter.show(TwitterTimeline.this,"","2");
			}
		});
		
		try {
			if (isConnected()) {
				try {
					buttConnect.setVisibility(View.INVISIBLE);
					new GetTimeline().execute();
				} catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				try {
					buttConnect.setVisibility(View.VISIBLE);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (isConnected()) {
			try {
				buttConnect.setVisibility(View.INVISIBLE);
				new GetTimeline().execute();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			try {
				buttConnect.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
	private boolean isConnected() {
		return mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, null) != null;
	}

	public void init(){

		dialog = (ProgressBar) findViewById(R.id.progressBar);
		dialog.setVisibility(View.INVISIBLE);
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		list_place 	= (ListView) findViewById(R.id.listView);

	}

	public void setDataToView() {
		try {
			Button btnLoadMore = new Button(this);
			btnLoadMore.setText("Load More");
			if (counter==0) {

				list_place.addFooterView(btnLoadMore);

				}else {

				}
				counter++;
			list_place.setAdapter(new ListAdapterTwitter(this, R.layout.socialitem_twitter, statuses));
			
			btnLoadMore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					SocialWebActivity.show(TwitterTimeline.this, "https://www.twitter.com/welovehonda");
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private class GetTimeline extends AsyncTask<Void, Void, List<twitter4j.Status>> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected List<twitter4j.Status> doInBackground(Void... ignore) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(BaseID.CONSUMER_KEY);
			builder.setOAuthConsumerSecret(BaseID.CONSUMER_SECRET);
			// Access Token 
			String access_token = mSharedPreferences.getString(BaseID.PREF_KEY_TOKEN, "");
			String access_token_secret = mSharedPreferences.getString(BaseID.PREF_KEY_SECRET, "");

			AccessToken accessToken = new AccessToken(access_token, access_token_secret);
			twitter4j.Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

			Paging page = new Paging();
			page.setCount(50);

		    statuses = new ArrayList<twitter4j.Status>();
			try {
				statuses = (List<twitter4j.Status>) twitter.getUserTimeline("welovehonda", page);
			} catch (TwitterException e) {

			}
			return statuses;
		}

		/* 
		 * {@inheritDoc}
		 */
		protected void onPostExecute(List<twitter4j.Status> result) {
			dialog.setVisibility(View.INVISIBLE);
			setDataToView();
		}
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
		super.onBackPressed();
	}
	
	public static void show(Context context) {
		Intent intent = new Intent(context, TwitterTimeline.class);
		context.startActivity(intent);
		((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}